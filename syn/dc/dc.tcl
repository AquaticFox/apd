# set_host_options -max_cores 16
# compile_ultra

set DC_HOME $env(DC_HOME)
set APD_SRC_PATH /home/zfu/rios/apd_from_lab1/apd

lappend search_path . 
lappend search_path $DC_HOME/libraries/syn 
lappend search_path $DC_HOME/dw/syn_ver
# lappend search_path $APD_SRC_PATH/syn/openram/sram_20_2048_scn4m_subm
# lappend search_path $APD_SRC_PATH/syn/openram/sram_2_2048_scn4m_subm
# lappend search_path $APD_SRC_PATH/syn/openram/sram_22_2048_scn4m_subm
# lappend search_path $APD_SRC_PATH/syn/openram/sram_512_2048_scn4m_subm

lappend search_path $APD_SRC_PATH
lappend search_path $APD_SRC_PATH/include
lappend search_path $APD_SRC_PATH/src
lappend search_path $APD_SRC_PATH/src/common_cell
lappend search_path $APD_SRC_PATH/src/cache_subsystem
lappend search_path $APD_SRC_PATH/src/load_store_unit
lappend search_path $APD_SRC_PATH/src/frontend
lappend search_path $APD_SRC_PATH/src/mmu
lappend search_path $APD_SRC_PATH/src/id_stage
lappend search_path $APD_SRC_PATH/src/is_stage
lappend search_path $APD_SRC_PATH/src/ex_stage

set synthetic_library "dw_foundation.sldb standard.sldb "
set target_library "class.db and_or.db "
set link_library "* class.db and_or.db dw_foundation.sldb "
set symbol_library "class.sdb generic.sdb"
# sram_20_2048_scn4m_subm_TT_5p0V_25C.lib sram_22_2048_scn4m_subm_TT_5p0V_25C.lib sram_2_2048_scn4m_subm_TT_5p0V_25C.lib sram_512_2048_scn4m_subm_TT_5p0V_25C.lib
analyze -format sverilog { riscv_pkg.sv apd_pkg.sv axi_pkg.sv apd_axi_pkg.sv lzc.sv fifo.sv }
analyze -format sverilog { btb.sv  alu.sv branch_unit.sv multiplier_fast.sv div.sv regfile.sv issue.sv pick.sv decoder_compressed.sv decoder.sv tlb.sv ptw.sv inst_buffer.sv  sram_wrapper.sv dcache_ram.sv icache_mesi.sv dcache_mesi.sv l2_cache_mesi.sv  }
analyze -format sverilog { mult.sv agu.sv cache_subsystem.sv load_store_unit.sv }
analyze -format sverilog { frontend.sv mmu.sv  id_stage.sv is_stage.sv ex_stage.sv csr_regfile.sv control.sv performance_counter.sv apd.sv }

elaborate apd 

link

create_clock -period 200 [get_ports clk_i]
set dont_touch_network [get_clocks clk_i]
set dont_touch_network [get_ports rst_ni]


# compile_ultra
compile -map_effort medium -incremental_mapping
report_timing -loops
write  -f  ddc  -hier  -out  APD.ddc




