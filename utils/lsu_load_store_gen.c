#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

void printfBin(int num, FILE*fp);

int main(int argc, char** argv)
{
    int cache_line_width_bit;
    int LOG2DCLOFFSET;

    int xlen;
    int LOG2REGOFFSET;

    if(argc > 1)
    {
        cache_line_width_bit = atoi(argv[1]);
    }
    else
    {
        cache_line_width_bit = 512; // default
        printf("no line-width input, default to 512 bits\n");
    }
    if(argc > 2)
    {
        xlen = atoi(argv[2]);
    }
    else
    {
        xlen = 64; // default
        printf("no xlen input, default to 64 bits\n");
    }
    LOG2DCLOFFSET = log10(cache_line_width_bit/8) / log10(2);
    LOG2REGOFFSET = log10(xlen/8) / log10(2);

    FILE *fp_load, *fp_store;
    
    if((fp_load = fopen("lsu_load_result_gen.txt", "wb+")) == NULL)
    {
        printf("generate file1 open failed\n");
    }

    if((fp_store = fopen("lsu_store_byteenable_gen.txt", "wb+")) == NULL)
    {
        printf("generate file2 open failed\n");
    }

    fprintf(fp_load, "always_comb begin: load_result\n");
    fprintf(fp_load, "    unique case(req_length_i)\n");
    fprintf(fp_store, "always_comb begin: store_byte_enable\n");
    fprintf(fp_store, "    byte_enable = '0;\n");
    fprintf(fp_store, "    unique case(req_length_i)\n");

    char L[100];
    char L2[100];
    int j_num_load;
    int j_num_store;
    for(int i = 0; i < 4; i++) // L8, L16, L32, L64
    {
        strcpy(L, "        L");
        sprintf(L2, "%d:", 1 << i+3);
        strcat(L, L2);
        fprintf(fp_load, "%s unique case(lsu_data_in_fun.dm_addr[%d-1:0]) // LOG2DCLOFFSET - 1\n", L, LOG2DCLOFFSET);
        fprintf(fp_store, "%s unique case(lsu_data_in_fun.dm_addr[%d-1:0]) // LOG2REGOFFSET - 1\n", L, LOG2REGOFFSET);
        
        j_num_load  = cache_line_width_bit/8 - ((1<<i+3)/8-1);
        j_num_store = xlen/8 - ((1<<i+3)/8-1);
        
        for(int j = 0; j < j_num_load; j++)
        {
            fprintf(fp_load, "            %d'd%d: result_o = { {(DATAWIDTH-%d){lsu_data_in_fun.load_signed & dc2lsu_i.data[%d]} }, dc2lsu_i.data[%d +: %d]  };\n", LOG2DCLOFFSET, j, 1 << i+3, j*8 + (1 << i+3) - 1, j*8, 1 << i+3);
        }
        for(int j = 0; j < j_num_store; j++)
        {
            fprintf(fp_store, "            %d'd%d: byte_enable = %d'b", LOG2REGOFFSET, j, xlen/8);
            printfBin((1<<(1<<i+3)/8)-1<<j, fp_store);
            fprintf(fp_store, ";\n");
        }
        fprintf(fp_load, "            default:; // TODO, misalign exception\n");
        fprintf(fp_load, "        endcase\n");

        fprintf(fp_store, "            default:; // TODO, misalign exception\n");
        fprintf(fp_store, "        endcase\n");
    }
    fprintf(fp_load, "        default:;\n");
    fprintf(fp_load, "    endcase\n");
    fprintf(fp_load, "end\n");
    fclose(fp_load);

    fprintf(fp_store, "        default:;\n");
    fprintf(fp_store, "    endcase\n");
    fprintf(fp_store, "end\n");
    fclose(fp_store);
    return 0;
}

void printfBin(int num, FILE*fp)
{
    int temp = num/2;
    if(temp != 0)
    {
        printfBin(temp, fp);
    }
    temp = num%2;
    fprintf(fp, "%d", temp);
}