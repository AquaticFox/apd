

module decoder import apd_pkg::*; (
    input  logic [riscv_pkg::VLEN-1:0] pc_i,            // PC from IF
    input  logic               is_compressed_i,         // is a compressed instruction
    input  logic [15:0]        compressed_instr_i,      // compressed form of instruction
    input  logic               is_illegal_i,            // illegal compressed instruction
    input  logic [31:0]        instruction_i,           // instruction from IF
    input  branchpredict_sbe_t branch_predict_i,                                                                // '0 TODO
    input  exception_t         ex_i,                    // if an exception occured in if
    input  logic [1:0]         irq_i,                   // external interrupt                                   // '0 TODO
    input  irq_ctrl_t          irq_ctrl_i,              // interrupt control and status information from CSRs   // '0 TODO
    // From CSR
    input  riscv_pkg::priv_lvl_t   priv_lvl_i,          // current privilege level                              // '0 TODO
    input  logic               debug_mode_i,            // we are in debug mode
    input  logic               tvm_i,                   // trap virtual memory
    input  logic               tw_i,                    // timeout wait
    input  logic               tsr_i,                   // trap sret
    output scoreboard_entry_t  instruction_o,           // scoreboard entry to scoreboard
    output logic               is_control_flow_instr_o  // this instruction will change the control flow
);
    localparam bit ENABLE_WFI = 1'b0;
    
    logic illegal_instr;
    // this instruction is an environment call (ecall), it is handled like an exception
    logic ecall;
    // this instruction is a software break-point
    logic ebreak;
    // this instruction needs floating-point rounding-mode verification
    logic check_fprm;
    riscv_pkg::instruction_t instr;
    assign instr = riscv_pkg::instruction_t'(instruction_i);
    // --------------------
    // Immediate select
    // --------------------
    enum logic[3:0] {
        NOIMM, IIMM, SIMM, SBIMM, UIMM, JIMM, RS3
    } imm_select;

    riscv_pkg::xlen_t imm_i_type;
    riscv_pkg::xlen_t imm_s_type;
    riscv_pkg::xlen_t imm_sb_type;
    riscv_pkg::xlen_t imm_u_type;
    riscv_pkg::xlen_t imm_uj_type;
    riscv_pkg::xlen_t imm_bi_type;

    always_comb begin : decoder

        imm_select                  = NOIMM;
        is_control_flow_instr_o     = 1'b0;
        illegal_instr               = 1'b0;
        instruction_o.pc            = pc_i;
        instruction_o.trans_id      = 5'b0;
        instruction_o.fu            = NONE;
        instruction_o.op            = apd_pkg::ADD;
        instruction_o.rs1           = '0;
        instruction_o.rs2           = '0;
        instruction_o.rd            = '0;
        instruction_o.use_pc        = 1'b0;
        instruction_o.trans_id      = '0;
        instruction_o.is_compressed = is_compressed_i;
        instruction_o.use_zimm      = 1'b0;
//        instruction_o.bp            = branch_predict_i; // TODO
        instruction_o.bp.cf = NoCF; // TODO
        instruction_o.bp.predict_address = pc_i + (is_compressed_i ? 2 : 4); // TODO
        
        ecall                       = 1'b0;
        ebreak                      = 1'b0;
        check_fprm                  = 1'b0;

        if (~ex_i.valid) begin
            case (instr.rtype.opcode)
                riscv_pkg::OpcodeSystem: begin
                    instruction_o.fu       = CSR;
                    instruction_o.rs1[4:0] = instr.itype.rs1;
                    instruction_o.rs2[4:0] = instr.rtype.rs2;   //TODO: needs to be checked if better way is available
                    instruction_o.rd[4:0]  = instr.itype.rd;

                    unique case (instr.itype.funct3)
                        3'b000: begin
                            // check if the RD and and RS1 fields are zero, this may be reset for the SENCE.VMA instruction
                            if (instr.itype.rs1 != '0 || instr.itype.rd != '0)
                                illegal_instr = 1'b1;
                            // decode the immiediate field
                            case (instr.itype.imm)
                                // ECALL -> inject exception
                                12'b0: ecall  = 1'b1;
                                // EBREAK -> inject exception
                                12'b1: ebreak = 1'b1;
                                // SRET
                                12'b1_0000_0010: begin
                                    instruction_o.op = apd_pkg::SRET;
                                    // check privilege level, SRET can only be executed in S and M mode
                                    // we'll just decode an illegal instruction if we are in the wrong privilege level
                                    if (priv_lvl_i == riscv_pkg::PRIV_LVL_U) begin
                                        illegal_instr = 1'b1;
                                        //  do not change privilege level if this is an illegal instruction
                                        instruction_o.op = apd_pkg::ADD;
                                    end
                                    // if we are in S-Mode and Trap SRET (tsr) is set -> trap on illegal instruction
                                    if (priv_lvl_i == riscv_pkg::PRIV_LVL_S && tsr_i) begin
                                        illegal_instr = 1'b1;
                                        //  do not change privilege level if this is an illegal instruction
                                        instruction_o.op = apd_pkg::ADD;
                                    end
                                end
                                // MRET
                                12'b11_0000_0010: begin
                                    instruction_o.op = apd_pkg::MRET;
                                    // check privilege level, MRET can only be executed in M mode
                                    // otherwise we decode an illegal instruction
                                    if (priv_lvl_i inside {riscv_pkg::PRIV_LVL_U, riscv_pkg::PRIV_LVL_S})
                                        illegal_instr = 1'b1;
                                end
                                // DRET
                                12'b111_1011_0010: begin
                                    instruction_o.op = apd_pkg::DRET;
                                    // check that we are in debug mode when executing this instruction
                                    illegal_instr = (!debug_mode_i) ? 1'b1 : 1'b0;
                                end
                                // WFI
                                12'b1_0000_0101: begin
                                    if (ENABLE_WFI) instruction_o.op = apd_pkg::WFI;
                                    // if timeout wait is set, trap on an illegal instruction in S Mode
                                    // (after 0 cycles timeout)
                                    if (priv_lvl_i == riscv_pkg::PRIV_LVL_S && tw_i) begin
                                        illegal_instr = 1'b1;
                                        instruction_o.op = apd_pkg::ADD;
                                    end
                                    // we don't support U mode interrupts so WFI is illegal in this context
                                    if (priv_lvl_i == riscv_pkg::PRIV_LVL_U) begin
                                        illegal_instr = 1'b1;
                                        instruction_o.op = apd_pkg::ADD;
                                    end
                                end
                                // SFENCE.VMA
                                default: begin
                                    if (instr.instr[31:25] == 7'b1001) begin
                                        // check privilege level, SFENCE.VMA can only be executed in M/S mode
                                        // otherwise decode an illegal instruction
                                        illegal_instr    = (priv_lvl_i inside {riscv_pkg::PRIV_LVL_M, riscv_pkg::PRIV_LVL_S}) ? 1'b0 : 1'b1;
                                        instruction_o.op = apd_pkg::SFENCE_VMA;
                                        // check TVM flag and intercept SFENCE.VMA call if necessary
                                        if (priv_lvl_i == riscv_pkg::PRIV_LVL_S && tvm_i)
                                            illegal_instr = 1'b1;
                                    end
                                end
                            endcase
                        end
                        // atomically swaps values in the CSR and integer register
                        3'b001: begin// CSRRW
                            imm_select = IIMM;
                            instruction_o.op = apd_pkg::CSR_WRITE;
                        end
                        // atomically set values in the CSR and write back to rd
                        3'b010: begin// CSRRS
                            imm_select = IIMM;
                            // this is just a read
                            if (instr.itype.rs1 == 5'b0)
                                instruction_o.op = apd_pkg::CSR_READ;
                            else
                                instruction_o.op = apd_pkg::CSR_SET;
                        end
                        // atomically clear values in the CSR and write back to rd
                        3'b011: begin// CSRRC
                            imm_select = IIMM;
                            // this is just a read
                            if (instr.itype.rs1 == 5'b0)
                                instruction_o.op = apd_pkg::CSR_READ;
                            else
                                instruction_o.op = apd_pkg::CSR_CLEAR;
                        end
                        // use zimm and iimm
                        3'b101: begin// CSRRWI
                            instruction_o.rs1[4:0] = instr.itype.rs1;
                            imm_select = IIMM;
                            instruction_o.use_zimm = 1'b1;
                            instruction_o.op = apd_pkg::CSR_WRITE;
                        end
                        3'b110: begin// CSRRSI
                            instruction_o.rs1[4:0] = instr.itype.rs1;
                            imm_select = IIMM;
                            instruction_o.use_zimm = 1'b1;
                            // this is just a read
                            if (instr.itype.rs1 == 5'b0)
                                instruction_o.op = apd_pkg::CSR_READ;
                            else
                                instruction_o.op = apd_pkg::CSR_SET;
                        end
                        3'b111: begin// CSRRCI
                            instruction_o.rs1[4:0] = instr.itype.rs1;
                            imm_select = IIMM;
                            instruction_o.use_zimm = 1'b1;
                            // this is just a read
                            if (instr.itype.rs1 == 5'b0)
                                instruction_o.op = apd_pkg::CSR_READ;
                            else
                                instruction_o.op = apd_pkg::CSR_CLEAR;
                        end
                        default: illegal_instr = 1'b1;
                    endcase
                end
                // Memory ordering instructions
                riscv_pkg::OpcodeMiscMem: begin
                    instruction_o.fu  = CSR;
                    instruction_o.rs1 = '0;
                    instruction_o.rs2 = '0;
                    instruction_o.rd  = '0;

                    case (instr.stype.funct3)
                        // FENCE
                        // Currently implemented as a whole DCache flush boldly ignoring other things
                        3'b000: instruction_o.op  = apd_pkg::FENCE;
                        // FENCE.I
                        3'b001: begin
                            if (instr.instr[31:20] != '0)
                                illegal_instr = 1'b1;
                            instruction_o.op  = apd_pkg::FENCE;
                            // instruction_o.op  = apd_pkg::FENCE_I;
                        end
                        default: illegal_instr = 1'b1;
                    endcase

                    if (instr.stype.rs1 != '0 || instr.stype.imm0 != '0 || instr.instr[31:28] != '0)
                        illegal_instr = 1'b1;
                end

                // --------------------------
                // Reg-Reg Operations
                // --------------------------
                riscv_pkg::OpcodeOp: begin
                    // --------------------------------------------
                    // Vectorial Floating-Point Reg-Reg Operations
                    // --------------------------------------------
                    if (instr.rvftype.funct2 == 2'b10) begin // Prefix 10 for all Xfvec ops
                        // only generate decoder if FP extensions are enabled (static)
                        illegal_instr = 1'b1;
                    // ---------------------------
                    // Integer Reg-Reg Operations
                    // ---------------------------
                    end else begin
                        instruction_o.fu  = (instr.rtype.funct7 == 7'b000_0001) ? MULT : ALU;
                        instruction_o.rs1 = instr.rtype.rs1;
                        instruction_o.rs2 = instr.rtype.rs2;
                        instruction_o.rd  = instr.rtype.rd;

                        unique case ({instr.rtype.funct7, instr.rtype.funct3})
                            {7'b000_0000, 3'b000}: instruction_o.op = apd_pkg::ADD;   // Add
                            {7'b010_0000, 3'b000}: instruction_o.op = apd_pkg::SUB;   // Sub
                            {7'b000_0000, 3'b010}: instruction_o.op = apd_pkg::SLTS;  // Set Lower Than
                            {7'b000_0000, 3'b011}: instruction_o.op = apd_pkg::SLTU;  // Set Lower Than Unsigned
                            {7'b000_0000, 3'b100}: instruction_o.op = apd_pkg::XORL;  // Xor
                            {7'b000_0000, 3'b110}: instruction_o.op = apd_pkg::ORL;   // Or
                            {7'b000_0000, 3'b111}: instruction_o.op = apd_pkg::ANDL;  // And
                            {7'b000_0000, 3'b001}: instruction_o.op = apd_pkg::SLL;   // Shift Left Logical
                            {7'b000_0000, 3'b101}: instruction_o.op = apd_pkg::SRL;   // Shift Right Logical
                            {7'b010_0000, 3'b101}: instruction_o.op = apd_pkg::SRA;   // Shift Right Arithmetic
                            // Multiplications
                            {7'b000_0001, 3'b000}: instruction_o.op = apd_pkg::MUL;
                            {7'b000_0001, 3'b001}: instruction_o.op = apd_pkg::MULH;
                            {7'b000_0001, 3'b010}: instruction_o.op = apd_pkg::MULHSU;
                            {7'b000_0001, 3'b011}: instruction_o.op = apd_pkg::MULHU;
                            {7'b000_0001, 3'b100}: instruction_o.op = apd_pkg::DIV;
                            {7'b000_0001, 3'b101}: instruction_o.op = apd_pkg::DIVU;
                            {7'b000_0001, 3'b110}: instruction_o.op = apd_pkg::REM;
                            {7'b000_0001, 3'b111}: instruction_o.op = apd_pkg::REMU;
                            default: begin
                                illegal_instr = 1'b1;
                            end
                        endcase
                    end
                end

                // --------------------------
                // 32bit Reg-Reg Operations
                // --------------------------
                riscv_pkg::OpcodeOp32: begin
                    instruction_o.fu  = (instr.rtype.funct7 == 7'b000_0001) ? MULT : ALU;
                    instruction_o.rs1[4:0] = instr.rtype.rs1;
                    instruction_o.rs2[4:0] = instr.rtype.rs2;
                    instruction_o.rd[4:0]  = instr.rtype.rd;
                      if (riscv_pkg::IS_XLEN64) begin
                        unique case ({instr.rtype.funct7, instr.rtype.funct3})
                            {7'b000_0000, 3'b000}: instruction_o.op = apd_pkg::ADDW; // addw
                            {7'b010_0000, 3'b000}: instruction_o.op = apd_pkg::SUBW; // subw
                            {7'b000_0000, 3'b001}: instruction_o.op = apd_pkg::SLLW; // sllw
                            {7'b000_0000, 3'b101}: instruction_o.op = apd_pkg::SRLW; // srlw
                            {7'b010_0000, 3'b101}: instruction_o.op = apd_pkg::SRAW; // sraw
                            // Multiplications
                            {7'b000_0001, 3'b000}: instruction_o.op = apd_pkg::MULW;
                            {7'b000_0001, 3'b100}: instruction_o.op = apd_pkg::DIVW;
                            {7'b000_0001, 3'b101}: instruction_o.op = apd_pkg::DIVUW;
                            {7'b000_0001, 3'b110}: instruction_o.op = apd_pkg::REMW;
                            {7'b000_0001, 3'b111}: instruction_o.op = apd_pkg::REMUW;
                            default: illegal_instr = 1'b1;
                        endcase
                      end else illegal_instr = 1'b1;
                end
                // --------------------------------
                // Reg-Immediate Operations
                // --------------------------------
                riscv_pkg::OpcodeOpImm: begin
                    instruction_o.fu  = ALU;
                    imm_select = IIMM;
                    instruction_o.rs1[4:0] = instr.itype.rs1;
                    instruction_o.rd[4:0]  = instr.itype.rd;

                    unique case (instr.itype.funct3)
                        3'b000: instruction_o.op = apd_pkg::ADD;   // Add Immediate
                        3'b010: instruction_o.op = apd_pkg::SLTS;  // Set to one if Lower Than Immediate
                        3'b011: instruction_o.op = apd_pkg::SLTU;  // Set to one if Lower Than Immediate Unsigned
                        3'b100: instruction_o.op = apd_pkg::XORL;  // Exclusive Or with Immediate
                        3'b110: instruction_o.op = apd_pkg::ORL;   // Or with Immediate
                        3'b111: instruction_o.op = apd_pkg::ANDL;  // And with Immediate

                        3'b001: begin
                          instruction_o.op = apd_pkg::SLL;  // Shift Left Logical by Immediate
                          if (instr.instr[31:26] != 6'b0)
                            illegal_instr = 1'b1;
                          if (instr.instr[25] != 1'b0 && riscv_pkg::XLEN==32) illegal_instr = 1'b1;
                        end

                        3'b101: begin
                            if (instr.instr[31:26] == 6'b0)
                                instruction_o.op = apd_pkg::SRL;  // Shift Right Logical by Immediate
                            else if (instr.instr[31:26] == 6'b010_000)
                                instruction_o.op = apd_pkg::SRA;  // Shift Right Arithmetically by Immediate
                            else
                                illegal_instr = 1'b1;
                            if (instr.instr[25] != 1'b0 && riscv_pkg::XLEN==32) illegal_instr = 1'b1;
                        end
                    endcase
                end

                // --------------------------------
                // 32 bit Reg-Immediate Operations
                // --------------------------------
                riscv_pkg::OpcodeOpImm32: begin
                    instruction_o.fu  = ALU;
                    imm_select = IIMM;
                    instruction_o.rs1[4:0] = instr.itype.rs1;
                    instruction_o.rd[4:0]  = instr.itype.rd;
                    if (riscv_pkg::IS_XLEN64)
                    unique case (instr.itype.funct3)
                        3'b000: instruction_o.op = apd_pkg::ADDW;  // Add Immediate

                        3'b001: begin
                          instruction_o.op = apd_pkg::SLLW;  // Shift Left Logical by Immediate
                          if (instr.instr[31:25] != 7'b0)
                              illegal_instr = 1'b1;
                        end

                        3'b101: begin
                            if (instr.instr[31:25] == 7'b0)
                                instruction_o.op = apd_pkg::SRLW;  // Shift Right Logical by Immediate
                            else if (instr.instr[31:25] == 7'b010_0000)
                                instruction_o.op = apd_pkg::SRAW;  // Shift Right Arithmetically by Immediate
                            else
                                illegal_instr = 1'b1;
                        end

                        default: illegal_instr = 1'b1;
                    endcase
                    else illegal_instr = 1'b1;
                end
                // --------------------------------
                // LSU
                // --------------------------------
                riscv_pkg::OpcodeStore: begin
                    instruction_o.fu  = STORE;
                    imm_select = SIMM;
                    instruction_o.rs1[4:0]  = instr.stype.rs1;
                    instruction_o.rs2[4:0]  = instr.stype.rs2;
                    // determine store size
                    unique case (instr.stype.funct3)
                        3'b000: instruction_o.op  = apd_pkg::SB;
                        3'b001: instruction_o.op  = apd_pkg::SH;
                        3'b010: instruction_o.op  = apd_pkg::SW;
                        3'b011: if (riscv_pkg::XLEN==64) instruction_o.op  = apd_pkg::SD; 
                                else illegal_instr = 1'b1;
                        default: illegal_instr = 1'b1;
                    endcase
                end

                riscv_pkg::OpcodeLoad: begin
                    instruction_o.fu  = LOAD;
                    imm_select = IIMM;
                    instruction_o.rs1[4:0] = instr.itype.rs1;
                    instruction_o.rd[4:0]  = instr.itype.rd;
                    // determine load size and signed type
                    unique case (instr.itype.funct3)
                        3'b000: instruction_o.op  = apd_pkg::LB;
                        3'b001: instruction_o.op  = apd_pkg::LH;
                        3'b010: instruction_o.op  = apd_pkg::LW;
                        3'b100: instruction_o.op  = apd_pkg::LBU;
                        3'b101: instruction_o.op  = apd_pkg::LHU;
                        3'b110: instruction_o.op  = apd_pkg::LWU;
                        3'b011: if (riscv_pkg::XLEN==64) instruction_o.op  = apd_pkg::LD; 
                                else illegal_instr = 1'b1;
                        default: illegal_instr = 1'b1;
                    endcase
                end

                // --------------------------------
                // Floating-Point Load/store
                // --------------------------------
                riscv_pkg::OpcodeStoreFp: begin
                    illegal_instr = 1'b1;
                end

                riscv_pkg::OpcodeLoadFp: begin
                    illegal_instr = 1'b1;
                end

                // ----------------------------------
                // Floating-Point Reg-Reg Operations
                // ----------------------------------
                riscv_pkg::OpcodeMadd,
                riscv_pkg::OpcodeMsub,
                riscv_pkg::OpcodeNmsub,
                riscv_pkg::OpcodeNmadd: begin
                    illegal_instr = 1'b1;
                end

                riscv_pkg::OpcodeOpFp: begin
                    illegal_instr = 1'b1;
                end

                // ----------------------------------
                // Atomic Operations
                // ----------------------------------
                riscv_pkg::OpcodeAmo: begin                    
                    illegal_instr = 1'b1;
                end

                // --------------------------------
                // Control Flow Instructions
                // --------------------------------
                riscv_pkg::OpcodeBranch: begin
                    imm_select              = SBIMM;
                    instruction_o.fu        = CTRL_FLOW;
                    instruction_o.rs1[4:0]  = instr.stype.rs1;
                    instruction_o.rs2[4:0]  = instr.stype.rs2;

                    is_control_flow_instr_o = 1'b1;

                    case (instr.stype.funct3)
                        3'b000: instruction_o.op = apd_pkg::EQ;
                        3'b001: instruction_o.op = apd_pkg::NE;
                        3'b100: instruction_o.op = apd_pkg::LTS;
                        3'b101: instruction_o.op = apd_pkg::GES;
                        3'b110: instruction_o.op = apd_pkg::LTU;
                        3'b111: instruction_o.op = apd_pkg::GEU;
                        default: begin
                            is_control_flow_instr_o = 1'b0;
                            illegal_instr           = 1'b1;
                        end
                    endcase
                end
                // Jump and link register
                riscv_pkg::OpcodeJalr: begin
                    instruction_o.fu        = CTRL_FLOW;
                    instruction_o.op        = apd_pkg::JALR;
                    instruction_o.rs1[4:0]  = instr.itype.rs1;
                    imm_select              = IIMM;
                    instruction_o.rd[4:0]   = instr.itype.rd;
                    is_control_flow_instr_o = 1'b1;
                    // invalid jump and link register -> reserved for vector encoding
                    if (instr.itype.funct3 != 3'b0) illegal_instr = 1'b1;
                end
                // Jump and link
                riscv_pkg::OpcodeJal: begin
                    instruction_o.fu        = CTRL_FLOW;
                    imm_select              = JIMM;
                    instruction_o.rd[4:0]   = instr.utype.rd;
                    is_control_flow_instr_o = 1'b1;
                end

                riscv_pkg::OpcodeAuipc: begin
                    instruction_o.fu      = ALU;
                    imm_select            = UIMM;
                    instruction_o.use_pc  = 1'b1;
                    instruction_o.rd[4:0] = instr.utype.rd;
                end

                riscv_pkg::OpcodeLui: begin
                    imm_select            = UIMM;
                    instruction_o.fu      = ALU;
                    instruction_o.rd[4:0] = instr.utype.rd;
                end

                default: illegal_instr = 1'b1;
            endcase
        end
    end

    // --------------------------------
    // Sign extend immediate
    // --------------------------------
    always_comb begin : sign_extend
        imm_i_type  = { {riscv_pkg::XLEN-12{instruction_i[31]}}, instruction_i[31:20] };
        imm_s_type  = { {riscv_pkg::XLEN-12{instruction_i[31]}}, instruction_i[31:25], instruction_i[11:7] };
        imm_sb_type = { {riscv_pkg::XLEN-13{instruction_i[31]}}, instruction_i[31], instruction_i[7], instruction_i[30:25], instruction_i[11:8], 1'b0 };
        imm_u_type  = { {riscv_pkg::XLEN-32{instruction_i[31]}}, instruction_i[31:12], 12'b0 }; // JAL, AUIPC, sign extended to 64 bit
        imm_uj_type = { {riscv_pkg::XLEN-20{instruction_i[31]}}, instruction_i[19:12], instruction_i[20], instruction_i[30:21], 1'b0 };
        imm_bi_type = { {riscv_pkg::XLEN-5{instruction_i[24]}}, instruction_i[24:20] };

        // NOIMM, IIMM, SIMM, BIMM, UIMM, JIMM, RS3
        // select immediate
        case (imm_select)
            IIMM: begin
                instruction_o.result = imm_i_type;
                instruction_o.use_imm = 1'b1;
            end
            SIMM: begin
                instruction_o.result = imm_s_type;
                instruction_o.use_imm = 1'b1;
            end
            SBIMM: begin
                instruction_o.result = imm_sb_type;
                instruction_o.use_imm = 1'b1;
            end
            UIMM: begin
                instruction_o.result = imm_u_type;
                instruction_o.use_imm = 1'b1;
            end
            JIMM: begin
                instruction_o.result = imm_uj_type;
                instruction_o.use_imm = 1'b1;
            end
            RS3: begin
                // result holds address of fp operand rs3
                instruction_o.result = {{riscv_pkg::XLEN-5{1'b0}}, instr.r4type.rs3};
                instruction_o.use_imm = 1'b0;
            end
            default: begin
                instruction_o.result = {riscv_pkg::XLEN{1'b0}};
                instruction_o.use_imm = 1'b0;
            end
        endcase
    end

    // ---------------------
    // Exception handling
    // ---------------------
    riscv_pkg::xlen_t interrupt_cause;

    // this instruction has already executed if the exception is valid
    assign instruction_o.valid   = instruction_o.ex.valid;

    always_comb begin : exception_handling
        interrupt_cause       = '0;
        instruction_o.ex      = ex_i;
        // look if we didn't already get an exception in any previous
        // stage - we should not overwrite it as we retain order regarding the exception
        if (~ex_i.valid) begin
            // if we didn't already get an exception save the instruction here as we may need it
            // in the commit stage if we got a access exception to one of the CSR registers
            instruction_o.ex.tval  = (is_compressed_i) ? {{riscv_pkg::XLEN-16{1'b0}}, compressed_instr_i} : {{riscv_pkg::XLEN-32{1'b0}}, instruction_i};
            // instructions which will throw an exception are marked as valid
            // e.g.: they can be committed anytime and do not need to wait for any functional unit
            // check here if we decoded an invalid instruction or if the compressed decoder already decoded
            // a invalid instruction
            if (illegal_instr || is_illegal_i) begin
                instruction_o.ex.valid = 1'b1;
                // we decoded an illegal exception here
                instruction_o.ex.cause = riscv_pkg::ILLEGAL_INSTR;
            // we got an ecall, set the correct cause depending on the current privilege level
            end else if (ecall) begin
                // this exception is valid
                instruction_o.ex.valid = 1'b1;
                // depending on the privilege mode, set the appropriate cause
                case (priv_lvl_i)
                    riscv_pkg::PRIV_LVL_M: instruction_o.ex.cause = riscv_pkg::ENV_CALL_MMODE;
                    riscv_pkg::PRIV_LVL_S: instruction_o.ex.cause = riscv_pkg::ENV_CALL_SMODE;
                    riscv_pkg::PRIV_LVL_U: instruction_o.ex.cause = riscv_pkg::ENV_CALL_UMODE;
                    default:; // this should not happen
                endcase
            end else if (ebreak) begin
                // this exception is valid
                instruction_o.ex.valid = 1'b1;
                // set breakpoint cause
                instruction_o.ex.cause = riscv_pkg::BREAKPOINT;
            end
            // -----------------
            // Interrupt Control
            // -----------------
            // we decode an interrupt the same as an exception, hence it will be taken if the instruction did not
            // throw any previous exception.
            // we have three interrupt sources: external interrupts, software interrupts, timer interrupts (order of precedence)
            // for two privilege levels: Supervisor and Machine Mode
            // Supervisor Timer Interrupt
            if (irq_ctrl_i.mie[riscv_pkg::S_TIMER_INTERRUPT[$clog2(riscv_pkg::XLEN)-1:0]] && irq_ctrl_i.mip[riscv_pkg::S_TIMER_INTERRUPT[$clog2(riscv_pkg::XLEN)-1:0]]) begin
                interrupt_cause = riscv_pkg::S_TIMER_INTERRUPT;
            end
            // Supervisor Software Interrupt
            if (irq_ctrl_i.mie[riscv_pkg::S_SW_INTERRUPT[$clog2(riscv_pkg::XLEN)-1:0]] && irq_ctrl_i.mip[riscv_pkg::S_SW_INTERRUPT[$clog2(riscv_pkg::XLEN)-1:0]]) begin
                interrupt_cause = riscv_pkg::S_SW_INTERRUPT;
            end
            // Supervisor External Interrupt
            // The logical-OR of the software-writable bit and the signal from the external interrupt controller is
            // used to generate external interrupts to the supervisor
            if (irq_ctrl_i.mie[riscv_pkg::S_EXT_INTERRUPT[$clog2(riscv_pkg::XLEN)-1:0]] && (irq_ctrl_i.mip[riscv_pkg::S_EXT_INTERRUPT[$clog2(riscv_pkg::XLEN)-1:0]] | irq_i[apd_pkg::SupervisorIrq])) begin
                interrupt_cause = riscv_pkg::S_EXT_INTERRUPT;
            end
            // Machine Timer Interrupt
            if (irq_ctrl_i.mip[riscv_pkg::M_TIMER_INTERRUPT[$clog2(riscv_pkg::XLEN)-1:0]] && irq_ctrl_i.mie[riscv_pkg::M_TIMER_INTERRUPT[$clog2(riscv_pkg::XLEN)-1:0]]) begin
                interrupt_cause = riscv_pkg::M_TIMER_INTERRUPT;
            end
            // Machine Mode Software Interrupt
            if (irq_ctrl_i.mip[riscv_pkg::M_SW_INTERRUPT[$clog2(riscv_pkg::XLEN)-1:0]] && irq_ctrl_i.mie[riscv_pkg::M_SW_INTERRUPT[$clog2(riscv_pkg::XLEN)-1:0]]) begin
                interrupt_cause = riscv_pkg::M_SW_INTERRUPT;
            end
            // Machine Mode External Interrupt
            if (irq_ctrl_i.mip[riscv_pkg::M_EXT_INTERRUPT[$clog2(riscv_pkg::XLEN)-1:0]] && irq_ctrl_i.mie[riscv_pkg::M_EXT_INTERRUPT[$clog2(riscv_pkg::XLEN)-1:0]]) begin
                interrupt_cause = riscv_pkg::M_EXT_INTERRUPT;
            end

            if (interrupt_cause[riscv_pkg::XLEN-1] && irq_ctrl_i.global_enable) begin
                // However, if bit i in mideleg is set, interrupts are considered to be globally enabled if the hart’s current privilege
                // mode equals the delegated privilege mode (S or U) and that mode’s interrupt enable bit
                // (SIE or UIE in mstatus) is set, or if the current privilege mode is less than the delegated privilege mode.
                if (irq_ctrl_i.mideleg[interrupt_cause[$clog2(riscv_pkg::XLEN)-1:0]]) begin
                    if ((irq_ctrl_i.sie && priv_lvl_i == riscv_pkg::PRIV_LVL_S) || priv_lvl_i == riscv_pkg::PRIV_LVL_U) begin
                        instruction_o.ex.valid = 1'b1;
                        instruction_o.ex.cause = interrupt_cause;
                    end
                end else begin
                    instruction_o.ex.valid = 1'b1;
                    instruction_o.ex.cause = interrupt_cause;
                end
            end
        end
    end
endmodule
