
module apd 
import riscv_pkg::*; 
import apd_pkg::*;
#()
(
    input logic         clk_i,
    input logic         rst_ni,

    input logic [riscv_pkg::VLEN-1:0]  boot_addr_i,
    input logic [63:0]  hart_id_i
);
    
    icache_req_f2ic_t   fr2ic;
    icache_req_ic2f_t   ic2fr;
    inst_f2d_t          fr2id;
    logic               id_ready_id2fr;
    inst_id2is_t        id2is_fifo_in;
    logic               id2is_valid;
    inst_id2is_t        id2is_fifo_out;
    logic[apd_pkg::ISSUE_NUM-1:0] id2is_fifo_full;
    logic[apd_pkg::ISSUE_NUM-1:0] id2is_fifo_empty;
    logic[apd_pkg::ISSUE_NUM-1:0] id2is_fifo_pop_req;
    fu_data_t           is2ex_entry;
    logic               is2ex_entry_valid;
    logic               ex_ready;
    ex2wb_t             ex2wb;
    logic               commit_valid;
    apd_pkg::bp_resolve_t resolved_branch;
    logic               resolved_branch_valid;
    fu_data_t           csr_data;
    riscv_pkg::xlen_t   csr_result;
    logic               csr_flush; // exception or csr flush when some csrs are writed
    riscv_pkg::priv_lvl_t priv_lvl_csr; // Current privilege level the CPU is in
    exception_t           csr_exception_csr; // attempts to access a CSR without appropriate privilege
    logic [riscv_pkg::VLEN-1:0] csr_flush_pc;
    irq_ctrl_t             irq_ctrl_csr; // interrupt management to id stage
    logic                  tvm_csr; // trap virtual memory
    logic [riscv_pkg::VLEN-1:0] flush_pc;
    dcache_req_dc2lsu_t dc2lsu;
    dcache_req_lsu2dc_t lsu2dc;
    logic dcache_fence;
    logic sfence_vma_en;

    dcache_req_cc2mem_t cc2mem;
    dcache_req_mem2cc_t mem2cc;

    icache_req_f2mmu_t fr2mmu;
    icache_req_mmu2f_t mmu2fr;

    lsu_req_lsu2mmu_t  lsu2mmu;
    lsu_req_mmu2lsu_t  mmu2lsu;
    logic              en_ld_st_translation_csr; // enable VA translation for load and stores
    
    mmu_req_mem2mmu_t  mem2mmu;
    mmu_req_mmu2mem_t  mmu2mem;


    cache_perf_counter_t l1i2perf;
    cache_perf_counter_t l1d2perf;
    cache_perf_counter_t l22perf;
    // ---------------------
    // Front End
    // ---------------------
    logic flush; // TODO

    frontend #(
        .ApdCfg(apd_pkg::ApdDefaultConfig)
    ) frontend_u 
    (
        .clk_i          ( clk_i             ),
        .rst_ni         ( rst_ni            ),
        .flush_i        ( flush             ),// TODO
        .flush_pc_i     ( flush_pc          ), // TODO
        .flush_bp_i     ( '0),// TODO
        // global input
        .boot_addr_i    ( boot_addr_i       ),

        // To MMU
        .f2mmu_o        ( fr2mmu            ),
        .mmu2f_i        ( mmu2fr            ),

        // Instruction Fetch
        .f2ic_o         ( fr2ic             ),
        .ic2f_i         ( ic2fr             ),
        
        // to ID 
        .inst_entry_f2d_o ( fr2id               ),
        .inst_entry_d2f_ready_i ( id_ready_id2fr    )
    );
    
    // ---------------------
    // Instruction Cache
    // ---------------------
`ifndef SYNTHESIS
    rom_ideal #(
        .ROM_LINE_WIDTH  ( ApdDefaultConfig.ICacheWidth      ),
        .ROM_ROW_NUM     ( 4096 )
    ) rom_ideal_u (
        .clk_i          ( clk_i          ),
        .rst_ni         ( rst_ni         ),
        .flush_i        ( flush          ),       // TODO
        .mmu2mem_i      ( mmu2mem        ),        // TODO
        .mem2mmu_o      ( mem2mmu        ),
        .cc2mem_i       ( cc2mem         ), //TODO
        .mem2cc_o       ( mem2cc         ) //TODO
    );
`endif    
    // --------------------
    // Instruction Decoder
    // --------------------
    id_stage id_stage_u (
        .clk_i              ( clk_i             ),
        .rst_ni             ( rst_ni            ),
        .flush_i            ( flush             ),
        // from IF
        .inst_entry_f2d_i   ( fr2id             ),
        .inst_entry_d2f_ready_o( id_ready_id2fr ),
        // to IS TODO
        .issue_entry_id2issue_o ( id2is_fifo_in           ),
        .id2is_valid_o          ( id2is_valid             ),
        .issue_entry_id2issue_ready_i(~(|id2is_fifo_full) ),

        // from CSR
        .priv_lvl_i             ( priv_lvl_csr            ),
        .irq_ctrl_i             ( irq_ctrl_csr            ),
        .tvm_i                  ( tvm_csr                 )
    );


    // ---------------------------------
    // FIFO between ID stage and ISSUE
    // ---------------------------------
    genvar i;
    generate
        for(i = 0; i < apd_pkg::ISSUE_NUM; i++) begin
            fifo #(
                .FALL_THROUGH   ( 1'b0          ),
                .dtype          ( id_per_issue_t),
                .DEPTH          ( 4             )
            ) fifo_u (
                .clk_i              ( clk_i         ),
                .rst_ni             ( rst_ni        ),
                .flush_i            ( flush         ), // TODO
                .gate_clock_en      ( 1'b1          ),
                .full_o             ( id2is_fifo_full[i]        ),
                .empty_o            ( id2is_fifo_empty[i]       ), // TODO
                .data_i             ( id2is_fifo_in.issue_inst[i]         ),
                .push_i             ( id2is_valid                         ),
                .data_o             ( id2is_fifo_out.issue_inst[i]        ), // TODO
                .pop_i              ( id2is_fifo_pop_req[i]         )   // TODO
            );
        end
    endgenerate
    
    // -------------------
    // ISSUE stage TODO
    // -------------------
    is_stage is_stage_u (
        .clk_i                  ( clk_i                 ),
        .rst_ni                 ( rst_ni                ),
        .flush_i                ( flush                 ),
        
        // from ID2IS FIFO
        .id2is_fifo_pop_req_o   ( id2is_fifo_pop_req    ),
        .inst_entry_id2is_i     ( id2is_fifo_out        ),
        .fifo_empty_i           ( id2is_fifo_empty      ),
        
        // to EX
        .is2ex_o                ( is2ex_entry           ), 
        .is2ex_valid_o          ( is2ex_entry_valid     ),
        .ex_ready_i             ( ex_ready              ),
        
        // from EX to write back
        .ex2wb_i                ( ex2wb                 ),
        // from EX
        .commit_valid_i         ( commit_valid          )
    );
    // ---------------
    // EX stage
    // ---------------
    dcache_req_i_t [1:0] lsu2dc_mid; // 0:ld, 1:st
    dcache_req_dc2lsu_t  dc2lsu_mid; // 0:ld, 1:st
    
    
    ex_stage ex_stage_u (
        .clk_i              ( clk_i                 ),
        .rst_ni             ( rst_ni                ),
        .flush_i            ( flush                 ),
        
        // from ISSUE
        .is_entry_i         ( is2ex_entry           ),
        .is_entry_valid_i   ( is2ex_entry_valid     ),
        .ex_ready_o         ( ex_ready              ),
        
        // to COMMIT
        .ma_ready_i         ( '1                    ), //TODO
        // to CSR
        .csr_data_o         ( csr_data              ),
        .csr_result_i       ( csr_result            ),   // data from csr to write in gpr
        .csr_exception_i    ( csr_exception_csr     ),

        
        // to ISSUE for write back
        .ex2wb_o            ( ex2wb                 ),        
        // to ISSUE 
        .commit_valid_o     ( commit_valid          ),
        
        // to control for flush
        .resolved_branch_o  ( resolved_branch       ),
        .resolved_branch_valid_o ( resolved_branch_valid ),
        
        // mmu interface
        .lsu2mmu_o          ( lsu2mmu               ),
        .mmu2lsu_i          ( mmu2lsu               ),
        .sfence_vma_en_o    ( sfence_vma_en         ),
        .en_ld_st_translation_i ( en_ld_st_translation_csr ),

        // to/from memory
        .lsu2dc_o           ( lsu2dc                ),
        .dc_fence_o         ( dcache_fence          ),
        .dc2lsu_i           ( dc2lsu_mid            ), // its vaddr not used yet
        
        // for CSR
        .boot_addr_i        ( boot_addr_i           ),
        .hart_id_i          ( hart_id_i             )
    );
    
    // ------------
    // Data cache
    // ------------
    dcache_req_o_t   [1:0] dcache_req_ports;
    logic                  dcache_ready;
    logic                   dcache_flush_ack_o;     // send a single cycle acknowledge signal when the cache is flushed
    logic                   dcache_miss_o;          // we missed on a ld/st
    
    generate
      for(i = 0; i < 2; i++) begin
        assign lsu2dc_mid[i] = '{
          address:    lsu2dc.paddr,
          data_wdata: lsu2dc.data,
          req_valid:  lsu2dc.valid,
          data_we:    lsu2dc.rw_req,
          data_be:    lsu2dc.byte_mask
        };
      end
    endgenerate
    


    assign dc2lsu_mid = '{
      ready:    dcache_ready,
      valid:    dcache_req_ports[0].data_rvalid,// || dcache_req_ports[1].data_wgrant,
      data:     dcache_req_ports[0].data_rdata,
      paddr:    dcache_req_ports[1].data_wgrant ? dcache_req_ports[1].vaddr : dcache_req_ports[0].vaddr
    };
    
    cache_subsystem #(
      .ADDR_SIZE  ( riscv_pkg::PLEN       ),
      .LINE_BYTE  ( apd_pkg::BYTE_PER_LINE)
    ) cache_subsystem_u (
      .clk_i              ( clk_i               ),
      .rst_ni             ( rst_ni              ),
      .flush_i            ( flush               ),
      // dcache
      .dcache_enable_i    ( '1                  ),  // TODO, from CSR
      .dcache_flush_i     ( '0                  ),  // TODO, fence
      .dcache_fence_i     ( dcache_fence        ),
      .dcache_flush_ack_o,                          // TODO, fence
      .dcache_miss_o,                               // TODO, performance counter
      .dcache_req_ports_i ( lsu2dc_mid          ),
      .dcache_req_ports_o ( dcache_req_ports    ),
      .dcache_ready_o     ( dcache_ready        ),
      // icache
      .icache_enable_i    ( '1                  ),   // TODO, from CSR
      .icache_fence_i     ( '0                  ),   // TODO, fence.i
      .icache_req_i       ( fr2ic               ),
      .icache_req_o       ( ic2fr               ),
     // mem interface
      .cc2mem_o           ( cc2mem              ),
      .mem2cc_i           ( mem2cc              ),
     // performance counter
      .l1i2perf_o         ( l1i2perf            ),
      .l1d2perf_o         ( l1d2perf            ),
      .l22perf_o          ( l22perf             )
    );
    
    // -------------
    // COMMIT stage
    // -------------


    // ------------
    // CSR regfile
    // ------------
    logic                  flush_csr;
    logic                  halt_csr; // halt requested
    
    // level or to write  a read-only register also
    // raises illegal instruction exceptions.
    // Interrupts/Exceptions
    logic  [riscv_pkg::VLEN-1:0] epc_csr; // the exception PC to PC Gen; the correct CSR (mepc; sepc) is set accordingly
    logic                  eret_csr; // Return from exception; set the PC of epc_csr
    logic  [riscv_pkg::VLEN-1:0] trap_vector_base_csr; // base of exception vector; correct CSR is (mtvec; stvec)

    // MMU
    logic                  en_translation_csr; // enable VA translation
    riscv_pkg::priv_lvl_t      ld_st_priv_lvl_csr; // Privilege level at which load and stores should happen
    logic                  sum_csr;
    logic                  mxr_csr;
    logic[riscv_pkg::PPNW-1:0] satp_ppn_csr;
    logic [16-1:0] asid_csr;
    // Virtualization Support
    logic                  tw_csr; // timeout wait
    logic                  tsr_csr; // trap sret
    // Caches
    logic                  icache_en_csr; // L1 ICache Enable
    logic                  dcache_en_csr; // L1 DCache Enable
    // Performance Counter
    logic  [4:0]           perf_addr_csr; // read/write address to performance counter module (up to 29 aux counters possible in riscv encoding.h)
    logic[riscv_pkg::XLEN-1:0] perf_data_csr; // write data to performance counter module
    logic                  perf_we_csr;
    // PMPs
    riscv_pkg::pmpcfg_t [15:0] pmpcfg_csr; // PMP configuration containing pmpcfg for max 16 PMPs
    logic [15:0][riscv_pkg::PLEN-3:0] pmpaddr_csr; // PMP addresses
   
   
   
   csr_regfile #(
       .AsidWidth       ( riscv_pkg::ASIDW ),
       .NrCommitPorts   ( 2  ),
       .NrPMPEntries    ( 8  )
   ) csr_regfile_u (
       .clk_i           ( clk_i             ),
       .rst_ni          ( rst_ni            ),
       .time_irq_i      ( '0                ), 
       .flush_o         ( flush_csr           ), // TODO
       .halt_csr_o      ( halt_csr        ), // TODO, wfi
       .commit_ack_i    ( {1'b0, commit_valid}    ),
       .boot_addr_i     ( boot_addr_i       ),
       .hart_id_i       ( hart_id_i         ),
       .ex_i            ( csr_data.ex       ), 
       .csr_op_i        ( csr_data.operator ),
       .csr_addr_i      ( csr_data.operand_b[11:0]),
       .csr_wdata_i     ( csr_data.operand_a),
       .csr_rdata_o     ( csr_result        ),
       .pc_i            ( csr_data.pc       ),
       .csr_exception_o ( csr_exception_csr   ),
       .epc_o           ( epc_csr             ),
       .eret_o          ( eret_csr            ),
       .trap_vector_base_o(trap_vector_base_csr),
       .priv_lvl_o      ( priv_lvl_csr        ),
       .irq_ctrl_o      ( irq_ctrl_csr        ), // to ID stage
       .en_translation_o( en_translation_csr  ),
       .en_ld_st_translation_o(en_ld_st_translation_csr),
       .ld_st_priv_lvl_o( ld_st_priv_lvl_csr  ),
       .sum_o           ( sum_csr             ),
       .mxr_o           ( mxr_csr             ),
       .satp_ppn_o      ( satp_ppn_csr        ),
       .asid_o          ( asid_csr            ),
       .irq_i           ( '0                ), 
       .ipi_i           ( '0                ),
       .tvm_o           ( tvm_csr             ),
       .tw_o            ( tw_csr              ),
       .tsr_o           ( tsr_csr             ),
       .icache_en_o     ( icache_en_csr       ),
       .dcache_en_o     ( dcache_en_csr       ),
       .perf_addr_o     ( perf_addr_csr       ),
       .perf_data_o     ( perf_data_csr       ),
       .perf_data_i     ( '0                ), // TODO, performance counter input
       .perf_we_o       ( perf_we_csr         ),
       .pmpcfg_o        ( pmpcfg_csr          ),
       .pmpaddr_o       ( pmpaddr_csr         )
   );
    
    // CSR flush
    assign csr_flush      = eret_csr || (csr_data.fu_valid.csr_valid && csr_data.ex.valid); // exception or csr flush when some csrs are writed
    assign csr_flush_pc   = eret_csr ? epc_csr : trap_vector_base_csr;

    always @(*) begin
        if(csr_flush) begin
            // $display("exception, tvec = %x", csr_flush_pc);
        end
    end
    // ----------
    // Control
    // ----------
    control control_u (
        .clk_i  ( clk_i         ),
        .rst_ni ( rst_ni        ),
        .resolved_branch_i( resolved_branch   ),
        .resolved_branch_valid_i ( resolved_branch_valid ),
        .csr_flush_i        ( csr_flush             ), // exception or csr flush when some csrs are writed
        .csr_flush_pc_i     ( csr_flush_pc          ),
        .flush_o    ( flush     ),
        .flush_pc_o ( flush_pc  )
    );
    
    // ---------
    // mmu
    // ---------
    logic              itlb_miss;
    logic              dtlb_miss;
    
    mmu #(
      .INSTR_TLB_ENTRIES(16),
      .DATA_TLB_ENTRIES (16),
      .ASID_WIDTH       (ASID_WIDTH)
    ) mmu_t (
      .clk_i                            ( clk_i           ),
      .rst_ni                           ( rst_ni          ),
      .pipeline_flush_i                 ( flush           ), // pipeline flush, not flush tlb
      .enable_translation_i             ( en_translation_csr ), // TODO, csr
      .en_ld_st_translation_i           ( en_ld_st_translation_csr ), // TODO, csr
      // IF interface
      .icache_areq_i                    ( fr2mmu          ),
      .icache_areq_o                    ( mmu2fr          ),
      // LSU interface
      .misaligned_ex_i                  ( lsu2mmu.misaligned_ex ),
      .lsu_req_i                        ( lsu2mmu.lsu_req       ),
      .lsu_vaddr_i                      ( lsu2mmu.lsu_vaddr     ),
      .lsu_is_store_i                   ( lsu2mmu.lsu_is_store  ),
      .lsu_dtlb_hit_o                   ( mmu2lsu.lsu_dtlb_hit  ),
      .lsu_dtlb_ppn_o                   ( mmu2lsu.lsu_dtlb_ppn  ),
      .lsu_valid_o                      ( mmu2lsu.lsu_valid     ),
      .lsu_paddr_o                      ( mmu2lsu.lsu_paddr     ),
      .lsu_exception_o                  ( mmu2lsu.lsu_exception ),
      // General control signals from csr
      .priv_lvl_i                       ( priv_lvl_csr          ),
      .ld_st_priv_lvl_i                 ( ld_st_priv_lvl_csr    ),
      .sum_i                            ( sum_csr               ),
      .mxr_i                            ( mxr_csr               ),
      .satp_ppn_i                       ( satp_ppn_csr          ),
      .asid_i                           ( asid_csr              ),
      .asid_to_be_flushed_i             ( '0),//csr_data.operand_b    ), // TODO, SFENCE_VMA
      .vaddr_to_be_flushed_i            ( '0),//csr_data.operand_a    ), // TODO, SFENCE_VMA
      .flush_tlb_i                      ( sfence_vma_en         ), // TODO, SFENCE_VMA
      // Performance counters
      .itlb_miss_o                      ( itlb_miss             ), // TODO
      .dtlb_miss_o                      ( dtlb_miss             ), // TODO
      // PTW memory interface
      .req_port_i                       ( mem2mmu               ),
      .req_port_o                       ( mmu2mem               )//,
//      .pmpcfg_i                         ( '0  ), // TODO, pmp
//      .pmpaddr_i                        ( '0  ) // TODO, pmp
    );

    // ---------------------------
    // performance_counter
    // ---------------------------
    performance_counter performance_counter_u
    (
      .clk_i                            ( clk_i                 ),
      .flush_i                          ( flush                 ),
      .rst_ni                           ( rst_ni                ),

    // performance counter
      .l1i2perf_i         ( l1i2perf            ),
      .l1d2perf_i         ( l1d2perf            ),
      .l22perf_i          ( l22perf             )

    );

endmodule