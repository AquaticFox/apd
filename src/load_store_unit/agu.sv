
module agu
import riscv_pkg::*;
import apd_pkg::*;
#()
(
    input  riscv_pkg::xlen_t             operand_a,
    input  riscv_pkg::xlen_t             imm,
    
    output logic [riscv_pkg::VLEN-1:0]   vaddr_o,
    output logic                         overflow_o
);
    riscv_pkg::xlen_t             vaddr_xlen;
    
    assign vaddr_xlen = $unsigned($signed(imm) + $signed(operand_a));
    assign vaddr_o    = vaddr_xlen[riscv_pkg::VLEN-1:0];
    
    // we work with SV39 or SV32, so if VM is enabled, check that all bits [XLEN-1:38] or [XLEN-1:31] are equal
    assign overflow_o = !((&vaddr_xlen[riscv_pkg::XLEN-1:riscv_pkg::SV-1]) == 1'b1 || (|vaddr_xlen[riscv_pkg::XLEN-1:riscv_pkg::SV-1]) == 1'b0);
    
endmodule