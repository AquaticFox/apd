
module fifo #(
    parameter bit          FALL_THROUGH = 1'b0, // fifo is in fall-through mode
    parameter type         dtype        = logic [32-1:0],
    parameter int unsigned DATA_WIDTH   = $bits(dtype),   // default data width if the fifo is of type logic
    parameter int unsigned DEPTH        = 8,    // depth can be arbitrary from 0 to 2**32
    // DO NOT OVERWRITE THIS PARAMETER
    parameter int unsigned ADDR_DEPTH   = (DEPTH > 1) ? $clog2(DEPTH) : 1,
    parameter int unsigned FIFO_DEPTH   = (DEPTH > 0) ? DEPTH : 1 // FIFO depth - handle the case of pass-through, synthesizer will do constant propagation
)(
    input  logic            clk_i,
    input  logic            rst_ni,
    input  logic            flush_i,
    input  logic            gate_clock_en,
    // status flags
    output logic            full_o,
    output logic            empty_o,
    // data push in
    input  dtype            data_i,
    input  logic            push_i,
    // data pop out
    output dtype            data_o,
    input  logic            pop_i
);

    // gate clock control
    logic gate_clock;
    // read pointer
    logic [ADDR_DEPTH - 1:0] read_pointer_d, read_pointer_q;
    // write pointer
    logic [ADDR_DEPTH - 1:0] write_pointer_d, write_pointer_q;
    // free entry count
    logic [ADDR_DEPTH:0]     free_cnt_d, free_cnt_q;
    // actual memory
    dtype [FIFO_DEPTH - 1:0] mem_d, mem_q;
    
    // output status
    logic free_cnt_full_en;
    assign free_cnt_full_en = (free_cnt_q == FIFO_DEPTH[ADDR_DEPTH:0]);
    if(DEPTH == 0) begin
        assign empty_o = ~push_i;
        assign full_o  = ~pop_i;
    end else begin
        assign full_o  = (free_cnt_q == '0);
        assign empty_o = (free_cnt_full_en) & ~(FALL_THROUGH & push_i);
    end
    
    logic push_en;
    logic pop_en;
    logic push_pop_en;
    logic fall_through_mode_en;
    assign push_en      = push_i & ~full_o;
    assign pop_en       = pop_i & ~empty_o;
    assign push_pop_en  = push_en & pop_en;
    assign fall_through_mode_en = FALL_THROUGH && free_cnt_full_en && push_i;
    
    // output logic
    assign data_o = ((DEPTH == 0) | fall_through_mode_en) ? data_i : mem_q[read_pointer_q];
    
    // read and write combinational logic
    always_comb begin : read_write_comb
        mem_d           = mem_q;
        read_pointer_d  = read_pointer_q;
        write_pointer_d = write_pointer_q;
        free_cnt_d      = free_cnt_q;
        gate_clock      = 1'b1;
        
        // push an element
        if(push_en) begin
            // data in
            mem_d[write_pointer_q] = data_i;
            // increment write pointer
            if(write_pointer_q == FIFO_DEPTH[ADDR_DEPTH:0] - 1) begin
                write_pointer_d = '0;
            end else begin
                write_pointer_d = write_pointer_q + 1;
            end
            // decrement free_cnt
            free_cnt_d = free_cnt_q - 1;
            // un-gate the clk to write fifo 
            gate_clock = 1'b0;
        end
        
        // pop an element
        if(pop_en) begin
            // data_out is not handle here
            // increment read pointer
            if(read_pointer_q == FIFO_DEPTH[ADDR_DEPTH:0] - 1) begin
                read_pointer_d = '0;
            end else begin
                read_pointer_d = read_pointer_q + 1;
            end
            // increment free_cnt
            free_cnt_d = free_cnt_q + 1;
        end
        
        if(push_pop_en) begin
            free_cnt_d = free_cnt_q;
        end
        
        if(fall_through_mode_en & pop_i) begin
            read_pointer_d  = read_pointer_q;
            write_pointer_d = write_pointer_q;
            free_cnt_d      = free_cnt_q;
            // gate the clk, as it is poped at once, no need to write fifo 
            gate_clock = 1'b0;
        end
    end
     
    
    // sequential logic
    always_ff @(posedge clk_i or negedge rst_ni) begin : pointer_ff
        if(!rst_ni) begin
            read_pointer_q  <= '0;
            write_pointer_q <= '0;
            free_cnt_q      <= FIFO_DEPTH[ADDR_DEPTH:0];
        end else begin
            if(flush_i) begin
                read_pointer_q  <= '0;
                write_pointer_q <= '0;
                free_cnt_q      <= FIFO_DEPTH[ADDR_DEPTH:0];  
            end else begin
                read_pointer_q  <= read_pointer_d;
                write_pointer_q <= write_pointer_d;
                free_cnt_q      <= free_cnt_d;
            end
        end
    end // pointer_ff
    
    always_ff @(posedge clk_i or negedge rst_ni) begin : fifo_ff
       if(!rst_ni) begin
           mem_q <= '0;
       end else if(!(gate_clock & gate_clock_en)) begin
           mem_q <= mem_d;
       end
    end // fifo_ff

    //synthesis translate_off
    //buffer pointer assertions go here
    `ifndef SYNTHESIS
    `ifndef VERILATOR
    chk_inst_re_i: assert property (@(posedge clk_i) disable iff (!rst_ni) (flush_i | !rst_ni) |-> !pop_en) else $error("%m: fifo is not supposed to be read during flush_i or !rst_ni");
    
    chk_free_cnt: assert property (@(posedge clk_i) disable iff (!rst_ni) free_cnt_q <= FIFO_DEPTH) else $error("%m: fifo credit calculation overflow");
    
    chk_overflow: assert property (@(posedge clk_i) disable iff (!rst_ni) push_en |-> (read_pointer_q != write_pointer_q || free_cnt_q == FIFO_DEPTH)) else $error("%m: fifo overflow");
    `endif // VERILATOR
    `endif // SYNTHESIS
    
    //synthesis translate_on

endmodule