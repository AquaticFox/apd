module control import apd_pkg::*; (
    input  logic            clk_i,
    input  logic            rst_ni,
    
    input  bp_resolve_t     resolved_branch_i,
    input  logic            resolved_branch_valid_i,

    input  logic                csr_flush_i, // exception or csr flush when some csrs are writed
    input  logic [riscv_pkg::VLEN-1:0] csr_flush_pc_i,
    
    output logic            flush_o,
    output logic [riscv_pkg::VLEN-1:0]      flush_pc_o // to frontend
);
    logic flush_d, flush_q;
    logic [riscv_pkg::VLEN-1:0] flush_pc_q;
    
    assign flush_o = flush_q;
    
    always_comb begin : judge_misprediction
        flush_d = 1'b0;
        if(resolved_branch_i.is_mispredict && resolved_branch_i.valid && (flush_pc_q != resolved_branch_i.target_address) && resolved_branch_valid_i) begin
           flush_d      = 1'b1;
           flush_pc_o   = resolved_branch_i.target_address;
        end    

        if(csr_flush_i && (flush_pc_q != csr_flush_pc_i) ) begin
           flush_d      = 1'b1;
           flush_pc_o   = csr_flush_pc_i;
        end
    end
    
    always_ff @(posedge clk_i or negedge rst_ni) begin : flush_ff
        if(~rst_ni) begin
            flush_q <= 0;
        end else begin
            flush_q <= flush_d;
        end
    end
    
    always_ff @(posedge clk_i or negedge rst_ni) begin : flush_once_ff
       if(!rst_ni) begin
            flush_pc_q <= '0;
       end else if(flush_q) begin
            flush_pc_q <= resolved_branch_i.target_address;
       end else begin
            flush_pc_q <= '0;
       end
    end

endmodule