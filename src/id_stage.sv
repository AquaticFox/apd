
module id_stage import apd_pkg::*; #(
)(
    input  logic                clk_i,
    input  logic                rst_ni,
    input  logic                flush_i,
    
    // from IF
    input  inst_f2d_t           inst_entry_f2d_i,
    output logic                inst_entry_d2f_ready_o,
    // to ISSUE
    output inst_id2is_t         issue_entry_id2issue_o,
    output logic                id2is_valid_o, // enable when it is a data that want to push into the fifo
    input  logic                issue_entry_id2issue_ready_i,

    // from CSR
    input   riscv_pkg::priv_lvl_t priv_lvl_i,
    input   irq_ctrl_t            irq_ctrl_i,
    input   logic                 tvm_i
);
    logic [apd_pkg::ISSUE_NUM-1:0][31:0]    inst_c2i; // instruction compressed inst to I inst
    logic [apd_pkg::ISSUE_NUM-1:0]          is_illegal_c2i;
    
    scoreboard_entry_t [apd_pkg::ISSUE_NUM-1:0] inst_decoded;
    logic              [apd_pkg::ISSUE_NUM-1:0] is_control_flow_inst;
    
    // ---------------
    // 1. rvc -> rvi
    // ---------------
    genvar i;
    generate
        for(i = 0; i < apd_pkg::ISSUE_NUM; i++) begin
            decoder_compressed decoder_compressed_u (
                .instr_i            ( inst_entry_f2d_i.issue_inst[i].inst       ),
                .instr_o            ( inst_c2i[i]                               ),
                .is_illegal_instr_o ( is_illegal_c2i[i]                         )
            );
        end
    endgenerate
    // ---------------
    // 2. rvi decoder
    // ---------------
    generate
        for(i = 0; i < apd_pkg::ISSUE_NUM; i++) begin
            decoder decoder_u (
                .pc_i               ( inst_entry_f2d_i.issue_inst[i].addr       ),
                .is_compressed_i    ( inst_entry_f2d_i.issue_inst[i].is_rvc     ),
                .compressed_instr_i ( inst_entry_f2d_i.issue_inst[i].inst[15:0] ),
                .is_illegal_i       ( is_illegal_c2i[i]                         ),
                .instruction_i      ( inst_c2i[i]                               ),
                .branch_predict_i   ( '0                                        ), // TODO
                .ex_i               ( inst_entry_f2d_i.issue_inst[i].ex         ), // TODO
                .irq_i              ( '0                                        ), // TODO
                .irq_ctrl_i         ( irq_ctrl_i                                ), 
                .priv_lvl_i         ( priv_lvl_i                                ), 
                .debug_mode_i       ( '0                                        ), // TODO
                .tvm_i              ( tvm_i                                     ), // trap vm TODO
                .tw_i               ( '0                                        ), // WFI TODO
                .tsr_i              ( '0                                        ), // TODO
                .instruction_o      ( inst_decoded[i]                           ),
                .is_control_flow_instr_o ( is_control_flow_inst[i]              ) // enable when have: branch, jal, jalr
            );
        end
    endgenerate
    
    // ---------------
    // Pipeline register
    // ---------------
    inst_id2is_t    issue_entry_d, issue_entry_q;
    logic           update_en; // enable if this cycle has at least 1 valid inst, and this stage registers have space to store it
    logic           push_done_d, push_done_q;
    
    assign update_en                = inst_entry_f2d_i.issue_inst[0].valid && issue_entry_id2issue_ready_i;
    assign inst_entry_d2f_ready_o   = update_en && !flush_i;
    
    for(i = 0; i < apd_pkg::ISSUE_NUM; i++) begin
        assign issue_entry_d.issue_inst[i].sbe                      = update_en ? inst_decoded[i] : issue_entry_q.issue_inst[i].sbe;
        assign issue_entry_d.issue_inst[i].is_ctrl_flow             = update_en ? is_control_flow_inst[i] : issue_entry_q.issue_inst[i].is_ctrl_flow;
        assign issue_entry_d.issue_inst[i].valid                    = !flush_i && (update_en ? inst_entry_f2d_i.issue_inst[i].valid : issue_entry_q.issue_inst[i].valid);
    end
    
    always_ff @(posedge clk_i or negedge rst_ni) begin : issue_entry_q_update
       if(!rst_ni) begin
            issue_entry_q                <= '0;
       end else begin
            issue_entry_q               <= issue_entry_d;
       end
   end
   
   assign push_done_d = !update_en && ( push_done_q || (issue_entry_id2issue_ready_i && id2is_valid_o));
   
   always_ff @(posedge clk_i or negedge rst_ni) begin : push_done_q_update
       if(!rst_ni) begin
            push_done_q               <= '0;
       end else begin
            push_done_q               <= push_done_d;
       end
   end
   
   // ----------------------------------------------
   // FSM to ensure the pushed entries are latest
   // ----------------------------------------------
   enum {IDLE, BUSY} ib_state_d, ib_state_q; // state 0: idle, 1: fetching a line
   
   // --------------
   // Output
   // --------------
   assign issue_entry_id2issue_o    = issue_entry_q;
   
   // TODO, use the ready signal to ensure that the dual instructions can be put into both fifo at the same time
   assign id2is_valid_o             = issue_entry_q.issue_inst[0].valid && issue_entry_id2issue_ready_i && !push_done_q;
   
endmodule