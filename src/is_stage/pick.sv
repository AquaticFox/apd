
module pick import apd_pkg::*; #(
)(
  input  logic                clk_i,
  input  logic                rst_ni,
  input  logic                flush_i,
    
  // from id
  output logic[apd_pkg::ISSUE_NUM-1:0]    pick_ready_o,
  input  inst_id2is_t                     inst_entry_id2is_i,
  input  logic[apd_pkg::ISSUE_NUM-1:0]    fifo_empty_i,
    
  // to issue
  output id_per_issue_t       pick_entry_o,
  output logic                pick_entry_valid_o,
  input logic                 is_ready_i
  

);
    localparam FRONTEND_ISSUE_NUM_WIDTH = apd_pkg::ISSUE_NUM ? $clog2(apd_pkg::ISSUE_NUM) : 1;
    
    logic[FRONTEND_ISSUE_NUM_WIDTH-1:0] id2is_ptr_d, id2is_ptr_q_plus1, id2is_ptr_q, id2is_ptr_output;
    logic id2is_valid;
    logic first_entry_read;
    assign id2is_ptr_q_plus1 = id2is_ptr_q + 1;
    logic [apd_pkg::ISSUE_NUM-1:0] valid_2;
    assign valid_2[0] = inst_entry_id2is_i.issue_inst[0].valid;
    assign valid_2[1] = inst_entry_id2is_i.issue_inst[1].valid;
    logic valid_3, valid_4;
    assign valid_3 = valid_2[id2is_ptr_q] && !fifo_empty_i[id2is_ptr_q] /*&& is_ready_i*/;
    assign valid_4 = valid_2[id2is_ptr_q_plus1] && !fifo_empty_i[id2is_ptr_q_plus1] /*&& is_ready_i*/; //&& first_entry_read;
    always_comb begin : id2is_ptr_d_comb
       pick_ready_o       = '0;
       id2is_ptr_d = id2is_ptr_q;
       id2is_valid = 1'b0;
       id2is_ptr_output = '0;
       if(valid_3) begin // the instruction to issue is valid
           pick_ready_o[id2is_ptr_q]   = is_ready_i;
           id2is_ptr_d          = id2is_ptr_q_plus1;
           id2is_valid          = 1'b1;
           id2is_ptr_output     = id2is_ptr_q;
       end else if(valid_4) begin
           pick_ready_o[id2is_ptr_q]       = is_ready_i;
           pick_ready_o[id2is_ptr_q_plus1] = is_ready_i;
           id2is_ptr_d              = id2is_ptr_q;
           id2is_valid              = 1'b1;
           id2is_ptr_output         = id2is_ptr_q_plus1;
       end
       
       if(flush_i) begin
           pick_ready_o = '0;
       end
   end

   
  //  always_ff @(posedge clk_i or negedge rst_ni) begin
  //    if(!rst_ni) begin
  //      first_entry_read <= 1'b0;
  //    end else if(flush_i) begin
  //      first_entry_read <= 1'b0;
  //    end else if(id2is_ptr_q == '0 && id2is_valid == 1'b1) begin
  //      first_entry_read <= 1'b1;
  //    end
  //  end
   
   always_ff @(posedge clk_i or negedge rst_ni) begin : id2is_ptr_q_update_ff
      if(!rst_ni) begin
        id2is_ptr_q <= '0;
      end else if (flush_i) begin
        id2is_ptr_q <= '0;
      end else if(is_ready_i) begin
        id2is_ptr_q <= id2is_ptr_d;
      end
  end
  
  // output
  assign pick_entry_o       = inst_entry_id2is_i.issue_inst[id2is_ptr_output];
  assign pick_entry_valid_o = inst_entry_id2is_i.issue_inst[id2is_ptr_output].valid && id2is_valid;

endmodule