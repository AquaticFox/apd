// record what is in the ex stage ,resolve the data dependency
module issue import apd_pkg::*; #(
)(
  input  logic                clk_i,
  input  logic                rst_ni,
  input  logic                flush_i,
  
  // from is pick
  input  id_per_issue_t       pick_entry_i,
  input  logic                pick_entry_valid_i,
  output logic                is_ready_o,
  
  // to ex
  output fu_data_t            is2ex_o,
  output logic                is2ex_valid_o,
  input  logic                ex_ready_i,
  
  // from ex for wb
  input ex2wb_t               ex2wb_i,
  
  // from ex
  input  logic                commit_valid_i
);
    // data hazard
    issue_entry_t the_inst_at_ex;
    logic the_inst_at_is_rs1_en;
    logic the_inst_at_is_rs2_en;
    logic the_inst_at_is_rd_en;
    logic operand_a_pc_en, operand_a_zimm_en, operand_b_imm_en;
    
    assign operand_a_pc_en      = pick_entry_i.sbe.use_pc;
    assign operand_a_zimm_en    = pick_entry_i.sbe.use_zimm;
    assign operand_b_imm_en     = pick_entry_i.sbe.use_imm && (pick_entry_i.sbe.fu != STORE) && (pick_entry_i.sbe.fu != CTRL_FLOW);
    
    assign the_inst_at_is_rs1_en =  !operand_a_pc_en && !operand_a_zimm_en;
    assign the_inst_at_is_rs2_en =  !operand_b_imm_en;
    assign the_inst_at_is_rd_en  =  !op_is_branch (pick_entry_i.sbe.op)
                                 && !(pick_entry_i.sbe.fu == STORE);
    
    fu_data_t is2ex_d;
    logic issue_valid_d;
    logic the_inst_at_ex_rd_not_x0_en;
    logic rs1_pending_en, rs2_pending_en;
    logic no_data_hazard_en;

    assign the_inst_at_ex_rd_not_x0_en = |ex2wb_i.ex_rd_q[4:0];
    assign rs1_pending_en = (pick_entry_i.sbe.rs1 == ex2wb_i.ex_rd_q) && the_inst_at_ex_rd_not_x0_en;
    assign rs2_pending_en = (pick_entry_i.sbe.rs2 == ex2wb_i.ex_rd_q) && the_inst_at_ex_rd_not_x0_en;
    assign no_data_hazard_en = !ex2wb_i.ex_rd_en_q || (!rs1_pending_en || !the_inst_at_is_rs1_en) && (!rs2_pending_en || !the_inst_at_is_rs2_en);
    
    // issue control
    assign issue_valid_d = pick_entry_valid_i 
                      && no_data_hazard_en
                      && !flush_i;
//                      && ex_ready_i;
//                      && !(commit_valid_i && (the_inst_at_ex.fu inside { MULT }));
    
    

    
    logic [riscv_pkg::VLEN-1:0] wb_pc;
    always_ff @(posedge clk_i or negedge rst_ni) begin
        if(!rst_ni) begin
            wb_pc <= 'h80000000;
        end else if(commit_valid_i) begin
            wb_pc <= ex2wb_i.pc;
        end
    end

    // --------------
    // register file
    // --------------
    // read
    logic [1:0][riscv_pkg::XLEN-1:0] rdata;
    logic [riscv_pkg::XLEN-1:0] operand_a_regfile, operand_b_regfile;
    logic [1:0][4:0]  raddr_pack;
    assign raddr_pack = {pick_entry_i.sbe.rs2[4:0], pick_entry_i.sbe.rs1[4:0]};
    assign operand_a_regfile = rdata[0];
    assign operand_b_regfile = rdata[1];
    
    //write
    logic [BACK_END_ISSUE_NUM-1:0][4:0] waddr;
    logic [BACK_END_ISSUE_NUM-1:0]      we;
    assign waddr = ex2wb_i.wb_rd;
    assign we    = ex2wb_i.wb_valid; //the_inst_at_ex.rd_en && wb_valid_i && !fifo_empty;
    
    regfile #(
        .DATA_WIDTH     ( riscv_pkg::XLEN ),
        .NR_READ_PORTS  ( 2               ),
        .NR_WRITE_PORTS ( BACK_END_ISSUE_NUM ),
        .ZERO_REG_ZERO  ( 1               )
    ) regfile_u (
        .clk_i      ( clk_i         ),
        .rst_ni     ( rst_ni        ),
        .raddr_i    ( raddr_pack    ),
        .rdata_o    ( rdata         ),
        .waddr_i    ( waddr         ), //TODO
        .wdata_i    ( ex2wb_i.wb_data), //TODO
        .we_i       ( we            ) // TODO
    );
    
    // Output MUX (is2ex_d)
    assign is2ex_d.imm      = pick_entry_i.sbe.result;
    assign is2ex_d.operator = pick_entry_i.sbe.op;
    assign is2ex_d.pc       = pick_entry_i.sbe.pc;
    assign is2ex_d.fu       = pick_entry_i.sbe.fu;
    assign is2ex_d.is_rvc   = pick_entry_i.sbe.is_compressed;
    assign is2ex_d.bp       = pick_entry_i.sbe.bp;
    assign is2ex_d.rd       = pick_entry_i.sbe.rd;
    assign is2ex_d.rd_en    = the_inst_at_is_rd_en;
    assign is2ex_d.ex       = pick_entry_i.sbe.ex;
    always_comb begin : operand_select
        // default is regfiles
        is2ex_d.operand_a = operand_a_regfile;
        is2ex_d.operand_b = operand_b_regfile;

        // use the PC as operand a
        if (operand_a_pc_en) begin
            is2ex_d.operand_a = {{riscv_pkg::XLEN-riscv_pkg::VLEN{pick_entry_i.sbe.pc[riscv_pkg::VLEN-1]}}, pick_entry_i.sbe.pc};
        end

        // use the zimm as operand a
        if (operand_a_zimm_en) begin
            // zero extend operand a
            is2ex_d.operand_a = {{riscv_pkg::XLEN-5{1'b0}}, pick_entry_i.sbe.rs1[4:0]};
        end
        // or is it an immediate (including PC), this is not the case for a store and control flow instructions
        if (operand_b_imm_en) begin
            is2ex_d.operand_b = pick_entry_i.sbe.result;
        end
    end
    
    // always @(*)begin
    //     $display("time: %d, is2ex_d.operand_a = %h, lsu_data.operand_a = %h, lsu_data.imm = %h", 
    //         $time(), is2ex_d.operand_a, lsu_data.operand_a, lsu_data.imm);
    // end

    always_comb begin : fu_select
        is2ex_d.fu_valid = '0;
        if (pick_entry_valid_i && issue_valid_d) begin
            if(!pick_entry_i.sbe.ex.valid) begin
                case (pick_entry_i.sbe.fu)
                    ALU:
                        is2ex_d.fu_valid.alu_valid    = 1'b1;
                    CTRL_FLOW:
                        is2ex_d.fu_valid.branch_valid = 1'b1;
                    MULT:
                        is2ex_d.fu_valid.mult_valid   = 1'b1;
                    LOAD, STORE:
                        is2ex_d.fu_valid.lsu_valid    = 1'b1;
                    CSR:
                        is2ex_d.fu_valid.csr_valid    = 1'b1;
                    default:;
                endcase
            end else begin
                is2ex_d.fu_valid = '{
                    default   : 0,
                    csr_valid : 1
                };
            end
        end


    end
    

    
    // output
    assign is2ex_valid_o = issue_valid_d;
    assign is2ex_o       = is2ex_d;
    assign is_ready_o    = (ex_ready_i && no_data_hazard_en) && !flush_i;
    
    
endmodule
