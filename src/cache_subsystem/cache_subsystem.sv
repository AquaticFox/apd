// `define UNCACHED
`undef UNCACHED
module cache_subsystem 
  import riscv_pkg::*;
  import apd_pkg::*;
#(
  parameter ADDR_SIZE      = riscv_pkg::PLEN, 
  parameter LINE_BYTE      = apd_pkg::BYTE_PER_LINE,
  parameter BYTE_LEN       = 8,
  parameter DATA_WIDTH     = LINE_BYTE*BYTE_LEN // line = 256
)
(
  input logic                            clk_i,
  input logic                            rst_ni,
  input logic                            flush_i,
//   // I$ ------
//   input  logic                           icache_enable_i,        // enable icache (or bypass e.g: in debug mode)
//   input  logic                           icache_flush_i,         // flush the icache, flush and kill have to be asserted together
//   output logic                           icache_miss_o,          // to performance counter
//   // address translation requests
//   input  icache_areq_i_t                 icache_areq_i,          // to/from frontend
//   output icache_areq_o_t                 icache_areq_o,
//   // data requests
//   input  icache_dreq_i_t                 icache_dreq_i,          // to/from frontend
//   output icache_dreq_o_t                 icache_dreq_o,
  // D$ ------
  // Cache management
  input  logic                           dcache_enable_i,        
  input  logic                           dcache_flush_i,         // high until acknowledged
  input  logic                           dcache_fence_i,
  output logic                           dcache_flush_ack_o,     // send a single cycle acknowledge signal when the cache is flushed
  output logic                           dcache_miss_o,          // we missed on a ld/st
  // Request ports
  input  dcache_req_i_t   [1:0]          dcache_req_ports_i,     // to/from LSU // 0:ld, 1:st
  output dcache_req_o_t   [1:0]          dcache_req_ports_o,     // to/from LSU // 0:ld, 1:st
  output logic                           dcache_ready_o,

  // I$ ------
  // Cache management
  input  logic                           icache_enable_i,        
  input  logic                           icache_fence_i,         // fence.i
  // Request ports
  input  icache_req_f2ic_t               icache_req_i,
  output icache_req_ic2f_t               icache_req_o,

  // memory side // TODO axi interfice
//   output apd_axi_pkg::req_t             axi_req_o,
//   input  apd_axi_pkg::resp_t            axi_resp_i
  output dcache_req_cc2mem_t           cc2mem_o,
  input  dcache_req_mem2cc_t           mem2cc_i,

  // performance counter
  output cache_perf_counter_t l1i2perf_o,
  output cache_perf_counter_t l1d2perf_o,
  output cache_perf_counter_t l22perf_o
);

    // to l2 interface
    logic                       l2_wb_en;
    logic [ADDR_SIZE-1:0]       l2_wb_addr;
    logic [DATA_WIDTH-1:0]      l2_wb_data;
    logic                       l2_rm_en;
    logic [ADDR_SIZE-1:0]       l2_rm_addr;
    
  
    // Uncached
    logic  uncached_addr_range_en; //TODO

`ifndef UNCACHED
    assign uncached_addr_range_en = 0;
`else  
    assign uncached_addr_range_en = 1;
`endif
    
    // Control signal
    logic  cpu_ld_en;
    logic  cpu_st_en;
    logic  l1_ld_rd_en;
    logic  l1_st_wt_en;
    logic  dc_req_valid;
    logic  dc_inst_finish_en, ic_inst_finish_en, l2_inst_finish_en;
    logic  dc_req_l2_valid, ic_req_l2_valid;
    dcache_req_cc2mem_t l1_to_l2, l1_to_l2_q, l1_to_l2_select;
    dcache_req_mem2cc_t l2_to_l1;

    assign cpu_ld_en          = dcache_req_ports_i[0].req_valid && !dcache_req_ports_i[0].data_we && !dcache_fence_i;
    assign cpu_st_en          = dcache_req_ports_i[1].req_valid &&  dcache_req_ports_i[1].data_we && !dcache_fence_i;
    assign l1_ld_rd_en        = cpu_ld_en && !uncached_addr_range_en;
    assign l1_st_wt_en        = cpu_st_en && !uncached_addr_range_en;
    assign dc_req_valid       = l1_ld_rd_en || l1_st_wt_en;
    assign dc_inst_finish_en  = (dc_req_valid ? (dcache_req_ports_o[0].data_rvalid || dcache_req_ports_o[1].data_wgrant) : '1);// || (top_u.dut.csr_regfile_u.cycle_q == 'h8a04e);
    assign ic_inst_finish_en  = icache_req_i.req ? icache_req_o.valid : '1;
    assign l2_inst_finish_en  = l1_to_l2.valid ? (l1_to_l2.rw_req ? l2_to_l1.ready : l2_to_l1.valid) : '1;

    logic  l1_hit_en;
    logic  l1_hit_en_last;
    logic  l1_rd_miss_en;
    logic  l1_wt_miss_en;
    
    assign l1_hit_en         = ((l1_ld_rd_en && !l1_rd_miss_en) || (l1_st_wt_en && !l1_wt_miss_en)) && (uncached_addr_range_en ? !l1_hit_en_last : '1);
    always_ff @(posedge clk_i or negedge rst_ni) begin
      if(!rst_ni)
        l1_hit_en_last <= '0;
      else
        l1_hit_en_last <= l1_hit_en;
    end

  // ------------
  // D cache
  // ------------

    // data to write
    cc_data_t           cc_write_data;
    cc_byte_mask_t      cc_write_mask;
    logic[DCACHE_LINE_WIDTH-1:0] cc_read_data;
    dcache_req_i_t      dcache_req_data; // load have higher priority
    assign dcache_req_data = l1_ld_rd_en ? dcache_req_ports_i[0] : dcache_req_ports_i[1];
    
    always_comb begin
      unique case (dcache_req_data.address[5:3])
        3'b000: begin
          cc_write_data = {448'h0, dcache_req_data.data_wdata};
          cc_write_mask = {56'h0,  dcache_req_data.data_be};
        end
        3'b001: begin
          cc_write_data = {384'h0, dcache_req_data.data_wdata, 64'h0};
          cc_write_mask = {48'h0, dcache_req_data.data_be, 8'h0};
        end
        3'b010: begin
          cc_write_data = {320'h0, dcache_req_data.data_wdata, 128'h0};
          cc_write_mask = {40'h0, dcache_req_data.data_be, 16'h0};
        end
        3'b011: begin
          cc_write_data = {256'h0, dcache_req_data.data_wdata, 192'h0};
          cc_write_mask = {32'h0, dcache_req_data.data_be, 24'h0};
        end
        3'b100: begin
          cc_write_data = {192'h0, dcache_req_data.data_wdata, 256'h0};
          cc_write_mask = {56'h0,  dcache_req_data.data_be, 32'h0};
        end
        3'b101: begin
          cc_write_data = {128'h0, dcache_req_data.data_wdata, 320'h0};
          cc_write_mask = {48'h0, dcache_req_data.data_be, 40'h0};
        end
        3'b110: begin
          cc_write_data = {64'h0, dcache_req_data.data_wdata, 384'h0};
          cc_write_mask = {40'h0, dcache_req_data.data_be, 48'h0};
        end
        3'b111: begin
          cc_write_data = {dcache_req_data.data_wdata, 448'h0};
          cc_write_mask = {32'h0, dcache_req_data.data_be, 56'h0};
        end
        default: begin
          cc_write_data = '0;
          cc_write_mask = '0;
        end
      endcase
    end
    
    logic dc_ready; //TODO
    logic l2_ready;
    logic in_fence_en, finish_fence_en;
    data_cache_mesi #(
      .ADDR_SIZE(ADDR_SIZE), 
      .LINE_BYTE(LINE_BYTE),
      .MEM_DEPTH(128),
      .N_DCACHE_WAY(4)
    ) data_cache_mesi_u(
    .clk_i                          ( clk_i                  ),
    .rst_ni                         ( rst_ni                 ),
    .dc_inst_finish_en              ( dc_inst_finish_en      ),
    .data_addr_i                    ( dcache_req_data.address), // TODO, the vaddr from cpu need to be translated to paddr
    .store_data_i                   ( cc_write_data          ), 
    .load_data_o                    ( cc_read_data           ),
    .CS                             ( dcache_enable_i        ),
    .MemR_en_i                      ( l1_ld_rd_en            ), 
    .MemW_en_i                      ( l1_st_wt_en            ),
    .byte_enable_i                  ( cc_write_mask          ),
    .fence_en_i                     ( dcache_fence_i && !uncached_addr_range_en         ),

    .readMiss_o                     ( l1_rd_miss_en          ),    // read miss
    .writeMiss_o                    ( l1_wt_miss_en          ),     // write miss
    .ready_o                        ( dc_ready               ),
    .in_fence_en_o                  ( in_fence_en            ),
    .finish_fence_en_o              ( finish_fence_en        ),
    // L2
    // L2 signals
    .l2_ready_i                     ( l2_to_l1.ready && !(l2_to_l1.valid && l2_to_l1.dc_ic)        ),
    // write back
    .l2_wb_en_o                     ( l2_wb_en               ),
    .l2_wb_addr_o                   ( l2_wb_addr             ),
    .l2_wb_data_o                   ( l2_wb_data             ),
    // read miss
    .l2_rm_en_o                     ( l2_rm_en               ),
    .l2_rm_addr_o                   ( l2_rm_addr             ),
    .l2_valid_en_i                  ( l2_to_l1.valid && !l2_to_l1.dc_ic        ),
    .l2_addr_i                      ( l2_to_l1.paddr         ),
    .l2_data_i                      ( l2_to_l1.data          ),
    // uncached
    .uncached_addr_range_en         ( uncached_addr_range_en ),
    .l2_mask_i                      ( '1                     ) // TODO
  );

  logic l2_wb_en_o_last;
  always_ff @(posedge clk_i or negedge rst_ni) begin
    if(!rst_ni)begin
      l2_wb_en_o_last       <= '0;
    end else begin
      l2_wb_en_o_last       <= cc2mem_o.valid && cc2mem_o.rw_req;
    end
  end


    // to lsu interface
    assign dcache_ready_o                     = uncached_addr_range_en ? (!cpu_ld_en && !cpu_st_en) || (cpu_ld_en && mem2cc_i.valid) || (cpu_st_en && mem2cc_i.ready && l2_wb_en_o_last) : dc_ready;// && l2_ready; // TODO
      // read
    assign dcache_req_ports_o[0].data_rdata   = uncached_addr_range_en ? mem2cc_i.data : cc_read_data;
    assign dcache_req_ports_o[0].data_rvalid  = uncached_addr_range_en ? cpu_ld_en && mem2cc_i.valid : l1_ld_rd_en && l1_hit_en;
    assign dcache_req_ports_o[0].vaddr        = dcache_req_data.address; // TODO
      // write
    assign dcache_req_ports_o[1].data_wgrant  = uncached_addr_range_en ? cpu_st_en && mem2cc_i.ready && l2_wb_en_o_last : l1_st_wt_en && l1_hit_en;
    assign dcache_req_ports_o[1].vaddr        = dcache_req_data.address; // TODO
     

  logic a_l1d_80007010, a_l1d_pt;
    assign a_l1d_80007010 = l2_wb_en && (l2_wb_addr == 'h80007010);
    assign a_l1d_pt = l2_wb_en && (l2_wb_addr >= 'h80007000 && l2_wb_addr < 'h80007040);
    // assign a_l1d_pt = l2_wb_en && (l2_wb_addr >= 'h80004000 && l2_wb_addr <= 'h800083ee);
    


  // -----------
  // I cache
  // -----------
    logic ic_rd_miss;
    logic ic_wt_miss;
    logic ic_ready;
    logic in_fencei_en;
    logic ic_l2_rm_en;
    logic [ADDR_SIZE-1:0] ic_l2_rm_addr;
    logic [DATA_WIDTH-1:0] ic_load_data;;

    // place holders, no need to use:
    logic                  l2_wb_en_ic;
    logic [ADDR_SIZE-1:0]  l2_wb_addr_ic; 
    logic [DATA_WIDTH-1:0] l2_wb_data_ic;

    inst_cache_mesi #(
      .ADDR_SIZE(ADDR_SIZE), 
      .LINE_BYTE(LINE_BYTE),
      .MEM_DEPTH(128),
      .N_DCACHE_WAY(4)
    ) inst_cache_mesi_u(
    .clk_i                          ( clk_i                  ),
    .rst_ni                         ( rst_ni                 ),
    .ic_inst_finish_en              ( ic_inst_finish_en      ),
    .data_addr_i                    ( icache_req_i.paddr     ), // TODO, the vaddr from cpu need to be translated to paddr
    .store_data_i                   ( '0                     ), // TODO, now icache no need to write
    .load_data_o                    ( ic_load_data           ),
    .CS                             ( icache_enable_i        ),
    .MemR_en_i                      ( icache_req_i.req       ), 
    .MemW_en_i                      ( '0                     ), // TODO, now icache no need to write
    .byte_enable_i                  ( '0                     ), // TODO, now icache no need to write
    .fence_en_i                     ( icache_fence_i         ), // TODO, fence.i

    .readMiss_o                     ( ic_rd_miss             ),    // read miss
    .writeMiss_o                    ( ic_wt_miss             ),     // TODO, write miss
    .ready_o                        ( ic_ready               ),
    .in_fence_en_o                  ( in_fencei_en           ),  // TODO, fence.i
    // L2
    // L2 signals
    .l2_ready_i                     ( l2_to_l1.ready && (!dc_req_l2_valid || (l2_to_l1.valid && l2_to_l1.dc_ic) ) ), // dc has higher priority
    // write back
    .l2_wb_en_o   (l2_wb_en_ic), // no need in ic
    .l2_wb_addr_o (l2_wb_addr_ic), // no need in ic
    .l2_wb_data_o (l2_wb_data_ic), // no need in ic
    // read miss
    .l2_rm_en_o                     ( ic_l2_rm_en            ),
    .l2_rm_addr_o                   ( ic_l2_rm_addr          ),
    .l2_valid_en_i                  ( l2_to_l1.valid && l2_to_l1.dc_ic        ),
    .l2_addr_i                      ( l2_to_l1.paddr         ),
    .l2_data_i                      ( l2_to_l1.data          ),
    // uncached
    .uncached_addr_range_en         ( uncached_addr_range_en ),
    .l2_mask_i                      ( '1                     ) // TODO
  );

  assign icache_req_o = '{
      ready : ic_ready,
      valid : icache_req_i.req & !ic_rd_miss,
      data  : ic_load_data,
      paddr : icache_req_i.paddr
  };


  // -----------
  // L2 cache
  // -----------
  // to l2 interface
  dcache_req_cc2mem_t l1_to_l2_in_func, l1_to_l2_in_handshake_d, l1_to_l2_in_handshake_q, l1_to_l2_in_func_test;
  logic l2_hand_shaked_d, l2_hand_shaked_q, l2_hand_shaked_q_q;
  cc_data_t l2_load_data;
  logic l2_ld_rd_en, l2_st_wt_en;
  logic l2_rd_miss_en, l2_wt_miss_en;
  logic l2_hit_en, l2_hit_en_last;
  logic l2_in_fence_en;
  logic l2_read_same_line_en;

  assign l2_ld_rd_en       = l1_to_l2.valid && !l1_to_l2.rw_req;
  assign l2_st_wt_en       = l1_to_l2.valid &&  l1_to_l2.rw_req;
  assign l2_hit_en         = ((l2_ld_rd_en && !l2_rd_miss_en) || (l2_st_wt_en && !l2_wt_miss_en)) && (uncached_addr_range_en ? !l2_hit_en_last : '1);
  always_ff @(posedge clk_i or negedge rst_ni) begin
      if(!rst_ni) begin
        l2_hit_en_last <= '0;
      end else begin
        l2_hit_en_last <= l2_hit_en;
      end
  end

  always_ff @(posedge clk_i or negedge rst_ni) begin
      if(!rst_ni) begin
        l1_to_l2_q     <= '0;
      end else /*if(l2_to_l1.ready && l1_to_l2.valid)*/ begin
        l1_to_l2_q     <= l1_to_l2;
      end
  end

  assign l2_hand_shaked_d         = l2_hand_shaked_q ? !l2_to_l1.ready : l2_to_l1.ready && l1_to_l2.valid;
  assign l1_to_l2_in_handshake_d  = l2_hand_shaked_q && !l2_hand_shaked_q_q ? l1_to_l2 : l1_to_l2_in_handshake_q;
  always_ff @(posedge clk_i or negedge rst_ni) begin
      if(!rst_ni) begin
        l2_hand_shaked_q_q        <= '0;
        l2_hand_shaked_q          <= '0;
        l1_to_l2_in_handshake_q   <= '0;
      end else begin
        l2_hand_shaked_q_q        <= l2_hand_shaked_q;
        l2_hand_shaked_q          <= l2_hand_shaked_d;
        l1_to_l2_in_handshake_q   <= l1_to_l2_in_handshake_d;
      end
  end

  logic a_test;
  assign a_test = l2_hand_shaked_q && !l2_to_l1.ready;
  assign l1_to_l2_in_func = (l2_to_l1.valid && l2_to_l1.dc_ic && dc_req_l2_valid) ? l1_to_l2_q
                            :l1_to_l2//(a_test ? (l2_hand_shaked_q && !l2_hand_shaked_q_q ? l1_to_l2 : l1_to_l2_in_handshake_q) : l1_to_l2)
                            ;
  // assign l1_to_l2_in_func = a_test ? l1_to_l2 : l1_to_l2;
  // assign l1_to_l2_in_func = l1_to_l2;

  assign dc_req_l2_valid    = l2_wb_en || l2_rm_en || ((cpu_ld_en || cpu_st_en) && uncached_addr_range_en) ; // TODO, handling exception
  assign ic_req_l2_valid    = ic_l2_rm_en;
  
  assign l1_to_l2.byte_mask = (l2_wb_en && !uncached_addr_range_en) ? {LINE_BYTE{1'b1}} : cc_write_mask;
  assign l1_to_l2.data      = (l2_wb_en && !uncached_addr_range_en) ? l2_wb_data : cc_write_data;
  assign l1_to_l2.paddr     = dc_req_l2_valid || !ic_req_l2_valid ? 
                              ((l2_wb_en && !uncached_addr_range_en) ? l2_wb_addr
                            : ((l2_rm_en && !uncached_addr_range_en) ? l2_rm_addr : dcache_req_data.address))
                            : ic_l2_rm_addr;                                    // TODO, the vaddr from cpu need to be translated to paddr
  assign l1_to_l2.rw_req    = dc_req_l2_valid || !ic_req_l2_valid ? (l2_wb_en || (cpu_st_en && uncached_addr_range_en)) : 0;
  assign l1_to_l2.valid     = (dc_req_l2_valid || ic_req_l2_valid) && !flush_i; // TODO, ic req
  assign l1_to_l2.uncached_en = uncached_addr_range_en;
  assign l1_to_l2.l1_fence_en = in_fence_en;
  assign l1_to_l2.dc_ic     = !dc_req_l2_valid && ic_req_l2_valid;

  // to mem interface
  logic                       mem_wb_en;
  logic [ADDR_SIZE-1:0]       mem_wb_addr;
  logic [DATA_WIDTH-1:0]      mem_wb_data;
  logic                       mem_rm_en;
  logic [ADDR_SIZE-1:0]       mem_rm_addr;

  l2_cache_mesi #(
      .ADDR_SIZE(ADDR_SIZE), 
      .LINE_BYTE(LINE_BYTE),
      .MEM_DEPTH(1024),
      .N_DCACHE_WAY(8)
    ) l2_cache_mesi_u(
    .clk_i                          ( clk_i                  ),
    .rst_ni                         ( rst_ni                 ),
    .l2_inst_finish_en              ( l2_inst_finish_en      ),
    .data_addr_i                    ( l1_to_l2_in_func.paddr         ), // TODO, the vaddr from cpu need to be translated to paddr
    .store_data_i                   ( l1_to_l2_in_func.data          ), 
    .load_data_o                    ( l2_load_data           ),
    .CS                             ( '1                     ), // TODO
    .MemR_en_i                      ( l2_ld_rd_en            ), 
    .MemW_en_i                      ( l2_st_wt_en            ),
    .byte_enable_i                  ( l1_to_l2_in_func.byte_mask     ),
    .fence_en_i                     ( finish_fence_en && !uncached_addr_range_en ), // TODO

    .readMiss_o                     ( l2_rd_miss_en          ),    // read miss
    .writeMiss_o                    ( l2_wt_miss_en          ),     // write miss
    .ready_o                        ( l2_ready               ),
    .read_same_line_en_o            ( l2_read_same_line_en   ),
    .in_fence_en_o                  ( l2_in_fence_en         ),
    // mem
    // L2 signals
    .l2_ready_i                     ( mem2cc_i.ready         ),
    // write back
    .l2_wb_en_o                     ( mem_wb_en              ),
    .l2_wb_addr_o                   ( mem_wb_addr            ),
    .l2_wb_data_o                   ( mem_wb_data            ),
    // read miss
    .l2_rm_en_o                     ( mem_rm_en              ),
    .l2_rm_addr_o                   ( mem_rm_addr            ),
    .l2_valid_en_i                  ( mem2cc_i.valid         ),
    .l2_addr_i                      ( mem2cc_i.paddr         ),
    .l2_data_i                      ( mem2cc_i.data          ),
    // uncached
    .uncached_addr_range_en         ( uncached_addr_range_en ),
    .l2_mask_i                      ( '1                     ) // TODO
  );
  //   l2_cache_mesi #(
  //     .ADDR_SIZE(ADDR_SIZE), 
  //     .LINE_BYTE(LINE_BYTE),
  //     .MEM_DEPTH(8192),
  //     .N_DCACHE_WAY(8)
  //   ) l2_cache_mesi_u(
  //   .clk_i                          ( clk_i                  ),
  //   .rst_ni                         ( rst_ni                 ),
  //   .l2_inst_finish_en              ( l2_inst_finish_en      ),
  //   .data_addr_i                    ( l1_to_l2_in_func.paddr         ), // TODO, the vaddr from cpu need to be translated to paddr
  //   .store_data_i                   ( l1_to_l2_in_func.data          ), 
  //   .load_data_o                    ( l2_load_data           ),
  //   .CS                             ( '1                     ), // TODO
  //   .MemR_en_i                      ( l2_ld_rd_en            ), 
  //   .MemW_en_i                      ( l2_st_wt_en            ),
  //   .byte_enable_i                  ( l1_to_l2_in_func.byte_mask     ),
  //   .fence_en_i                     ( finish_fence_en && !uncached_addr_range_en ), // TODO

  //   .readMiss_o                     ( l2_rd_miss_en          ),    // read miss
  //   .writeMiss_o                    ( l2_wt_miss_en          ),     // write miss
  //   .ready_o                        ( l2_ready               ),
  //   .read_same_line_en_o            ( l2_read_same_line_en   ),
  //   .in_fence_en_o                  ( l2_in_fence_en         ),
  //   // mem
  //   // L2 signals
  //   .l2_ready_i                     ( mem2cc_i.ready         ),
  //   // write back
  //   .l2_wb_en_o                     ( mem_wb_en              ),
  //   .l2_wb_addr_o                   ( mem_wb_addr            ),
  //   .l2_wb_data_o                   ( mem_wb_data            ),
  //   // read miss
  //   .l2_rm_en_o                     ( mem_rm_en              ),
  //   .l2_rm_addr_o                   ( mem_rm_addr            ),
  //   .l2_valid_en_i                  ( mem2cc_i.valid         ),
  //   .l2_addr_i                      ( mem2cc_i.paddr         ),
  //   .l2_data_i                      ( mem2cc_i.data          ),
  //   // uncached
  //   .uncached_addr_range_en         ( uncached_addr_range_en ),
  //   .l2_mask_i                      ( '1                     ) // TODO
  // );

    // to l1 interface
  assign l1_to_l2_select = l2_read_same_line_en ? l1_to_l2 : l1_to_l2_q;
  assign l2_to_l1 = '{
      ready : l2_ready,
      valid : l2_ld_rd_en && l2_hit_en,
      data  : l2_load_data,
      paddr : {{l1_to_l2_select.paddr[riscv_pkg::VLEN-1:$clog2(apd_pkg::ApdDefaultConfig.ICacheWidth / 8)]}, {$clog2(apd_pkg::ApdDefaultConfig.ICacheWidth / 8){1'b0}}}, // l1_to_l2.paddr
      dc_ic : l1_to_l2_select.dc_ic
  };


    logic a_l2_80007010, a_l2_pt;
    assign a_l2_80007010 = mem_wb_en && (mem_wb_addr == 'h80007010);
    // assign a_l2_pt = mem_wb_en && (mem_wb_addr >= 'h80060000 && mem_wb_addr <= 'h80070000);
    assign a_l2_pt = mem_wb_en && (mem_wb_addr >= 'h80007000 && mem_wb_addr < 'h80007040);
  // ----------------------
  // to memery interface
  // ----------------------
    
    assign cc2mem_o.byte_mask = (mem_wb_en && !uncached_addr_range_en) ? {LINE_BYTE{1'b1}} : cc_write_mask;
    assign cc2mem_o.data      = (mem_wb_en && !uncached_addr_range_en) ? mem_wb_data : cc_write_data;
    assign cc2mem_o.paddr     = ((mem_wb_en && !uncached_addr_range_en) ? mem_wb_addr
                              : ((mem_rm_en && !uncached_addr_range_en) ? mem_rm_addr : dcache_req_data.address));                                    // TODO, the vaddr from cpu need to be translated to paddr
    assign cc2mem_o.rw_req    = mem_wb_en || (cpu_st_en && uncached_addr_range_en);
    assign cc2mem_o.valid     = mem_wb_en || mem_rm_en || ((cpu_ld_en || cpu_st_en) && uncached_addr_range_en);
    assign cc2mem_o.uncached_en = uncached_addr_range_en;
    assign cc2mem_o.l1_fence_en = in_fence_en;
    assign cc2mem_o.l2_fence_en = l2_in_fence_en;
    assign cc2mem_o.dc_ic     = '1; //TODO
    // always @(posedge clk_i) begin
    //   if(cc2mem_o.rw_req & cc2mem_o.valid) begin
    //     $display("time: %d, flush = %h, clk = %h, rst = %h, cc2mem_o.rw_req = %h, l2_wb_en = %h,  cpu_st_en = %h, wb_pc = 0x%h, dcache_req_data.address = 0x%h, l1_ld_rd_en = %h", 
    //     $time(), top_u.dut.flush, clk_i, rst_ni, cc2mem_o.rw_req, l2_wb_en,  cpu_st_en, top_u.dut.is_stage_u.issue_u.wb_pc, dcache_req_data.address, l1_ld_rd_en);
    //   end
    // end
    

    // --------------------
    // performance counter
    // --------------------
    assign l1i2perf_o = '{
        read_req   : icache_req_i.req,
        write_req  : '0,
        read_miss  : ic_rd_miss,
        write_miss : ic_wt_miss,

        inst_finish: ic_inst_finish_en,

        rm_en      : ic_l2_rm_en,
        wb_en      : '0,

        in_fence_en: in_fencei_en
    };

    assign l1d2perf_o = '{
        read_req   : l1_ld_rd_en,
        write_req  : l1_st_wt_en,
        read_miss  : l1_rd_miss_en,
        write_miss : l1_wt_miss_en,

        inst_finish: dc_inst_finish_en,

        rm_en      : l2_rm_en,
        wb_en      : l2_wb_en,

        in_fence_en: in_fence_en
    };

    assign l22perf_o = '{
        read_req   : l2_ld_rd_en,
        write_req  : l2_st_wt_en,
        read_miss  : l2_rd_miss_en,
        write_miss : l2_wt_miss_en,

        inst_finish: l2_inst_finish_en,

        rm_en      : mem_rm_en,
        wb_en      : mem_wb_en,

        in_fence_en: l2_in_fence_en
    };
  

endmodule