`ifdef SYNTHESIS
module sram_wrapper #(
    parameter DATA_WIDTH_PER_UNIT = 256,
    parameter DATA_DEPTH_PER_UNIT = 256,
    parameter ADDR_WIDTH_UNIT     = $clog2(DATA_DEPTH_PER_UNIT), // clog2(256)==8

    parameter DATA_WIDTH          = 512, // for data:  (512 bits) // for tag:  (20 bits) 
    parameter DATA_DEPTH          = 2048,
    parameter ADDR_WIDTH          = $clog2(DATA_DEPTH),  // == $clog2(8192) == 13; 

    parameter SET_NUM             = 2**(ADDR_WIDTH - ADDR_WIDTH_UNIT), // 2**5 = 32
    // parameter UNIT_NUM_PER_SET    = int'($ceil(DATA_WIDTH*1.0/DATA_WIDTH_PER_UNIT)), // $ceil(256/10)=26
    parameter UNIT_NUM_PER_SET    = int'(DATA_WIDTH/DATA_WIDTH_PER_UNIT), // $ceil(256/10)=26
    parameter ACTUAL_DATA_WIDTH   = int'(UNIT_NUM_PER_SET*DATA_WIDTH_PER_UNIT)  //260
    ) 
  (   
    input  logic                     clk_i,
    input  logic                     rst_ni,
    input  logic                     en, 
    input  logic                     rw, // rw=0 (read), rw=1 (write)
    input  logic [ADDR_WIDTH-1:0]    addr,
    input  logic [DATA_WIDTH-1:0]    din,
    output logic [DATA_WIDTH-1:0]    qout
  );
    logic [DATA_WIDTH-1:0]   din_unit;
    logic [DATA_WIDTH-1:0]   qout_unit [0 : SET_NUM-1];
    logic [SET_NUM-1:0]               set_en;

    assign din_unit = {{(ACTUAL_DATA_WIDTH-DATA_WIDTH){1'b0}}, {din}};


    generate
      if(ADDR_WIDTH == ADDR_WIDTH_UNIT) begin
        assign qout     = qout_unit[0][DATA_WIDTH-1 : 0];
      end else begin
        assign qout     = qout_unit[addr[ADDR_WIDTH - 1 : ADDR_WIDTH_UNIT]][DATA_WIDTH-1 : 0];
      end
    endgenerate

    // always_ff @(posedge clk_i) begin
    //     qout   <= qout_unit[addr[ADDR_WIDTH - 1 : ADDR_WIDTH_UNIT]][DATA_WIDTH-1 : 0];
    // end

    generate 
      for (genvar i = 0; i < SET_NUM; i++) begin
        if(ADDR_WIDTH == ADDR_WIDTH_UNIT) begin
            assign set_en[i] = en;
        end else begin
            assign set_en[i] = en & (addr[ADDR_WIDTH - 1 : ADDR_WIDTH_UNIT] == i);
        end
      end
    endgenerate

    generate 
      for (genvar i = 0; i < SET_NUM; i++) begin
        for (genvar j = 0; j < UNIT_NUM_PER_SET; j++) begin
        // sram_512_2048_scn4m_subm ACTUAL_RAM        
        // (
        //     .clk0       ( clk_i               ), // clock
        //     .csb0       ( ~set_en[i]          ), // active low chip select
        //     .web0       ( ~rw                 ), // active low write control
        //     .addr0      ( addr[ADDR_WIDTH_UNIT-1 : 0] ),
        //     .din0       ( din_unit            ),
        //     .dout0      ( qout_unit           )
        // );
            DW_ram_rw_s_dff #(.data_width(DATA_WIDTH_PER_UNIT), .depth(DATA_DEPTH_PER_UNIT), .rst_mode(1)) TAG_RAM( 
                .clk(clk_i),
                .rst_n(1'b1),
                .cs_n(~set_en[i]),
                .wr_n(~rw),
                .rw_addr(addr[ADDR_WIDTH_UNIT-1 : 0]),
                .data_in(din_unit[(j+1)*DATA_WIDTH_PER_UNIT-1 : j*DATA_WIDTH_PER_UNIT]),
                .data_out(qout_unit[i][(j+1)*DATA_WIDTH_PER_UNIT-1 : j*DATA_WIDTH_PER_UNIT])
            );


        end
      end
    endgenerate
  endmodule
`endif 