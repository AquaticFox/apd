// This ideal I$ is a temporary workaround

module rom_ideal 
  import riscv_pkg::*;
  import apd_pkg::*;
#(
    parameter ROM_LINE_WIDTH = ApdDefaultConfig.ICacheWidth,
    parameter ROM_ROW_NUM    = ApdDefaultConfig.ICacheLineNum
)
(
  input  logic              clk_i,
  input  logic              rst_ni,
  input  logic              flush_i,
  
  // mmu ptw
  input  mmu_req_mmu2mem_t    mmu2mem_i,
  output mmu_req_mem2mmu_t    mem2mmu_o,
  
  // data reqs
  input  dcache_req_cc2mem_t  cc2mem_i,
  output dcache_req_mem2cc_t  mem2cc_o // its paddr not used yet
    
);

    logic[ROM_LINE_WIDTH-1:0] rom_q[bit[36-1:0]];
    logic[ROM_LINE_WIDTH-1:0] rom_output;
    logic[36-1:0]             rom_read_index, rom_read_index_q;
    logic                     rom_valid_q;
    logic                     rom_ready_q;
    
    logic                     lsu_read_en; // frontend and lsu can only one read per time
    logic                     lsu_write_en;
    logic                     mmu_read_en; 


   // -----------
   // Update ROM
   // -----------

   always_ff @(posedge clk_i or negedge rst_ni) begin
       for(int i = 0; i < ROM_LINE_WIDTH/8; i++)
           if (cc2mem_i.byte_mask[i] & lsu_write_en)
               rom_q[cc2mem_i.paddr >> apd_pkg::LOG_BYTE_PER_LINE][i*8 +: 8]  = cc2mem_i.data[i*8 +: 8];
   end
   
    // -----------
    // Read ROM
    // -----------
    always_ff @( posedge clk_i or negedge rst_ni) begin : rom_output_ff
        if(!rst_ni)begin
            rom_valid_q <= '0;
            rom_ready_q <= '0;
        end else begin
            rom_valid_q <= rst_ni && !flush_i;
            rom_ready_q <= rst_ni && !flush_i;
        end
    end
    
    assign lsu_read_en    = cc2mem_i.valid && !cc2mem_i.rw_req;
    assign lsu_write_en   = cc2mem_i.valid && cc2mem_i.rw_req;
    assign mmu_read_en     = mmu2mem_i.data_req;
    assign rom_read_index = lsu_read_en ? cc2mem_i.paddr : (mmu_read_en ? mmu2mem_i.paddr : '0);
    
    // always @(*) begin
    //     $display("rom_read_index = %h", rom_read_index);
    //     $display("lsu_read_en = %h,  cc2mem_i.valid = %h, cc2mem_i.rw_req = %h", lsu_read_en,  cc2mem_i.valid, cc2mem_i.rw_req);
    //     $display("cc2mem_i.paddr = %h; mmu2mem_i.paddr = %h", cc2mem_i.paddr, mmu2mem_i.paddr);
    // end
    
    always_comb begin : read_rom_comb
        rom_output = '0;
        if(!rst_ni) begin
            rom_output = '0;
        end else if(rom_valid_q && (lsu_read_en || mmu_read_en)) begin
            // $display("rom_read_index = %h", rom_read_index);
            rom_output =  rom_q[(rom_read_index >> apd_pkg::LOG_BYTE_PER_LINE)];
        end
        // rom_output = '0;
    end
    
    // mem2mmu
    assign mem2mmu_o.data_rdata  = rom_output;
    assign mem2mmu_o.data_rvalid = rom_valid_q && mmu2mem_i.data_req && !lsu_read_en    && !cc2mem_i.l1_fence_en && !cc2mem_i.l2_fence_en;
    assign mem2mmu_o.data_gnt    = rom_ready_q && !lsu_read_en                          && !cc2mem_i.l1_fence_en && !cc2mem_i.l2_fence_en; // wordaround, TODO
    // assign mem2mmu_o.vaddr = {mmu2mem_i.paddr[riscv_pkg::PLEN-1:apd_pkg::LOG_BYTE_PER_LINE], {apd_pkg::LOG_BYTE_PER_LINE{1'b0}}}; 
    
    // dc2lsu
    dcache_req_mem2cc_t mem2cc_q;
    logic               same_line_en;
    always_ff @(posedge clk_i) begin
        mem2cc_q.data  <= rom_output;
        mem2cc_q.valid <= rom_valid_q && lsu_read_en && !same_line_en && !flush_i; // it is a valid load
        mem2cc_q.ready <= rom_ready_q; // TODO
        mem2cc_q.paddr <= {cc2mem_i.paddr[riscv_pkg::VLEN-1:apd_pkg::LOG_BYTE_PER_LINE], {apd_pkg::LOG_BYTE_PER_LINE{1'b0}}};
        mem2cc_q.dc_ic <= cc2mem_i.dc_ic;
    end

    always_ff @( posedge clk_i or negedge rst_ni) begin
        if(!rst_ni)
            rom_read_index_q <= '0;
        else
            rom_read_index_q <= rom_read_index;
    end
    assign same_line_en = rom_read_index == rom_read_index_q;
    always_comb begin
        mem2cc_o        = mem2cc_q;
        if(cc2mem_i.uncached_en) begin
            mem2cc_o.valid  = mem2cc_q.valid && lsu_read_en && same_line_en;
        end
    end
    // assign mem2cc_o.data = rom_output;
    // assign mem2cc_o.valid = rom_valid_q && lsu_read_en; // it is a valid load
    // assign mem2cc_o.ready = rom_ready_q; // TODO
    // assign mem2cc_o.paddr = {cc2mem_i.paddr[riscv_pkg::VLEN-1:apd_pkg::LOG_BYTE_PER_LINE], {apd_pkg::LOG_BYTE_PER_LINE{1'b0}}};  
    
`ifdef LAB_TEST2
    int fdebug;
    string     image_f;
    string     output_f;
    logic [63:0] paddr_trace;
    initial begin
  `ifdef VCS
        $value$plusargs("backdoor_load_image=%s", image_f);
        $sformat(output_f,"../tb/logs/dramsim_vcs_%s",image_f);
        fdebug = $fopen(output_f, "wb+");
  `else
        $sformat(output_f,"../tb/logs/dramsim_ver_%s",image_f);
        fdebug = $fopen(output_f, "wb+");
  `endif
        // $fwrite(fdebug, "%s:\n", image_f);
    end

    always @(posedge clk_i) begin
        paddr_trace = {25'b0, cc2mem_i.paddr};
        if(lsu_read_en && mem2cc_o.ready && !same_line_en && !cc2mem_i.l2_fence_en) begin
            $fwrite(fdebug, "0x%x %s %d\n", rom_read_index, "READ", top_u.dut.csr_regfile_u.cycle_q);
        end else if(lsu_write_en && mem2cc_o.ready && !cc2mem_i.l2_fence_en) begin
            $fwrite(fdebug, "0x%x %s %d\n", cc2mem_i.paddr, "WRITE", top_u.dut.csr_regfile_u.cycle_q);
        end
    end

    // always @(posedge clk_i) begin
    //   if(lsu_write_en) begin
    //     $display("write time: %d, flush = %h, clk = %h, rst = %h, lsu_write_en = %h,  mem2cc_o.ready = %h, same_line_en = %h, cc2mem_i.l2_fence_en = %h, wb_pc = 0x%h, cc2mem_i.paddr = 0x%h", 
    //     $time(), top_u.dut.flush, clk_i, rst_ni, lsu_write_en, mem2cc_o.ready, same_line_en, cc2mem_i.l2_fence_en, top_u.dut.is_stage_u.issue_u.wb_pc, cc2mem_i.paddr);
    //   end

    //   if(lsu_read_en) begin
    //     $display("read  time: %d, flush = %h, clk = %h, rst = %h, lsu_write_en = %h, mem2cc_o.ready = %h, same_line_en = %h, cc2mem_i.l2_fence_en = %h, wb_pc = 0x%h, cc2mem_i.paddr = 0x%h", 
    //     $time(), top_u.dut.flush, clk_i, rst_ni, lsu_write_en, mem2cc_o.ready, same_line_en, cc2mem_i.l2_fence_en, top_u.dut.is_stage_u.issue_u.wb_pc, cc2mem_i.paddr);
    //   end
    // end

    final begin
        $fclose(fdebug);
    end
`endif

    // -------
    // handle tohost call
    // -------
    logic[31:0] struct_addr;
    logic[31:0] addr;
    logic[31:0] len;
    logic[31:0] cnt;
    logic stop_printf;
    int i;
    // logic[5:0] prinf_cnt;
    
    logic a1, a2, a12_q;
    logic [31:0] exit_verilator;

    assign a1 = (cc2mem_i.paddr == TOHOST_ADDR) || (cc2mem_i.paddr == 32'h80003000); // for rv64uc-p-rvc, whose tohost is 80003000
    assign a2 = ((|cc2mem_i.data[31:0]) == 1'b1);
    always_ff @(posedge clk_i)begin
        a12_q <= a1 & a2 & !cc2mem_i.l2_fence_en;
    end

`ifdef LAB_TEST
    int fdebug;
    string     image_f;
    initial begin
  `ifdef VCS
        $value$plusargs("backdoor_load_image=%s", image_f);
        fdebug = $fopen("../tb/logs/apd_vcs.dump", "ab+");
  `else
        fdebug = $fopen("../tb/logs/apd_verilator.dump", "ab+");
  `endif
        $fwrite(fdebug, "%s:\n", image_f);
    end

    always @(*) begin
        if(top_u.dut.csr_regfile_u.cycle_q > 500000) begin
            $fwrite(fdebug, "Timeout\n\n");
            $finish;
        end
    end

    final begin
        $fclose(fdebug);
    end
`endif

    always @(posedge clk_i) begin
        // if(!rst_ni) prinf_cnt = 0;
        exit_verilator = '0;
        if (a1 && a2) begin 
            // $display("wb_pc: %h", top_u.dut.is_stage_u.issue_u.wb_pc);
            // $display("mcycle: %h", top_u.dut.ex_stage_u.mcycle);
            // $display("mcycle: %h", top_u.dut.ex_stage_u.csr_regfile_u.cycle_q);
            
            struct_addr = cc2mem_i.data[31:0];
            // exit
`ifndef LAB_TEST
            if (cc2mem_i.data[31:0] == 32'b1) begin
                $display("receive exit code: %x", cc2mem_i.data[63:0]);
                $display("pc = %x", top_u.dut.is_stage_u.issue_u.wb_pc);
                $display("\nRun cycle = %d cycles", top_u.dut.csr_regfile_u.cycle_q);
                $display("Total load store Latency = %d cycles", 
                                    top_u.dut.performance_counter_u.l1i_hit_cycles_q 
                                +   top_u.dut.performance_counter_u.l1i_miss_cycles_q
                                +   top_u.dut.performance_counter_u.l1d_hit_cycles_q
                                +   top_u.dut.performance_counter_u.l1d_miss_cycles_q
                            );
                $display("Cache Hit rate = %f%s", 
                            (1.0 - ( 
                            //   top_u.dut.performance_counter_u.l1i_read_miss_q
                            // + top_u.dut.performance_counter_u.l1i_write_miss_q
                            // + top_u.dut.performance_counter_u.l1d_read_miss_q
                            // + top_u.dut.performance_counter_u.l1d_write_miss_q
                            top_u.dut.performance_counter_u.l2_read_miss_q * 1.0
                            + top_u.dut.performance_counter_u.l2_write_miss_q * 1.0
                            )
                            /
                            (
                            top_u.dut.performance_counter_u.l1i_read_req_q * 1.0
                            + top_u.dut.performance_counter_u.l1i_write_req_q * 1.0
                            + top_u.dut.performance_counter_u.l1d_read_req_q * 1.0
                            + top_u.dut.performance_counter_u.l1d_write_req_q * 1.0
                            //   top_u.dut.performance_counter_u.l2_read_req_q
                            // + top_u.dut.performance_counter_u.l2_write_req_q
                            )) * 100.0,
                            "%"
                        );
                $display("Cache Miss rate = %f%s", 
                    (( 
                    //   top_u.dut.performance_counter_u.l1i_read_miss_q
                    // + top_u.dut.performance_counter_u.l1i_write_miss_q
                    // + top_u.dut.performance_counter_u.l1d_read_miss_q
                    // + top_u.dut.performance_counter_u.l1d_write_miss_q
                        top_u.dut.performance_counter_u.l2_read_miss_q * 1.0
                    + top_u.dut.performance_counter_u.l2_write_miss_q *1.0
                    )
                    /
                    (
                        top_u.dut.performance_counter_u.l1i_read_req_q * 1.0
                    + top_u.dut.performance_counter_u.l1i_write_req_q * 1.0
                    + top_u.dut.performance_counter_u.l1d_read_req_q * 1.0
                    + top_u.dut.performance_counter_u.l1d_write_req_q * 1.0
                    //   top_u.dut.performance_counter_u.l2_read_req_q
                    // + top_u.dut.performance_counter_u.l2_write_req_q
                    )) * 100.0,
                    "%"
                );
                $display("Cache Hit Latency = %f cycles", 
                    ( 
                        top_u.dut.performance_counter_u.l1i_hit_cycles_q * 1.0
                    +   top_u.dut.performance_counter_u.l1d_hit_cycles_q * 1.0
                    )
                    /
                    (
                        top_u.dut.performance_counter_u.l1i_read_req_q * 1.0
                    + top_u.dut.performance_counter_u.l1i_write_req_q * 1.0
                    + top_u.dut.performance_counter_u.l1d_read_req_q * 1.0
                    + top_u.dut.performance_counter_u.l1d_write_req_q * 1.0
                    - top_u.dut.performance_counter_u.l2_read_req_q * 1.0
                    //   top_u.dut.performance_counter_u.l2_read_req_q
                    // + top_u.dut.performance_counter_u.l2_write_req_q
                    )
                );
                $display("Cache Miss Latency = %f cycles", 
                    ( 
                        top_u.dut.performance_counter_u.l1i_miss_cycles_q * 1.0
                    +   top_u.dut.performance_counter_u.l1d_miss_cycles_q * 1.0
                    )
                    /
                    (
                    //     top_u.dut.performance_counter_u.l1i_read_req_q * 1.0
                    // + top_u.dut.performance_counter_u.l1i_write_req_q * 1.0
                    // + top_u.dut.performance_counter_u.l1d_read_req_q * 1.0
                    // + top_u.dut.performance_counter_u.l1d_write_req_q * 1.0
                    top_u.dut.performance_counter_u.l2_read_req_q * 1.0
                    // + top_u.dut.performance_counter_u.l2_write_req_q * 1.0
                    )
                );
                $display("DCache Hit rate = %f%s", 
                            (1.0 - ( 
                            //   top_u.dut.performance_counter_u.l1i_read_miss_q
                            // + top_u.dut.performance_counter_u.l1i_write_miss_q
                            top_u.dut.performance_counter_u.l1d_read_miss_q * 1.0
                            + top_u.dut.performance_counter_u.l1d_write_miss_q * 1.0
                            // top_u.dut.performance_counter_u.l2_read_miss_q * 1.0
                            // + top_u.dut.performance_counter_u.l2_write_miss_q * 1.0
                            )
                            /
                            (
                            // top_u.dut.performance_counter_u.l1i_read_req_q * 1.0
                            // + top_u.dut.performance_counter_u.l1i_write_req_q * 1.0
                            top_u.dut.performance_counter_u.l1d_read_req_q * 1.0
                            + top_u.dut.performance_counter_u.l1d_write_req_q * 1.0
                            //   top_u.dut.performance_counter_u.l2_read_req_q
                            // + top_u.dut.performance_counter_u.l2_write_req_q
                            )) * 100.0,
                            "%"
                        );
                $display("DCache Miss rate = %f%s", 
                    (( 
                    //   top_u.dut.performance_counter_u.l1i_read_miss_q
                    // + top_u.dut.performance_counter_u.l1i_write_miss_q
                    top_u.dut.performance_counter_u.l1d_read_miss_q * 1.0
                    + top_u.dut.performance_counter_u.l1d_write_miss_q * 1.0
                    //     top_u.dut.performance_counter_u.l2_read_miss_q * 1.0
                    // + top_u.dut.performance_counter_u.l2_write_miss_q *1.0
                    )
                    /
                    (
                    //     top_u.dut.performance_counter_u.l1i_read_req_q * 1.0
                    // + top_u.dut.performance_counter_u.l1i_write_req_q * 1.0
                    top_u.dut.performance_counter_u.l1d_read_req_q * 1.0
                    + top_u.dut.performance_counter_u.l1d_write_req_q * 1.0
                    //   top_u.dut.performance_counter_u.l2_read_req_q
                    // + top_u.dut.performance_counter_u.l2_write_req_q
                    )) * 100.0,
                    "%"
                );
                $display("DCache Hit Latency = %f cycles", 
                    ( 
                        // top_u.dut.performance_counter_u.l1i_hit_cycles_q * 1.0
                        top_u.dut.performance_counter_u.l1d_hit_cycles_q * 1.0
                    )
                    /
                    (
                    //     top_u.dut.performance_counter_u.l1i_read_req_q * 1.0
                    // + top_u.dut.performance_counter_u.l1i_write_req_q * 1.0
                    top_u.dut.performance_counter_u.l1d_read_req_q * 1.0
                    + top_u.dut.performance_counter_u.l1d_write_req_q * 1.0
                    - top_u.dut.performance_counter_u.l1d_read_miss_q * 1.0
                    - top_u.dut.performance_counter_u.l1d_write_miss_q * 1.0
                    //   top_u.dut.performance_counter_u.l2_read_req_q
                    // + top_u.dut.performance_counter_u.l2_write_req_q
                    )
                );
                $display("DCache Miss Latency = %f cycles", 
                    ( 
                        // top_u.dut.performance_counter_u.l1i_miss_cycles_q * 1.0
                        top_u.dut.performance_counter_u.l1d_miss_cycles_q * 1.0
                    )
                    /
                    (
                        top_u.dut.performance_counter_u.l1d_read_miss_q * 1.0
                    +   top_u.dut.performance_counter_u.l1d_write_miss_q * 1.0
                    )
                );
                $display("\nl1i_read_req_q    = %d,", top_u.dut.performance_counter_u.l1i_read_req_q );
                $display("l1i_write_req_q   = %d,", top_u.dut.performance_counter_u.l1i_write_req_q );
                $display("l1d_read_req_q    = %d,", top_u.dut.performance_counter_u.l1d_read_req_q );
                $display("l1d_write_req_q   = %d,", top_u.dut.performance_counter_u.l1d_write_req_q );
                $display("l2_read_req_q     = %d,", top_u.dut.performance_counter_u.l2_read_req_q );
                $display("l2_write_req_q    = %d,\n", top_u.dut.performance_counter_u.l2_write_req_q );

                $display("l1i_read_miss_q   = %d,", top_u.dut.performance_counter_u.l1i_read_miss_q );
                $display("l1i_write_miss_q  = %d,", top_u.dut.performance_counter_u.l1i_write_miss_q );
                $display("l1d_read_miss_q   = %d,", top_u.dut.performance_counter_u.l1d_read_miss_q );
                $display("l1d_write_miss_q  = %d,", top_u.dut.performance_counter_u.l1d_write_miss_q );
                $display("l2_read_miss_q    = %d,", top_u.dut.performance_counter_u.l2_read_miss_q );
                $display("l2_write_miss_q   = %d,\n", top_u.dut.performance_counter_u.l2_write_miss_q );

                $display("l1i_hit_cycles_q  = %d,", top_u.dut.performance_counter_u.l1i_hit_cycles_q );
                $display("l1i_miss_cycles_q = %d,", top_u.dut.performance_counter_u.l1i_miss_cycles_q );
                $display("l1d_hit_cycles_q  = %d,", top_u.dut.performance_counter_u.l1d_hit_cycles_q );
                $display("l1d_miss_cycles_q = %d,", top_u.dut.performance_counter_u.l1d_miss_cycles_q );
                $display("l2_hit_cycles_q   = %d,", top_u.dut.performance_counter_u.l2_hit_cycles_q );
                $display("l2_miss_cycles_q  = %d\n", top_u.dut.performance_counter_u.l2_miss_cycles_q );
`else
            if (cc2mem_i.data[0:0] == 1'b1) begin
                 $fwrite(fdebug, "%x\n", cc2mem_i.data[63:0]);
                 $fwrite(fdebug, "Latency = %d cycles\n\n", top_u.dut.csr_regfile_u.cycle_q);
`endif

`ifdef VERILATOR
  `ifdef U_TEST
                 exit_verilator = cc2mem_i.data[31:0];
  `else
                 exit_verilator = cc2mem_i.data[31:0];
                 $finish;
  `endif
`else
                 $finish;
`endif

            end
            // print
            else if(!a12_q && !cc2mem_i.l2_fence_en) begin
                if(rom_q[struct_addr >> apd_pkg::LOG_BYTE_PER_LINE][8*8-1:0] == 64'd64) begin
                    // addr        = rom_q[(struct_addr+32'd16) >> apd_pkg::LOG_BYTE_PER_LINE];
                    // len         = rom_q[(struct_addr+32'd24) >> apd_pkg::LOG_BYTE_PER_LINE];
                    addr        = rom_q[struct_addr >> apd_pkg::LOG_BYTE_PER_LINE][24*8-1:16*8];
                    len         = rom_q[struct_addr >> apd_pkg::LOG_BYTE_PER_LINE][32*8-1:24*8];
                    i           = 0;
                    cnt         = 0;
                    stop_printf = 1'b0;
                    // $display("line_to_print: %h", rom_q[addr >> apd_pkg::LOG_BYTE_PER_LINE]);
                    while(stop_printf == 1'b0) begin
                        if (cnt >= len) begin
                            stop_printf = 1'b1;
                        end else begin
                            $write("%c", rom_q[(addr >> apd_pkg::LOG_BYTE_PER_LINE) + i][8*cnt[LOG_BYTE_PER_LINE-1:0]+:8]);
                            cnt = cnt + 1;
                            if (cnt[LOG_BYTE_PER_LINE-1:0] == '0) begin
                                i = i + 1;
                            end
                        end
                    end
                    // $display("current mcycle = %h", top_u.dut.csr_regfile_u.cycle_q);
                    // if(prinf_cnt == 1) $finish;
                    // prinf_cnt = prinf_cnt + 1;
                    rom_q[32'h80001040 >> apd_pkg::LOG_BYTE_PER_LINE][63:0] = 64'b1; 
                end
            end
        end else begin
            for(int i = 0; i < ROM_LINE_WIDTH/8; i++)
                if (cc2mem_i.byte_mask[i] & lsu_write_en)
                    rom_q[cc2mem_i.paddr >> apd_pkg::LOG_BYTE_PER_LINE][i*8 +: 8]  = cc2mem_i.data[i*8 +: 8];
        end
    end
    
    logic [511:0] a_line, b_line;
    logic a_addr_en, b_addr_en;
    assign a_addr_en = (cc2mem_i.paddr == 32'h81000000);
    assign b_addr_en = (cc2mem_i.paddr == 32'h8101ff80);
    always_comb begin
        a_line = rom_q[(32'h81000000 >> apd_pkg::LOG_BYTE_PER_LINE)];
        b_line = rom_q[(32'h8101ff80 >> apd_pkg::LOG_BYTE_PER_LINE)];
    end
endmodule