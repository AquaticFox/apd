
module frontend import apd_pkg::*; #(
    parameter apd_pkg::apd_cfg_t ApdCfg = apd_pkg::ApdDefaultConfig
) (
    input  logic                            clk_i,
    input  logic                            rst_ni,
    input  logic                            flush_i,
    input  logic [riscv_pkg::VLEN-1:0]      flush_pc_i, // TODO
    input  logic                            flush_bp_i,
    // global input
    input  logic [riscv_pkg::VLEN-1:0]      boot_addr_i,

    // To MMU
    output icache_req_f2mmu_t               f2mmu_o,
    input  icache_req_mmu2f_t               mmu2f_i,

    // Instruction Fetch
    output icache_req_f2ic_t                f2ic_o,
    input  icache_req_ic2f_t                ic2f_i,

    // To ID
    output inst_f2d_t                       inst_entry_f2d_o,
    input  logic                            inst_entry_d2f_ready_i
);

    logic                   ic_line_req;
    logic                   ib_ready;
    logic                   inst_wr;
    // -------------------
    // Next PC mux
    // -------------------
    // next PC (NPC) can come from (in order of precedence):
    // 0. Default assignment/replay instruction
    // 1. Branch Predict taken
    // 2. Control flow change request (misprediction flush)
    // 3. Return from environment call
    // 4. Exception/Interrupt
    logic npc_rst_en_q;
    logic [riscv_pkg::VLEN-1:0] npc_d, npc_q;
    logic [riscv_pkg::VLEN-1:0] fetch_addr_d, fetch_addr_q; 

    always_comb begin : npc_mux
        if(npc_rst_en_q) begin
            npc_d           = boot_addr_i;
        end else begin
            npc_d           = npc_q;
        end

        // if(if_ready)

    end

    logic if_ready;
    assign if_ready = ic2f_i.ready & ib_ready;
    assign inst_wr  = '1; // TODO 
    always_comb begin : fetch_addr
        if(npc_rst_en_q) begin
            fetch_addr_d    = boot_addr_i;
        end else begin
            fetch_addr_d    = fetch_addr_q;
        end
        
        if(if_ready & f2ic_o.req) begin
            fetch_addr_d    = fetch_addr_d + apd_pkg::BYTE_PER_LINE; // pc + 64 for next cache line
        end
    end

    always_ff @(posedge clk_i or negedge rst_ni) begin : npc_mux_ff
        if(!rst_ni) begin
            npc_rst_en_q <= 1'b1;
            npc_q        <= '0;
            fetch_addr_q <= boot_addr_i;
        end else begin
            if(flush_i) begin
                npc_rst_en_q <= 1'b0;
                npc_q        <= flush_pc_i;
                fetch_addr_q <= flush_pc_i;
            end else begin
                npc_rst_en_q <= 1'b0;
                npc_q        <= npc_d;
                fetch_addr_q <= fetch_addr_d;
            end
        end
    end

    // ----------
    // vm logic
    // ----------
    logic                   mmu_valid_d, mmu_valid_q;
    icache_req_mmu2f_t      mmu2f_d, mmu2f_q;
    icache_req_mmu2f_t      mmu2f_in_fuc;
    assign mmu2f_in_fuc = mmu_valid_q ? mmu2f_q : mmu2f_i;

    always_comb begin : mmu_valid_comb
        mmu_valid_d     = mmu_valid_q;
        mmu2f_d         = mmu2f_q;

        if(mmu2f_i.fetch_valid) begin
            mmu2f_d         = mmu2f_i;
            mmu_valid_d     = '1;
        end
        
        if(if_ready & f2ic_o.req) begin // new vaddr
            mmu2f_d         = '0;
            mmu_valid_d     = '0;
        end
    end

    always_ff @(posedge clk_i or negedge rst_ni) begin : mmu_valid_ff
        if(!rst_ni) begin
            mmu_valid_q  <= '0;
            mmu2f_q      <= '0;
        end else begin
            if(flush_i) begin
                mmu_valid_q  <= '0;
                mmu2f_q      <= '0;
            end else begin
                mmu_valid_q  <= mmu_valid_d;
                mmu2f_q      <= mmu2f_d;
            end
        end
    end

    // ---------------------
    // Instruction Buffer
    // ---------------------
    inst_f2d_t              inst_from_ib;
    logic                   inst_valid; // 1 valid inst can make it valid
    exception_t             fetch_exception;
    assign fetch_exception = '{
        cause   : mmu2f_in_fuc.fetch_exception.cause,
        tval    : mmu2f_in_fuc.fetch_exception.tval,
        valid   : mmu2f_in_fuc.fetch_exception.valid && mmu2f_in_fuc.fetch_valid
    };

    inst_buffer #(
        .DEPTH      (apd_pkg::IB_FIFO_DEPTH ), 
        .LINEWIDTH  (apd_pkg::FETCH_WIDTH   )
    ) inst_buffer_u
    (
        .clk_i          ( clk_i         ), 
        .rst_ni         ( rst_ni        ),
        .flush_i        ( flush_i       ),                         //flush instructin buffer
        .flush_order_i  ( flush_pc_i[$clog2(ENTRIESPERLINE):1]  ),
        // following: TODO
       .stall_i         ( '0            ),                         //pipeline stall
       .line_i          ( fetch_exception.valid ? '0 : ic2f_i.data   ),     //instruction in
       .ex_i            ( fetch_exception ),
       .line_addr_i     ( fetch_addr_q  ),
       .line_ready_i    ( ic2f_i.ready  ),  //memory system is ready
       .line_valid_i    ( ic2f_i.valid  ),
       .mmu_valid_i     ( mmu2f_in_fuc.fetch_valid ),
   
       .line_req_o      ( ic_line_req   ),     //prefetch a line (split transaction)
       .inst_ib2d_o     ( inst_from_ib  ),
       .inst_valid_o    ( inst_valid    ),
       
       .id_ready_i      ( inst_entry_d2f_ready_i ), // TODO, from ID
       .inst_wr_i       ( inst_wr       ),
       .ib_ready_o      ( ib_ready      )
    );
    `ifndef SYNTHESIS
    `ifndef VERILATOR
        chk_flush_order_i_bitwidth: assert property (@(posedge clk_i) (rst_ni) |-> $clog2(ENTRIESPERLINE) >= 1) else $error("%m: flush_order_i bit width less than 0");
    `endif
    `endif
    
    // ---------------------
    // FR to ID output
    // ---------------------
    assign inst_entry_f2d_o = inst_from_ib;
    
    // ---------------------
    // Address translation
    // ---------------------
    assign f2mmu_o = '{
        fetch_req  : ic_line_req,
        fetch_vaddr: fetch_addr_q
    };

    // ---------------------
    // Instr Fetch
    // ---------------------
    // assign f2ic_o.paddr = fetch_addr_q;
    // assign f2ic_o.req   = ic_line_req;
    assign f2ic_o = '{
        paddr   : mmu2f_in_fuc.fetch_paddr,
        req     : ic_line_req && mmu2f_in_fuc.fetch_valid
    }; // TODO, exception from mmu

    // ---------------------
    // BTB
    // ---------------------
    // apd_pkg::btb_output_t [apd_pkg::INSTR_PER_FETCH-1:0] btb_prediction_o; //TODO
    // btb #(
    //     .NR_ENTRIES(apd_pkg::ApdDefaultConfig.BTBEntries)
    // ) btb_u
    // (
    //     .clk_i          ( clk_i         ),
    //     .rst_ni         ( rst_ni         ),
    //     .btb_flush_i    ( '0),// TODO, from ex
    //     .vpc_i          ( '0),// TODO
    //     .btb_update_i   ( '0),// TODO, from ex
    //     .*
    // );

endmodule