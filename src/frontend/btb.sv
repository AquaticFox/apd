
// module btb #(
//     parameter int NR_ENTRIES = 32
// )
// (
//     input  logic                        clk_i,
//     input  logic                        rst_ni,
//     input  logic                        btb_flush_i,

//     input  logic [riscv_pkg::VLEN-1:0]  vpc_i,      // virtual PC from IF stage
//     input  apd_pkg::btb_update_t        btb_update_i,
//     output apd_pkg::btb_output_t [apd_pkg::INSTR_PER_FETCH-1:0] btb_prediction_o
// );
//     localparam IGNORED_LSB       = 1; // the last bit is always 0 (pc always + 2 or 4)
//     localparam NR_ROWS           = NR_ENTRIES / apd_pkg::INSTR_PER_FETCH;
//     localparam BTB_OFFSET_LEN    = $clog2(apd_pkg::INSTR_PER_FETCH);
//     localparam BTB_INDEX_LEN     = $clog2(NR_ROWS);
//     localparam BTB_TAG_LEN       = riscv_pkg::VLEN - BTB_OFFSET_LEN - BTB_INDEX_LEN;

//     logic [BTB_INDEX_LEN-1:0]   output_idx, update_idx;
//     logic [BTB_OFFSET_LEN-1:0]  update_offset_idx;
//     logic [BTB_TAG_LEN-1:0]     output_tag, update_tag;
//     assign output_idx           = vpc_i[BTB_INDEX_LEN + BTB_OFFSET_LEN + IGNORED_LSB -1 : BTB_OFFSET_LEN + IGNORED_LSB];
//     assign output_tag           = vpc_i[riscv_pkg::VLEN - 1 : riscv_pkg::VLEN - BTB_TAG_LEN];
//     assign update_idx           = btb_update_i.pc[BTB_INDEX_LEN + BTB_OFFSET_LEN + IGNORED_LSB -1 : BTB_OFFSET_LEN + IGNORED_LSB];
//     assign update_offset_idx    = btb_update_i.pc[BTB_OFFSET_LEN + IGNORED_LSB -1 : IGNORED_LSB];
//     assign update_tag           = btb_update_i.pc[riscv_pkg::VLEN - 1 : riscv_pkg::VLEN - BTB_TAG_LEN];

//     apd_pkg::btb_entry_t btb_q[NR_ROWS-1:0][apd_pkg::INSTR_PER_FETCH-1:0];
//     apd_pkg::btb_entry_t btb_update;

//     // ---------------------
//     // Output BTB Prediction
//     // ---------------------
//     for (genvar i = 0; i < apd_pkg::INSTR_PER_FETCH; i++) begin
//         // check the tag
//         assign btb_prediction_o[i].valid = (btb_q[output_idx][i].tag == output_tag) ? btb_q[output_idx][i].valid : 1'b0;
//         // put out prediction result
//         assign btb_prediction_o[i].target_address = btb_q[output_idx][i].target_address;
//     end

//     // ---------------------
//     // Update BTB
//     // ---------------------
//     assign btb_update.tag            = update_tag;
//     assign btb_update.valid          = btb_update_i.valid;
//     assign btb_update.target_address = btb_update_i.target_address;

//     // ff part
//     always_ff @( posedge clk_i or negedge rst_ni ) begin : btb_update_ff
//         if(rst_ni) begin
//             for (int i = 0; i < NR_ROWS; i++) begin
//                 btb_q[i] <= '{default: 0};
//             end
//         end else begin
//             if(btb_flush_i) begin // flush all the btb entries
//                 for(int i = 0; i < NR_ROWS; i++) begin
//                     for(int j = 0; j < apd_pkg::INSTR_PER_FETCH; j++) begin
//                         btb_q[i][j].valid <= 1'b0;
//                     end
//                 end
//             end else begin
//                btb_q[update_idx][update_offset_idx] <= btb_update;
//             end
//         end 
//     end
// endmodule