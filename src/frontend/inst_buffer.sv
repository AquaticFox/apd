
module inst_buffer import apd_pkg::*; 
#(  
    parameter DEPTH = apd_pkg::IB_FIFO_DEPTH, 
    parameter LINEWIDTH = apd_pkg::FETCH_WIDTH,
    parameter OFFSET_WIDTH = $clog2(apd_pkg::ApdDefaultConfig.ICacheWidth / 8),
    parameter ENTRIESPERLINE = LINEWIDTH/16
    ) // Depth is in 16-bit RVC instructions
(
    input  logic                    clk_i, 
    input  logic                    rst_ni,
    input  logic                    flush_i,                         //flush_i instructin buffer
    input  apd_pkg::idx_per_ic_line_t flush_order_i,
    
    input  logic                    stall_i,                         //pipeline stall_i
    input  logic [LINEWIDTH-1:0]    line_i,     //instruction in
    input  apd_pkg::exception_t     ex_i,
    input  logic [riscv_pkg::VLEN-1:0]  line_addr_i,
    input  logic                    line_ready_i,  //memory system is ready
    input  logic                    line_valid_i, 
    input  logic                    mmu_valid_i,

    output logic                    line_req_o,     //prefetch a line (split transaction)
    
    output inst_f2d_t               inst_ib2d_o,
    output logic                    inst_valid_o,
    
    input  logic                    id_ready_i,  // id read a inst
    input  logic                    inst_wr_i,  // can load a line from ic
    output logic                    ib_ready_o  // ib is idle, if can change the fetch addr to start a new line fetch
);
    localparam DEPTH_BIT_NUM  = $clog2(DEPTH);
    localparam ENTRIES_TO_CHECK_FOR_RVC = 3; // it is associate with issue number
    
    //inst buffer pointers
    rd_ptr_t                rff_rd_ptr, rff_wr_ptr, rd_ptr_next;
    logic [DEPTH_BIT_NUM:0] rff_free_cnt, next_free_cnt;             //number of free entries (credit based flow control)
    
    ib_entry_t [DEPTH-1:0]dff_inst_buf;
    
    logic flush_q;
    always_ff @(posedge clk_i) begin
        flush_q <= flush_i;
    end
    //synthesis translate_off
    //buffer pointer assertions go here
    `ifndef SYNTHESIS
    `ifndef VERILATOR
    chk_inst_re_i: assert property (@(posedge clk_i) disable iff (!rst_ni) (stall_i | flush_i) |-> !id_ready_i) else $error("%m: instruction buffer is not supposed to be read during pipeline stall_i and flush_i");
    
    chk_free_cnt: assert property (@(posedge clk_i) disable iff (!rst_ni) rff_free_cnt <= DEPTH+32) else $error("%m: instruction buffer credit calculation overflow");
    
    chk_overflow: assert property (@(posedge clk_i) disable iff (!rst_ni) line_valid_i |-> (rff_rd_ptr != rff_wr_ptr || rff_free_cnt == DEPTH)) else $error("%m: instruction buffer overflow");
    `endif // VERILATOR
    `endif // SYNTHESIS
    
    //synthesis translate_on
    
    
    rd_ptr_t incr_rd_ptr_inst1, incr_rd_ptr_inst2;
    rd_ptr_t incr_rd_ptr;
    logic rd_ptr_en;
    logic is_top_rvc;
    logic [ENTRIES_TO_CHECK_FOR_RVC-1:0] is_entry_rvc; // for dual issue, need to check the top 3 enrties for rvc to dp the fetch and next rd_ptr update
    logic ib_notfull;
    logic wr_will_take_space_en;
    apd_pkg::idx_per_ic_line_t dff_flush_order, flush_order_nextcnt;
    
    // -----------------
    // post flush logic
    // -----------------
    logic line_valid_after_flush;
    always_ff @(posedge clk_i or negedge rst_ni) begin
        if(!rst_ni) begin
            line_valid_after_flush <= '0;
        end else if (flush_i) begin
            line_valid_after_flush <= '0;
        end else if (line_valid_i) begin
            line_valid_after_flush <= '1;
        end
    end

    // ---------------
    // Instr Output
    // ---------------
    typedef logic [DEPTHMSB:0] rd_inx_t;
    rd_inx_t[2:0] rd_idx; 
    for(genvar i = 0; i < ENTRIES_TO_CHECK_FOR_RVC; i++) begin
        assign rd_idx[i]       = rff_rd_ptr + i;
        assign is_entry_rvc[i] = apd_pkg::is_inst_rvc(dff_inst_buf[rd_idx[i]].entry[1:0]);
    end
    
    rd_ptr_t rff_rd_ptr_plus_1, rff_rd_ptr_plus_2, rff_rd_ptr_plus_3;
    assign rff_rd_ptr_plus_1 = rff_rd_ptr + 1;
    assign rff_rd_ptr_plus_2 = rff_rd_ptr + 2;
    assign rff_rd_ptr_plus_3 = rff_rd_ptr + 3;
    
    assign is_top_rvc = is_entry_rvc[0];
    assign inst_ib2d_o.issue_inst[0].inst   = { dff_inst_buf[rff_rd_ptr_plus_1].entry, dff_inst_buf[rff_rd_ptr].entry };
    assign inst_ib2d_o.issue_inst[1].inst   = is_entry_rvc[0] ? { dff_inst_buf[rff_rd_ptr_plus_2].entry, dff_inst_buf[rff_rd_ptr_plus_1].entry }
                                                              : { dff_inst_buf[rff_rd_ptr_plus_3].entry, dff_inst_buf[rff_rd_ptr_plus_2].entry };
    assign inst_ib2d_o.issue_inst[0].addr   = dff_inst_buf[rff_rd_ptr].addr;
    assign inst_ib2d_o.issue_inst[1].addr   = is_entry_rvc[0] ? dff_inst_buf[rff_rd_ptr_plus_1].addr
                                                              : dff_inst_buf[rff_rd_ptr_plus_2].addr;
    assign inst_ib2d_o.issue_inst[0].valid  = ((rff_free_cnt <= DEPTH-2) | ((rff_free_cnt == DEPTH-1) & is_top_rvc)) 
                                            && line_valid_after_flush;
    assign inst_ib2d_o.issue_inst[1].valid  = (is_top_rvc & ((rff_free_cnt <= DEPTH-3) | (is_entry_rvc[1] & (rff_free_cnt == DEPTH-2)))
                                            | ~is_top_rvc &((rff_free_cnt <= DEPTH-4) | (is_entry_rvc[2] & (rff_free_cnt == DEPTH-3))))
                                            && line_valid_after_flush;
    assign inst_ib2d_o.issue_inst[0].is_rvc = is_top_rvc;
    assign inst_ib2d_o.issue_inst[1].is_rvc = is_top_rvc ? is_entry_rvc[1] : is_entry_rvc[2];
    
    assign inst_ib2d_o.issue_inst[0].ex     = dff_inst_buf[rff_rd_ptr].exception;
    assign inst_ib2d_o.issue_inst[1].ex     = is_entry_rvc[0] ? dff_inst_buf[rff_rd_ptr_plus_1].exception
                                                              : dff_inst_buf[rff_rd_ptr_plus_2].exception;

    assign inst_valid_o = inst_ib2d_o.issue_inst[0].valid && line_valid_after_flush;
    // ----------------
    // FIFO ptr update
    // ----------------
    always_comb begin
        // next ib read pointer
        incr_rd_ptr_inst1 = is_top_rvc ? 1 : 2;
        incr_rd_ptr_inst2 = inst_ib2d_o.issue_inst[1].valid ? (inst_ib2d_o.issue_inst[1].is_rvc ? 1 : 2) : 0;
        incr_rd_ptr    = incr_rd_ptr_inst1 + incr_rd_ptr_inst2 ;
        rd_ptr_en      = inst_valid_o & id_ready_i & ~stall_i;
    
        //refill request
        //issue inst buffer refill request as long as there are free spaces
        ib_notfull = (rff_free_cnt >= ENTRIESPERLINE);
        line_req_o = ib_notfull & ~flush_i & ~stall_i & rst_ni & inst_wr_i;
        
        //buffer credit calculation
        wr_will_take_space_en = line_req_o & line_ready_i & ib_ready_o & mmu_valid_i;
        next_free_cnt = rff_free_cnt - (wr_will_take_space_en ? ENTRIESPERLINE : 0) + flush_order_nextcnt
                                     + ((inst_valid_o & id_ready_i) ? incr_rd_ptr : 0 );
            
    end
    
    // -------------------------
    // FSM when flush_i for rd_ptr
    // state 0: no flush_i, 1: flush_i for the first cycle, rest rd_ptr, 2: after the flush_i, the ib is empty and start to read a line, change the rff_free_cnt to fit the right free cnt
    enum {SEQUENTIAL, FLUSH_RST_RDPTR, FLUSH_FIT_FREECNT} rd_ptr_state_d, rd_ptr_state_q; 
    assign rd_ptr_next = rff_rd_ptr + incr_rd_ptr;
    always_comb begin: rd_ptr_state_d_comb
        rd_ptr_state_d = rd_ptr_state_q;
        case(rd_ptr_state_q)
            SEQUENTIAL: begin
                if(flush_i) begin
                    rd_ptr_state_d = FLUSH_RST_RDPTR;
                end
            end
            FLUSH_RST_RDPTR: begin
                if(!flush_i) begin
                    rd_ptr_state_d = FLUSH_FIT_FREECNT;
                end
            end
            FLUSH_FIT_FREECNT: begin
                if(wr_will_take_space_en & line_valid_i) begin
                    rd_ptr_state_d = SEQUENTIAL;
                end
            end
            default: rd_ptr_state_d = SEQUENTIAL;
        endcase
    end // rd_ptr_state_d_comb 
    
    always_comb begin: rd_ptr_state_output
        case(rd_ptr_state_q)
           FLUSH_RST_RDPTR: begin
                flush_order_nextcnt = dff_flush_order;
           end
           default: flush_order_nextcnt = '0;
        endcase
    end // rd_ptr_state_output
    
    always_ff @(posedge clk_i or negedge rst_ni) begin :rd_ptr_state_q_update_ff 
        if(!rst_ni)begin
            rd_ptr_state_q <= SEQUENTIAL;
        end else begin
            rd_ptr_state_q <= rd_ptr_state_d;
        end
    end // rd_ptr_state_q_update_ff
    
    always_ff @(posedge clk_i) begin: dff_flush_order_update_ff
       if(flush_i)
            dff_flush_order <= flush_order_i; 
    end // dff_flush_order_update_ff
    // -------------------------
    
    always_ff @(posedge clk_i or negedge rst_ni) begin : update_ptr_cnt_ff
        if (!rst_ni) begin
            rff_free_cnt    <= DEPTH;
            rff_rd_ptr      <= '0; 
            rff_wr_ptr      <= '0;
        end
        else if (flush_i) begin
            rff_free_cnt    <= DEPTH;
            rff_rd_ptr      <= {{(DEPTHMSB+1-$clog2(ENTRIESPERLINE)){1'b0}}, flush_order_i}; 
            rff_wr_ptr      <= '0;
        end
        else begin
            if (rd_ptr_en) 
                    rff_rd_ptr <= rd_ptr_next;
    
            if ((line_req_o & line_ready_i) | rd_ptr_en | flush_q)
                    rff_free_cnt <= next_free_cnt;
    
            //even if the pipeline is stalled, we cannot lose data 
            if (line_valid_i & ib_ready_o)
                    rff_wr_ptr <= rff_wr_ptr + ENTRIESPERLINE;
                
        end     
    end
    
    `ifndef SYNTHESIS
    `ifndef VERILATOR
        chk_rff_rd_ptr_bitwidth: assert property (@(posedge clk_i) (rst_ni) |-> DEPTHMSB+1-$clog2(ENTRIESPERLINE) >= 0) else $error("%m: rff_rd_ptr bit width less than 0");
    `endif
    `endif
    
    always_ff @(posedge clk_i) begin : update_ib_ff
        if (line_valid_i) begin
            for (int i=0;i<ENTRIESPERLINE;i++) begin
                dff_inst_buf[rff_wr_ptr+i].entry        <= line_i[i*16 +: 16];
                dff_inst_buf[rff_wr_ptr+i].addr         <= { {line_addr_i[riscv_pkg::VLEN-1:OFFSET_WIDTH]}, {i[$clog2(ENTRIESPERLINE)-1:0]}, {1'b0}};
                dff_inst_buf[rff_wr_ptr+i].exception    <= ex_i;
            end
        end
    end 
    
    // ---------------
    // ib_ready_o FSM
    // ---------------
    enum {IDLE, BUSY} ib_state_d, ib_state_q; // state 0: idle, 1: fetching a line
    always_comb begin : ib_state_d_comb
        ib_state_d = ib_state_q;
        case(ib_state_q)
            IDLE: begin
                if(line_req_o && !line_valid_i)begin
                    ib_state_d = BUSY;
                end
            end
            BUSY: begin
                if(line_valid_i)begin
                    ib_state_d = IDLE;
                end
            end
            default: ib_state_d = IDLE;
        endcase
    end // ib_state_d_comb
    
    always_comb begin : ib_state_output_comb
        case(ib_state_q)
            IDLE: begin
                ib_ready_o = 1'b1;
            end
            BUSY: begin
                ib_ready_o = 1'b0;
            end
            default: ib_ready_o = 1'b0;
        endcase
    end // ib_state_output_comb
    
    always_ff @(posedge clk_i or negedge rst_ni) begin :ib_state_q_update_ff 
        if(!rst_ni)begin
            ib_state_q <= IDLE;
        end else begin
            ib_state_q <= ib_state_d;
        end
    end // ib_state_q_update_ff
    
endmodule

