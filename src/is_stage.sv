// issue stage, single-issue, in-order backend
module is_stage import apd_pkg::*; #(
)(
    input  logic                    clk_i,
    input  logic                    rst_ni,
    input  logic                    flush_i,
    
    // from ID2IS FIFO
    output logic[apd_pkg::ISSUE_NUM-1:0]    id2is_fifo_pop_req_o,
    input  inst_id2is_t                     inst_entry_id2is_i,
    input  logic[apd_pkg::ISSUE_NUM-1:0]    fifo_empty_i,

    // to EX
    output fu_data_t            is2ex_o,
    output logic                is2ex_valid_o,
    input  logic                ex_ready_i,
    
    // from EX to write back
    input  ex2wb_t              ex2wb_i,
    
    // from EX
    input  logic                commit_valid_i
);
    
    localparam FRONTEND_ISSUE_NUM_WIDTH = apd_pkg::ISSUE_NUM ? $clog2(apd_pkg::ISSUE_NUM) : 1;
    
    id_per_issue_t pick_entry;
    logic pick_entry_valid;
    logic is_ready;
    // ----------------------
    // 1. pick inst from ID2IS FIFO
    // ----------------------
//    logic[FRONTEND_ISSUE_NUM_WIDTH-1:0] id2is_ptr_d, id2is_ptr_d_plus1, id2is_ptr_q;
//    logic id2is_valid;
//    assign id2is_ptr_q_plus1 = id2is_ptr_q + 1;
//    
//    always_comb begin : id2is_ptr_d_comb
//       id2is_fifo_pop_req_o       = '0;
//       id2is_ptr_d = id2is_ptr_q;
//       id2is_valid = 1'b0;
//       if(inst_entry_id2is_i.issue_inst[id2is_ptr_q].valid && !fifo_empty_i[id2is_ptr_q] && is_ready) begin // the instruction to issue is valid
//           id2is_fifo_pop_req_o[id2is_ptr_q]   = 1'b1;
//           id2is_ptr_d          = id2is_ptr_q_plus1;
//           id2is_valid          = 1'b1;
//       end else if(inst_entry_id2is_i.issue_inst[id2is_ptr_q_plus1].valid && !fifo_empty_i[id2is_ptr_q_plus1] && is_ready) begin
//           id2is_fifo_pop_req_o[id2is_ptr_q]       = 1'b1;
//           id2is_fifo_pop_req_o[id2is_ptr_q_plus1] = 1'b1;
//           id2is_ptr_d              = id2is_ptr_q;
//           id2is_valid              = 1'b1;
//       end
//       
//       if(flush_i) begin
//           id2is_fifo_pop_req_o = '0;
//       end
//   end
//   
//   always_ff @(posedge clk_i or negedge rst_ni) begin : id2is_ptr_q_update_ff
//      if(!rst_ni) begin
//        id2is_ptr_q <= '0;
//      end else if (flush_i) begin
//        id2is_ptr_q <= '0;
//      end else begin
//        id2is_ptr_q <= id2is_ptr_d;
//      end
//  end
  pick pick_u (
      .clk_i            ( clk_i                 ),
      .rst_ni           ( rst_ni                ),
      .flush_i          ( flush_i               ),
      .pick_ready_o     ( id2is_fifo_pop_req_o  ),
      .inst_entry_id2is_i(inst_entry_id2is_i    ),
      .fifo_empty_i     ( fifo_empty_i          ),
      .pick_entry_o     ( pick_entry            ),
      .pick_entry_valid_o(pick_entry_valid      ),
      .is_ready_i       ( is_ready              )
  );
  
  // --------------------------------
  // 2. read regfile, issue instruction
  // --------------------------------
  issue issue_u (
      .clk_i        ( clk_i             ),
      .rst_ni       ( rst_ni            ),
      .flush_i      ( flush_i           ),
      
      // from is pick
      .pick_entry_i ( pick_entry        ),
      .pick_entry_valid_i(pick_entry_valid ),
      .is_ready_o   ( is_ready          ),
      
      // to ex
      .is2ex_o      ( is2ex_o           ),
      .is2ex_valid_o( is2ex_valid_o     ),
      .ex_ready_i   ( ex_ready_i        ),
      
      // from ex for wb
      .ex2wb_i      ( ex2wb_i           ),
      
      // from ex
      .commit_valid_i ( commit_valid_i  )     
  );

endmodule