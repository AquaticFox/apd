
/************************************************
* 64-bit multiplier
 ************************************************/

module multiplier_fast (
        input  logic              clk_i,
        input  logic              rst_ni,
        //control
        input  logic              mul_req_valid_i,
        output logic              mul_resp_valid_o,
        output logic              mul_ready_o,
        //operand
        input  riscv_pkg::xlen_t       mul_a_i, // Multiplicand<< // Dividend
        input  riscv_pkg::xlen_t       mul_b_i, // multiplier>>   // divisor
        
        // unsigned / signed
        input  logic              is_signed_a,
        input  logic              is_signed_b,

        // regfile interface
        output logic [riscv_pkg::XLEN*2-1:0]      result_o
        );
    
    logic [riscv_pkg::XLEN*2-1:0]       product; // begin: [ multiplier | product ]//in division, [Dividend|Quotient]
    logic [riscv_pkg::XLEN*2-1:0]       product_next;

    logic [1:0]         round_record; // record mul rounds from 0 to 3
    logic [1:0]         round_record_next;
    
    logic [riscv_pkg::XLEN-1:0]        mult_operand_a;
    logic [riscv_pkg::XLEN-1:0]        mult_operand_b;
    logic [riscv_pkg::XLEN:0]        adder_result_ext;
    
    logic mul_resp_valid_o_q;
    always_ff @(posedge clk_i) begin
        mul_resp_valid_o_q <= mul_resp_valid_o;
    end

    assign round_record_next = mul_resp_valid_o ? '0 : round_record + 1;
    assign mul_resp_valid_o  =  round_record == 2'd3;
    assign mul_ready_o       =  (round_record == 2'd0); // (!mul_req_valid_i && (round_record == 2'd0)) || (mul_resp_valid_o_q ) ;

    //// 32*32 multiplier
    logic[riscv_pkg::XLEN-1:0] partial_product;
    logic[31:0] partial_operand_a;
    logic[31:0] partial_operand_b;
    
    assign partial_product = partial_operand_a * partial_operand_b;
    
    logic              a_negate;
    logic              b_negate;
    assign  a_negate     = mul_a_i[63];
    assign  b_negate     = mul_b_i[63];
    
    logic [63:0]      mul_signed_a;
    logic [63:0]      mul_signed_b;
    assign mul_signed_a = a_negate ? { ~mul_a_i + 1 } : mul_a_i;
    assign mul_signed_b = b_negate ? { ~mul_b_i + 1 } : mul_b_i;
    
    logic [63:0]       muldiv_operand_a;
    logic [63:0]       muldiv_operand_b;
    assign muldiv_operand_a = is_signed_a ? mul_signed_a : mul_a_i;
    assign muldiv_operand_b = is_signed_b ? mul_signed_b : mul_b_i;

    always_comb begin : partial_product_operand
        case( round_record )
            2'd0: begin
                partial_operand_a = muldiv_operand_a[31:0];
                partial_operand_b = muldiv_operand_b[31:0];
            end
            2'd1: begin
                partial_operand_a = muldiv_operand_a[63:32];
                partial_operand_b = muldiv_operand_b[31:0];
            end
            2'd2: begin
                partial_operand_a = muldiv_operand_a[31:0];
                partial_operand_b = muldiv_operand_b[63:32];
            end
            default: begin //2'd3
                partial_operand_a = muldiv_operand_a[63:32];
                partial_operand_b = muldiv_operand_b[63:32];
            end
        endcase
    end
    
    
    
    
    //to alu
    always_comb begin : to_alu
        case( round_record )
            2'b01: begin
                mult_operand_a = partial_product;
                mult_operand_b = {32'b0, product[63:32]};
            end
            2'b10: begin
                mult_operand_a = partial_product;
                mult_operand_b = product[96-1:32];
            end
            default: begin //2'b11
                mult_operand_a = partial_product;
                mult_operand_b = {31'b0, product[96:64]};
            end
        endcase
    end
    assign adder_result_ext = $unsigned(mult_operand_a) + $unsigned(mult_operand_b);

    
    //register refresh
    always_comb begin : product_refresh
        case( round_record )
            2'b00: begin
                product_next[63:0]  = partial_product;
            end
            2'b01: begin
                product_next[96:32] = adder_result_ext[64:0];
            end
            2'b10: begin
                product_next[96:32] = adder_result_ext[64:0];
            end
            default: begin //2'b11
                product_next[127:64] = adder_result_ext[63:0];
            end
        endcase
    end
    
    always_ff @(posedge clk_i or negedge rst_ni) begin : multiply
        if(!rst_ni) begin
            product      <= '0;
            round_record <= '0;
        end else if((mul_req_valid_i && mul_ready_o) || !mul_ready_o) begin
            product      <= product_next;
            round_record <= round_record_next;
        end else begin
            product      <= '0;
            round_record <= '0;
        end
    end
    
    
    
    //output
    logic        result_negate;
    logic [127:0] neg_result;
    assign result_negate    = a_negate ^ b_negate;
    assign neg_result       = ~product_next + 1;
    always_comb begin : muldiv_output
        result_o = product_next;
        if (is_signed_b) begin// must be ss
            if(result_negate) begin
                result_o = neg_result;
            end
        end
        else if (is_signed_a) begin  //must be su
            if(a_negate) begin
                result_o = neg_result;
            end
        end
    end
endmodule
