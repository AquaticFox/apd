

module mult import apd_pkg::*; (
    input  logic                     clk_i,
    input  logic                     rst_ni,
    input  logic                     flush_i,
    input  fu_data_t                 fu_data_i,
    input  logic                     mult_valid_i,
    output riscv_pkg::xlen_t             result_o,
    output logic                     mult_valid_o,
    output logic                     mult_ready_o
);
    logic                     mul_valid;
    logic                     div_valid;
    logic                     div_ready_i; // receiver of division result is able to accept the result
    riscv_pkg::xlen_t             mul_result;
    riscv_pkg::xlen_t             div_result;

    logic                     div_valid_op;
    logic                     mul_valid_op;
    // Input Arbitration
    assign mul_valid_op = ~flush_i && mult_valid_i && (fu_data_i.operator inside { MUL, MULH, MULHU, MULHSU, MULW });
    assign div_valid_op = ~flush_i && mult_valid_i && (fu_data_i.operator inside { DIV, DIVU, DIVW, DIVUW, REM, REMU, REMW, REMUW });

    // ---------------------
    // Output Arbitration
    // ---------------------
    logic mul_ready, div_ready;
    assign div_ready_i      = (mul_valid) ? 1'b0         : 1'b1;
    assign result_o         = (mul_valid) ? mul_result   : div_result;
    assign mult_valid_o     = div_valid | mul_valid;
    assign mult_ready_o     = mul_ready && div_ready;
    // mult_ready_o = division as the multiplication will unconditionally be ready to accept new requests

    // ---------------------
    // Multiplication
    // ---------------------
    logic sign_a, sign_b;
    logic [riscv_pkg::XLEN*2-1:0] mul_output;
    fu_op mul_operator_d, mul_operator_q;
    // Sign Select MUX
    fu_op                   mul_operator_in_fun;
    riscv_pkg::xlen_t[1:0]  mul_operand_d, mul_operand_q, mul_operand_in_fun;
    
    assign mul_operator_in_fun = mul_ready ? mul_operator_d : mul_operator_q;
    assign mul_operand_in_fun  = mul_ready ? mul_operand_d  : mul_operand_q;
    
    always_comb begin : mul_sign_mux_comb
        sign_a = 1'b0;
        sign_b = 1'b0;
        // signed multiplication
        if (mul_operator_in_fun == MULH) begin
            sign_a   = 1'b1;
            sign_b   = 1'b1;
        // signed - unsigned multiplication
        end else if (mul_operator_in_fun == MULHSU) begin
            sign_a   = 1'b1;
        // unsigned multiplication
        end else begin
            sign_a   = 1'b0;
            sign_b   = 1'b0;
        end
    end
    // Output selec
    always_comb begin : mul_output_mux_comb
        unique case (mul_operator_q)
            MULH, MULHU, MULHSU: mul_result = mul_output[riscv_pkg::XLEN*2-1:riscv_pkg::XLEN];
            MULW:                mul_result = sext32(mul_output[31:0]);
            // MUL performs an XLEN-bit×XLEN-bit multiplication and places the lower XLEN bits in the destination register
            default:             mul_result = mul_output[riscv_pkg::XLEN-1:0];// including MUL
        endcase
    end
    
    assign mul_operator_d   = fu_data_i.operator;
    assign mul_operand_d[0] = fu_data_i.operand_a;
    assign mul_operand_d[1] = fu_data_i.operand_b;
    always_ff @(posedge clk_i or negedge rst_ni) begin : mul_operator_ff
        if(!rst_ni) begin
            mul_operator_q <= '0;
            mul_operand_q  <= '0;
        end else if(mul_valid_op && mul_ready) begin
            mul_operator_q <= mul_operator_d;
            mul_operand_q  <= mul_operand_d;
        end
    end

    multiplier_fast multiplier_fast_u (
        .clk_i             ( clk_i              ),
        .rst_ni            ( rst_ni             ),
        // handshake
        .mul_req_valid_i   ( mul_valid_op       ),
        .mul_resp_valid_o  ( mul_valid          ),
        .mul_ready_o       ( mul_ready          ),
        //operand
        .mul_a_i           ( mul_operand_in_fun[0]),
        .mul_b_i           ( mul_operand_in_fun[1]),
        // unsigned / signed
        .is_signed_a       ( sign_a             ),
        .is_signed_b       ( sign_b             ),
        // output
        .result_o          ( mul_output         )
    );

    // ---------------------
    // Division
    // ---------------------
    riscv_pkg::xlen_t           operand_b, operand_a;  // input operands after input MUX (input silencing, word operations or full inputs)
    riscv_pkg::xlen_t           result;                // result before result mux

    logic        div_signed;            // signed or unsigned division
    logic        rem;                   // is it a reminder (or not a reminder e.g.: a division)
    logic        word_op_d, word_op_q;  // save whether the operation was signed or not

    // is this a signed op?
    assign div_signed = fu_data_i.operator inside {DIV, DIVW, REM, REMW};
    // is this a modulo?
    assign rem        = fu_data_i.operator inside {REM, REMU, REMW, REMUW};

    // prepare the input operands and control divider
    always_comb begin
        // silence the inputs
        operand_a   = '0;
        operand_b   = '0;
        // control signals
        word_op_d   = word_op_q;

        // we've go a new division operation
        if (mult_valid_i && fu_data_i.operator inside {DIV, DIVU, DIVW, DIVUW, REM, REMU, REMW, REMUW}) begin
            // is this a word operation?
            if (fu_data_i.operator inside {DIVW, DIVUW, REMW, REMUW}) begin
                // yes so check if we should sign extend this is only done for a signed operation
                if (div_signed) begin
                    operand_a = sext32(fu_data_i.operand_a[31:0]);
                    operand_b = sext32(fu_data_i.operand_b[31:0]);
                end else begin
                    operand_a = fu_data_i.operand_a[31:0];
                    operand_b = fu_data_i.operand_b[31:0];
                end

                // save whether we want sign extend the result or not, this is done for all word operations
                word_op_d = 1'b1;
            end else begin
                // regular op
                operand_a = fu_data_i.operand_a;
                operand_b = fu_data_i.operand_b;
                word_op_d = 1'b0;
            end
        end
    end

    // ---------------------
    // Serial Divider
    // ---------------------
    div #(
        .WIDTH       ( riscv_pkg::XLEN )
    ) i_div (
        .clk_i       ( clk_i                ),
        .rst_ni      ( rst_ni               ),
        .op_a_i      ( operand_a            ),
        .op_b_i      ( operand_b            ),
        .opcode_i    ( {rem, div_signed}    ), // 00: udiv, 10: urem, 01: div, 11: rem
        .in_vld_i    ( div_valid_op         ),
        .in_rdy_o    ( div_ready            ),
        .flush_i     ( flush_i              ),
        .out_vld_o   ( div_valid            ),
        .out_rdy_i   ( div_ready_i          ),
        .res_o       ( result               )
    );

    // Result multiplexer
    // if it was a signed word operation the bit will be set and the result will be sign extended accordingly
    assign div_result = (word_op_q) ? sext32(result) : result;

    // ---------------------
    // Registers
    // ---------------------
    always_ff @(posedge clk_i or negedge rst_ni) begin
        if(~rst_ni) begin
            word_op_q <= '0;
        end else begin
            word_op_q <= word_op_d;
        end
    end
endmodule
