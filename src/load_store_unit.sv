module load_store_unit 
import riscv_pkg::*;
import apd_pkg::*; 
#(
    parameter DATAWIDTH     = riscv_pkg::XLEN,
    parameter LINEWIDTH     = apd_pkg::ApdDefaultConfig.DCacheWidth,
    parameter VADDRWIDTH    = riscv_pkg::VLEN,
    parameter PADDRWIDTH    = riscv_pkg::PLEN
)
(
        input  logic                   clk_i,
        input  logic                   rst_ni,
        
        // to / from ex
        input  logic                   rw_en_i,   // 0 read, 1 write
        input  logic                   req_valid_i,    // mem req enable
        input  ls_length_t             req_length_i,   // L8, L16, L32, L64
        input  logic                   load_signed_i,   //0: unsigned ext,1: sext
        input  logic [VADDRWIDTH-1:0]  dm_addr_i,   // data mem w/r address
        input  logic [DATAWIDTH-1:0]   data_i,    // data to mem
        input  logic                   fence_en_i,
        
        output logic [DATAWIDTH-1:0]   result_o,  //data to regfile 
        output exception_t             lsu_exception_o,
        output logic                   load_finish_o,
        output logic                   lsu_ready_o,
        
        // mmu interface
        output lsu_req_lsu2mmu_t       lsu2mmu_o,
        input  lsu_req_mmu2lsu_t       mmu2lsu_i,
        input  logic                   en_ld_st_translation_i,

        // to / from mem
        output dcache_req_lsu2dc_t     lsu2dc_o,
        input  dcache_req_dc2lsu_t     dc2lsu_i // its vaddr not used yet
);
    localparam int BYTEPERDATA = DATAWIDTH/8;
    localparam int LOG2REGOFFSET  = $clog2(BYTEPERDATA);
    localparam int LOG2DCLOFFSET  = $clog2(LINEWIDTH/8);
    
    
    // to control simplified
    logic lsu_ready_q;
    logic same_vaddr_en;
    always_ff @( posedge clk_i or negedge rst_ni ) begin
        if(!rst_ni)
            lsu_ready_q <= '0;
        else 
            lsu_ready_q <= dc2lsu_i.ready;
    end
    assign load_finish_o  = dc2lsu_i.valid;
    assign lsu_ready_o    = lsu_ready_q && (lsu2mmu_o.lsu_req ? mmu2lsu_i.lsu_valid && same_vaddr_en : 1); 

    lsu_data_t lsu_data_d, lsu_data_q, lsu_data_in_fun;
    assign lsu_data_d = '{
        rw_en         : rw_en_i,   // 0 read, 1 write
        req_valid     : req_valid_i,    // mem req enable
        req_length    : req_length_i,   // L8, L16, L32, L64
        load_signed   : load_signed_i,   //0: unsigned ext,1: sext
        dm_addr       : dm_addr_i,   // data mem w/r address
        data          : data_i,
        fence         : fence_en_i
    };

    always_ff @(posedge clk_i or negedge rst_ni) begin : mul_operator_ff
        if(!rst_ni) begin
            lsu_data_q      <= '0;
        end else if((req_valid_i && lsu_ready_q) || fence_en_i) begin
            lsu_data_q      <= lsu_data_d;
        end
    end

    assign lsu_data_in_fun = lsu_ready_q ? lsu_data_d : lsu_data_q;
    /////////////
    
    // ----------
    // lsu2mmu
    // ----------
    assign lsu2mmu_o = '{
            misaligned_ex   : '0, // TODO
            lsu_req         : lsu_data_in_fun.req_valid && en_ld_st_translation_i,
            lsu_vaddr       : { lsu_data_in_fun.dm_addr[VADDRWIDTH-1:LOG2REGOFFSET], {LOG2REGOFFSET{1'b0}} },
            lsu_is_store    : lsu_data_in_fun.rw_en
    };
        
    always_comb begin : lsu_exception_comb
        lsu_exception_o       = mmu2lsu_i.lsu_exception;
        lsu_exception_o.valid = mmu2lsu_i.lsu_exception.valid && mmu2lsu_i.lsu_valid && en_ld_st_translation_i;
    end
    // ----------
    // lsu2dc_o
    // ----------
    logic [DATAWIDTH-1:0]   store_data;
    logic [DATAWIDTH/8-1:0] byte_enable;
    logic [riscv_pkg::VLEN-1:0] vaddr_req_q;

    assign same_vaddr_en = lsu2mmu_o.lsu_vaddr == vaddr_req_q;
    always_ff @(posedge clk_i or negedge rst_ni) begin
        if(!rst_ni) begin
            vaddr_req_q <= '0;
        end else if(req_valid_i) begin
            vaddr_req_q <= lsu2mmu_o.lsu_vaddr;
        end
    end

    assign lsu2dc_o = '{
        rw_req      : lsu_data_in_fun.rw_en,
        valid       : lsu_data_in_fun.req_valid && (en_ld_st_translation_i ? mmu2lsu_i.lsu_valid && same_vaddr_en && !mmu2lsu_i.lsu_exception.valid : 1),
        paddr       : en_ld_st_translation_i ? mmu2lsu_i.lsu_paddr : { lsu_data_in_fun.dm_addr[VADDRWIDTH-1:LOG2REGOFFSET], {LOG2REGOFFSET{1'b0}} },
        data        : store_data,
        byte_mask   : byte_enable,
        fence       : lsu_data_in_fun.fence
    };
    // assign lsu2dc_o.rw_req = lsu_data_in_fun.rw_en;// & rst_ni;
    // assign lsu2dc_o.valid  = lsu_data_in_fun.req_valid;// & rst_ni;
    // assign lsu2dc_o.vaddr  = { lsu_data_in_fun.dm_addr[VADDRWIDTH-1:LOG2REGOFFSET], {LOG2REGOFFSET{1'b0}} } /*& {$bits(lsu2dc_o.vaddr){rst_ni}}*//* & 39'hffffffff*/; // TODO, 0xffffffff to mask high 32 bit ffffffff...
    // assign lsu2dc_o.data   = store_data;// & {$bits(lsu2dc_o.data){rst_ni}};
    // assign lsu2dc_o.byte_mask = byte_enable;// & {$bits(lsu2dc_o.byte_mask){rst_ni}};
    // assign lsu2dc_o.fence  = lsu_data_in_fun.fence;
    
    logic a_lsu_80007010, a_lsu_pt;
    assign a_lsu_80007010 = lsu2dc_o.valid && (lsu2dc_o.paddr == 'h80007010) && lsu2dc_o.rw_req;
    assign a_lsu_pt = lsu2dc_o.valid && (lsu2dc_o.paddr >= 'h80007000 && lsu2dc_o.paddr < 'h80007040) && lsu2dc_o.rw_req;
    // assign a_lsu_pt = lsu2dc_o.valid && (lsu2dc_o.vaddr >= 'h80004000 && lsu2dc_o.vaddr <= 'h800083ee) && lsu2dc_o.rw_req;
    
//    assign lsu2dc_o.rw_req = '0;
//    assign lsu2dc_o.valid  = '0;
//    assign lsu2dc_o.vaddr  = '0;
//    assign lsu2dc_o.data   = '0;
//    assign lsu2dc_o.byte_mask = '0;
    
    //store
    always_comb begin: store_result
        unique case(lsu_data_in_fun.req_length)
            L8: store_data = {BYTEPERDATA{lsu_data_in_fun.data[7:0]}};// lb, L8
            L16: begin 
                if(&lsu_data_in_fun.dm_addr[LOG2REGOFFSET-1:0]) begin // TODO misaligned exception, 111
                end
                unique case(lsu_data_in_fun.dm_addr[0:0]) // lh, L16                  
                    1'b0: store_data =  {       (BYTEPERDATA/2){lsu_data_in_fun.data[15:0]}            }; // even aligned
                    1'b1: store_data =  { 8'b0, {(BYTEPERDATA/2-1){lsu_data_in_fun.data[15:0]}}, 8'b0  }; // odd  aligned
                endcase
            end
            L32: begin
                unique case(lsu_data_in_fun.dm_addr[1:0]) // lh, L16                  
                    2'b00: store_data =  {       (BYTEPERDATA/4){lsu_data_in_fun.data[31:0]}            }; // even aligned
                    2'b01: store_data =  {{(DATAWIDTH-32-8){1'b0}}, {lsu_data_in_fun.data[31:0]}, 8'b0  }; 
                    2'b10: store_data =  {{(DATAWIDTH-32-16){1'b0}}, {lsu_data_in_fun.data[31:0]}, 16'b0}; 
                    2'b11: store_data =  {{(DATAWIDTH-32-24){1'b0}}, {lsu_data_in_fun.data[31:0]}, 24'b0}; 
                endcase 
                // TODO misaligned, exception 101, 110, 111
            end
            default: begin // L64
                store_data =   lsu_data_in_fun.data;//TODO misaligned, exception, 001, 010, 011, 100, 101, 110, 111
            end
        endcase
    end


    // -------------------------------------------
    // auto generated mux by lsu_load_store_gen.c
    // -------------------------------------------
    always_comb begin: load_result
        unique case(lsu_data_in_fun.req_length)
            L8: unique case(lsu_data_in_fun.dm_addr[6-1:0]) // LOG2DCLOFFSET - 1
                6'd0: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[7]} }, dc2lsu_i.data[0 +: 8]  };
                6'd1: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[15]} }, dc2lsu_i.data[8 +: 8]  };
                6'd2: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[23]} }, dc2lsu_i.data[16 +: 8]  };
                6'd3: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[31]} }, dc2lsu_i.data[24 +: 8]  };
                6'd4: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[39]} }, dc2lsu_i.data[32 +: 8]  };
                6'd5: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[47]} }, dc2lsu_i.data[40 +: 8]  };
                6'd6: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[55]} }, dc2lsu_i.data[48 +: 8]  };
                6'd7: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[63]} }, dc2lsu_i.data[56 +: 8]  };
                6'd8: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[71]} }, dc2lsu_i.data[64 +: 8]  };
                6'd9: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[79]} }, dc2lsu_i.data[72 +: 8]  };
                6'd10: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[87]} }, dc2lsu_i.data[80 +: 8]  };
                6'd11: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[95]} }, dc2lsu_i.data[88 +: 8]  };
                6'd12: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[103]} }, dc2lsu_i.data[96 +: 8]  };
                6'd13: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[111]} }, dc2lsu_i.data[104 +: 8]  };
                6'd14: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[119]} }, dc2lsu_i.data[112 +: 8]  };
                6'd15: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[127]} }, dc2lsu_i.data[120 +: 8]  };
                6'd16: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[135]} }, dc2lsu_i.data[128 +: 8]  };
                6'd17: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[143]} }, dc2lsu_i.data[136 +: 8]  };
                6'd18: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[151]} }, dc2lsu_i.data[144 +: 8]  };
                6'd19: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[159]} }, dc2lsu_i.data[152 +: 8]  };
                6'd20: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[167]} }, dc2lsu_i.data[160 +: 8]  };
                6'd21: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[175]} }, dc2lsu_i.data[168 +: 8]  };
                6'd22: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[183]} }, dc2lsu_i.data[176 +: 8]  };
                6'd23: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[191]} }, dc2lsu_i.data[184 +: 8]  };
                6'd24: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[199]} }, dc2lsu_i.data[192 +: 8]  };
                6'd25: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[207]} }, dc2lsu_i.data[200 +: 8]  };
                6'd26: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[215]} }, dc2lsu_i.data[208 +: 8]  };
                6'd27: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[223]} }, dc2lsu_i.data[216 +: 8]  };
                6'd28: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[231]} }, dc2lsu_i.data[224 +: 8]  };
                6'd29: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[239]} }, dc2lsu_i.data[232 +: 8]  };
                6'd30: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[247]} }, dc2lsu_i.data[240 +: 8]  };
                6'd31: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[255]} }, dc2lsu_i.data[248 +: 8]  };
                6'd32: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[263]} }, dc2lsu_i.data[256 +: 8]  };
                6'd33: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[271]} }, dc2lsu_i.data[264 +: 8]  };
                6'd34: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[279]} }, dc2lsu_i.data[272 +: 8]  };
                6'd35: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[287]} }, dc2lsu_i.data[280 +: 8]  };
                6'd36: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[295]} }, dc2lsu_i.data[288 +: 8]  };
                6'd37: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[303]} }, dc2lsu_i.data[296 +: 8]  };
                6'd38: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[311]} }, dc2lsu_i.data[304 +: 8]  };
                6'd39: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[319]} }, dc2lsu_i.data[312 +: 8]  };
                6'd40: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[327]} }, dc2lsu_i.data[320 +: 8]  };
                6'd41: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[335]} }, dc2lsu_i.data[328 +: 8]  };
                6'd42: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[343]} }, dc2lsu_i.data[336 +: 8]  };
                6'd43: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[351]} }, dc2lsu_i.data[344 +: 8]  };
                6'd44: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[359]} }, dc2lsu_i.data[352 +: 8]  };
                6'd45: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[367]} }, dc2lsu_i.data[360 +: 8]  };
                6'd46: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[375]} }, dc2lsu_i.data[368 +: 8]  };
                6'd47: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[383]} }, dc2lsu_i.data[376 +: 8]  };
                6'd48: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[391]} }, dc2lsu_i.data[384 +: 8]  };
                6'd49: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[399]} }, dc2lsu_i.data[392 +: 8]  };
                6'd50: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[407]} }, dc2lsu_i.data[400 +: 8]  };
                6'd51: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[415]} }, dc2lsu_i.data[408 +: 8]  };
                6'd52: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[423]} }, dc2lsu_i.data[416 +: 8]  };
                6'd53: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[431]} }, dc2lsu_i.data[424 +: 8]  };
                6'd54: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[439]} }, dc2lsu_i.data[432 +: 8]  };
                6'd55: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[447]} }, dc2lsu_i.data[440 +: 8]  };
                6'd56: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[455]} }, dc2lsu_i.data[448 +: 8]  };
                6'd57: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[463]} }, dc2lsu_i.data[456 +: 8]  };
                6'd58: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[471]} }, dc2lsu_i.data[464 +: 8]  };
                6'd59: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[479]} }, dc2lsu_i.data[472 +: 8]  };
                6'd60: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[487]} }, dc2lsu_i.data[480 +: 8]  };
                6'd61: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[495]} }, dc2lsu_i.data[488 +: 8]  };
                6'd62: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[503]} }, dc2lsu_i.data[496 +: 8]  };
                6'd63: result_o = { {(DATAWIDTH-8){lsu_data_in_fun.load_signed&dc2lsu_i.data[511]} }, dc2lsu_i.data[504 +: 8]  };
                default:; // TODO, misalign exception
            endcase
            L16: unique case(lsu_data_in_fun.dm_addr[6-1:0]) // LOG2DCLOFFSET - 1
                6'd0: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[15]} }, dc2lsu_i.data[0 +: 16]  };
                6'd1: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[23]} }, dc2lsu_i.data[8 +: 16]  };
                6'd2: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[31]} }, dc2lsu_i.data[16 +: 16]  };
                6'd3: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[39]} }, dc2lsu_i.data[24 +: 16]  };
                6'd4: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[47]} }, dc2lsu_i.data[32 +: 16]  };
                6'd5: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[55]} }, dc2lsu_i.data[40 +: 16]  };
                6'd6: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[63]} }, dc2lsu_i.data[48 +: 16]  };
                6'd7: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[71]} }, dc2lsu_i.data[56 +: 16]  };
                6'd8: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[79]} }, dc2lsu_i.data[64 +: 16]  };
                6'd9: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[87]} }, dc2lsu_i.data[72 +: 16]  };
                6'd10: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[95]} }, dc2lsu_i.data[80 +: 16]  };
                6'd11: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[103]} }, dc2lsu_i.data[88 +: 16]  };
                6'd12: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[111]} }, dc2lsu_i.data[96 +: 16]  };
                6'd13: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[119]} }, dc2lsu_i.data[104 +: 16]  };
                6'd14: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[127]} }, dc2lsu_i.data[112 +: 16]  };
                6'd15: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[135]} }, dc2lsu_i.data[120 +: 16]  };
                6'd16: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[143]} }, dc2lsu_i.data[128 +: 16]  };
                6'd17: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[151]} }, dc2lsu_i.data[136 +: 16]  };
                6'd18: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[159]} }, dc2lsu_i.data[144 +: 16]  };
                6'd19: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[167]} }, dc2lsu_i.data[152 +: 16]  };
                6'd20: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[175]} }, dc2lsu_i.data[160 +: 16]  };
                6'd21: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[183]} }, dc2lsu_i.data[168 +: 16]  };
                6'd22: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[191]} }, dc2lsu_i.data[176 +: 16]  };
                6'd23: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[199]} }, dc2lsu_i.data[184 +: 16]  };
                6'd24: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[207]} }, dc2lsu_i.data[192 +: 16]  };
                6'd25: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[215]} }, dc2lsu_i.data[200 +: 16]  };
                6'd26: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[223]} }, dc2lsu_i.data[208 +: 16]  };
                6'd27: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[231]} }, dc2lsu_i.data[216 +: 16]  };
                6'd28: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[239]} }, dc2lsu_i.data[224 +: 16]  };
                6'd29: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[247]} }, dc2lsu_i.data[232 +: 16]  };
                6'd30: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[255]} }, dc2lsu_i.data[240 +: 16]  };
                6'd31: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[263]} }, dc2lsu_i.data[248 +: 16]  };
                6'd32: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[271]} }, dc2lsu_i.data[256 +: 16]  };
                6'd33: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[279]} }, dc2lsu_i.data[264 +: 16]  };
                6'd34: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[287]} }, dc2lsu_i.data[272 +: 16]  };
                6'd35: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[295]} }, dc2lsu_i.data[280 +: 16]  };
                6'd36: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[303]} }, dc2lsu_i.data[288 +: 16]  };
                6'd37: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[311]} }, dc2lsu_i.data[296 +: 16]  };
                6'd38: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[319]} }, dc2lsu_i.data[304 +: 16]  };
                6'd39: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[327]} }, dc2lsu_i.data[312 +: 16]  };
                6'd40: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[335]} }, dc2lsu_i.data[320 +: 16]  };
                6'd41: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[343]} }, dc2lsu_i.data[328 +: 16]  };
                6'd42: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[351]} }, dc2lsu_i.data[336 +: 16]  };
                6'd43: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[359]} }, dc2lsu_i.data[344 +: 16]  };
                6'd44: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[367]} }, dc2lsu_i.data[352 +: 16]  };
                6'd45: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[375]} }, dc2lsu_i.data[360 +: 16]  };
                6'd46: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[383]} }, dc2lsu_i.data[368 +: 16]  };
                6'd47: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[391]} }, dc2lsu_i.data[376 +: 16]  };
                6'd48: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[399]} }, dc2lsu_i.data[384 +: 16]  };
                6'd49: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[407]} }, dc2lsu_i.data[392 +: 16]  };
                6'd50: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[415]} }, dc2lsu_i.data[400 +: 16]  };
                6'd51: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[423]} }, dc2lsu_i.data[408 +: 16]  };
                6'd52: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[431]} }, dc2lsu_i.data[416 +: 16]  };
                6'd53: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[439]} }, dc2lsu_i.data[424 +: 16]  };
                6'd54: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[447]} }, dc2lsu_i.data[432 +: 16]  };
                6'd55: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[455]} }, dc2lsu_i.data[440 +: 16]  };
                6'd56: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[463]} }, dc2lsu_i.data[448 +: 16]  };
                6'd57: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[471]} }, dc2lsu_i.data[456 +: 16]  };
                6'd58: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[479]} }, dc2lsu_i.data[464 +: 16]  };
                6'd59: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[487]} }, dc2lsu_i.data[472 +: 16]  };
                6'd60: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[495]} }, dc2lsu_i.data[480 +: 16]  };
                6'd61: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[503]} }, dc2lsu_i.data[488 +: 16]  };
                6'd62: result_o = { {(DATAWIDTH-16){lsu_data_in_fun.load_signed&dc2lsu_i.data[511]} }, dc2lsu_i.data[496 +: 16]  };
                default:; // TODO, misalign exception
            endcase
            L32: unique case(lsu_data_in_fun.dm_addr[6-1:0]) // LOG2DCLOFFSET - 1
                6'd0: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[31]} }, dc2lsu_i.data[0 +: 32]  };
                6'd1: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[39]} }, dc2lsu_i.data[8 +: 32]  };
                6'd2: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[47]} }, dc2lsu_i.data[16 +: 32]  };
                6'd3: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[55]} }, dc2lsu_i.data[24 +: 32]  };
                6'd4: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[63]} }, dc2lsu_i.data[32 +: 32]  };
                6'd5: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[71]} }, dc2lsu_i.data[40 +: 32]  };
                6'd6: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[79]} }, dc2lsu_i.data[48 +: 32]  };
                6'd7: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[87]} }, dc2lsu_i.data[56 +: 32]  };
                6'd8: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[95]} }, dc2lsu_i.data[64 +: 32]  };
                6'd9: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[103]} }, dc2lsu_i.data[72 +: 32]  };
                6'd10: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[111]} }, dc2lsu_i.data[80 +: 32]  };
                6'd11: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[119]} }, dc2lsu_i.data[88 +: 32]  };
                6'd12: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[127]} }, dc2lsu_i.data[96 +: 32]  };
                6'd13: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[135]} }, dc2lsu_i.data[104 +: 32]  };
                6'd14: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[143]} }, dc2lsu_i.data[112 +: 32]  };
                6'd15: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[151]} }, dc2lsu_i.data[120 +: 32]  };
                6'd16: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[159]} }, dc2lsu_i.data[128 +: 32]  };
                6'd17: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[167]} }, dc2lsu_i.data[136 +: 32]  };
                6'd18: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[175]} }, dc2lsu_i.data[144 +: 32]  };
                6'd19: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[183]} }, dc2lsu_i.data[152 +: 32]  };
                6'd20: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[191]} }, dc2lsu_i.data[160 +: 32]  };
                6'd21: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[199]} }, dc2lsu_i.data[168 +: 32]  };
                6'd22: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[207]} }, dc2lsu_i.data[176 +: 32]  };
                6'd23: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[215]} }, dc2lsu_i.data[184 +: 32]  };
                6'd24: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[223]} }, dc2lsu_i.data[192 +: 32]  };
                6'd25: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[231]} }, dc2lsu_i.data[200 +: 32]  };
                6'd26: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[239]} }, dc2lsu_i.data[208 +: 32]  };
                6'd27: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[247]} }, dc2lsu_i.data[216 +: 32]  };
                6'd28: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[255]} }, dc2lsu_i.data[224 +: 32]  };
                6'd29: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[263]} }, dc2lsu_i.data[232 +: 32]  };
                6'd30: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[271]} }, dc2lsu_i.data[240 +: 32]  };
                6'd31: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[279]} }, dc2lsu_i.data[248 +: 32]  };
                6'd32: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[287]} }, dc2lsu_i.data[256 +: 32]  };
                6'd33: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[295]} }, dc2lsu_i.data[264 +: 32]  };
                6'd34: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[303]} }, dc2lsu_i.data[272 +: 32]  };
                6'd35: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[311]} }, dc2lsu_i.data[280 +: 32]  };
                6'd36: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[319]} }, dc2lsu_i.data[288 +: 32]  };
                6'd37: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[327]} }, dc2lsu_i.data[296 +: 32]  };
                6'd38: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[335]} }, dc2lsu_i.data[304 +: 32]  };
                6'd39: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[343]} }, dc2lsu_i.data[312 +: 32]  };
                6'd40: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[351]} }, dc2lsu_i.data[320 +: 32]  };
                6'd41: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[359]} }, dc2lsu_i.data[328 +: 32]  };
                6'd42: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[367]} }, dc2lsu_i.data[336 +: 32]  };
                6'd43: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[375]} }, dc2lsu_i.data[344 +: 32]  };
                6'd44: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[383]} }, dc2lsu_i.data[352 +: 32]  };
                6'd45: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[391]} }, dc2lsu_i.data[360 +: 32]  };
                6'd46: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[399]} }, dc2lsu_i.data[368 +: 32]  };
                6'd47: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[407]} }, dc2lsu_i.data[376 +: 32]  };
                6'd48: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[415]} }, dc2lsu_i.data[384 +: 32]  };
                6'd49: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[423]} }, dc2lsu_i.data[392 +: 32]  };
                6'd50: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[431]} }, dc2lsu_i.data[400 +: 32]  };
                6'd51: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[439]} }, dc2lsu_i.data[408 +: 32]  };
                6'd52: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[447]} }, dc2lsu_i.data[416 +: 32]  };
                6'd53: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[455]} }, dc2lsu_i.data[424 +: 32]  };
                6'd54: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[463]} }, dc2lsu_i.data[432 +: 32]  };
                6'd55: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[471]} }, dc2lsu_i.data[440 +: 32]  };
                6'd56: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[479]} }, dc2lsu_i.data[448 +: 32]  };
                6'd57: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[487]} }, dc2lsu_i.data[456 +: 32]  };
                6'd58: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[495]} }, dc2lsu_i.data[464 +: 32]  };
                6'd59: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[503]} }, dc2lsu_i.data[472 +: 32]  };
                6'd60: result_o = { {(DATAWIDTH-32){lsu_data_in_fun.load_signed&dc2lsu_i.data[511]} }, dc2lsu_i.data[480 +: 32]  };
                default:; // TODO, misalign exception
            endcase
            L64: unique case(lsu_data_in_fun.dm_addr[6-1:0]) // LOG2DCLOFFSET - 1
                6'd0: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[63]} }, dc2lsu_i.data[0 +: 64]  };
                6'd1: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[71]} }, dc2lsu_i.data[8 +: 64]  };
                6'd2: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[79]} }, dc2lsu_i.data[16 +: 64]  };
                6'd3: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[87]} }, dc2lsu_i.data[24 +: 64]  };
                6'd4: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[95]} }, dc2lsu_i.data[32 +: 64]  };
                6'd5: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[103]} }, dc2lsu_i.data[40 +: 64]  };
                6'd6: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[111]} }, dc2lsu_i.data[48 +: 64]  };
                6'd7: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[119]} }, dc2lsu_i.data[56 +: 64]  };
                6'd8: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[127]} }, dc2lsu_i.data[64 +: 64]  };
                6'd9: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[135]} }, dc2lsu_i.data[72 +: 64]  };
                6'd10: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[143]} }, dc2lsu_i.data[80 +: 64]  };
                6'd11: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[151]} }, dc2lsu_i.data[88 +: 64]  };
                6'd12: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[159]} }, dc2lsu_i.data[96 +: 64]  };
                6'd13: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[167]} }, dc2lsu_i.data[104 +: 64]  };
                6'd14: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[175]} }, dc2lsu_i.data[112 +: 64]  };
                6'd15: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[183]} }, dc2lsu_i.data[120 +: 64]  };
                6'd16: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[191]} }, dc2lsu_i.data[128 +: 64]  };
                6'd17: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[199]} }, dc2lsu_i.data[136 +: 64]  };
                6'd18: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[207]} }, dc2lsu_i.data[144 +: 64]  };
                6'd19: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[215]} }, dc2lsu_i.data[152 +: 64]  };
                6'd20: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[223]} }, dc2lsu_i.data[160 +: 64]  };
                6'd21: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[231]} }, dc2lsu_i.data[168 +: 64]  };
                6'd22: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[239]} }, dc2lsu_i.data[176 +: 64]  };
                6'd23: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[247]} }, dc2lsu_i.data[184 +: 64]  };
                6'd24: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[255]} }, dc2lsu_i.data[192 +: 64]  };
                6'd25: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[263]} }, dc2lsu_i.data[200 +: 64]  };
                6'd26: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[271]} }, dc2lsu_i.data[208 +: 64]  };
                6'd27: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[279]} }, dc2lsu_i.data[216 +: 64]  };
                6'd28: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[287]} }, dc2lsu_i.data[224 +: 64]  };
                6'd29: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[295]} }, dc2lsu_i.data[232 +: 64]  };
                6'd30: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[303]} }, dc2lsu_i.data[240 +: 64]  };
                6'd31: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[311]} }, dc2lsu_i.data[248 +: 64]  };
                6'd32: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[319]} }, dc2lsu_i.data[256 +: 64]  };
                6'd33: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[327]} }, dc2lsu_i.data[264 +: 64]  };
                6'd34: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[335]} }, dc2lsu_i.data[272 +: 64]  };
                6'd35: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[343]} }, dc2lsu_i.data[280 +: 64]  };
                6'd36: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[351]} }, dc2lsu_i.data[288 +: 64]  };
                6'd37: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[359]} }, dc2lsu_i.data[296 +: 64]  };
                6'd38: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[367]} }, dc2lsu_i.data[304 +: 64]  };
                6'd39: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[375]} }, dc2lsu_i.data[312 +: 64]  };
                6'd40: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[383]} }, dc2lsu_i.data[320 +: 64]  };
                6'd41: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[391]} }, dc2lsu_i.data[328 +: 64]  };
                6'd42: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[399]} }, dc2lsu_i.data[336 +: 64]  };
                6'd43: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[407]} }, dc2lsu_i.data[344 +: 64]  };
                6'd44: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[415]} }, dc2lsu_i.data[352 +: 64]  };
                6'd45: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[423]} }, dc2lsu_i.data[360 +: 64]  };
                6'd46: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[431]} }, dc2lsu_i.data[368 +: 64]  };
                6'd47: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[439]} }, dc2lsu_i.data[376 +: 64]  };
                6'd48: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[447]} }, dc2lsu_i.data[384 +: 64]  };
                6'd49: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[455]} }, dc2lsu_i.data[392 +: 64]  };
                6'd50: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[463]} }, dc2lsu_i.data[400 +: 64]  };
                6'd51: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[471]} }, dc2lsu_i.data[408 +: 64]  };
                6'd52: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[479]} }, dc2lsu_i.data[416 +: 64]  };
                6'd53: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[487]} }, dc2lsu_i.data[424 +: 64]  };
                6'd54: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[495]} }, dc2lsu_i.data[432 +: 64]  };
                6'd55: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[503]} }, dc2lsu_i.data[440 +: 64]  };
                6'd56: result_o = { {(DATAWIDTH-64){lsu_data_in_fun.load_signed&dc2lsu_i.data[511]} }, dc2lsu_i.data[448 +: 64]  };
                default:; // TODO, misalign exception
            endcase
            default:;
        endcase
    end


    always_comb begin: store_byte_enable
        byte_enable = '0;
        unique case(lsu_data_in_fun.req_length)
            L8: unique case(lsu_data_in_fun.dm_addr[3-1:0]) // LOG2REGOFFSET - 1
                3'd0: byte_enable = 8'b1;
                3'd1: byte_enable = 8'b10;
                3'd2: byte_enable = 8'b100;
                3'd3: byte_enable = 8'b1000;
                3'd4: byte_enable = 8'b10000;
                3'd5: byte_enable = 8'b100000;
                3'd6: byte_enable = 8'b1000000;
                3'd7: byte_enable = 8'b10000000;
                default:; // TODO, misalign exception
            endcase
            L16: unique case(lsu_data_in_fun.dm_addr[3-1:0]) // LOG2REGOFFSET - 1
                3'd0: byte_enable = 8'b11;
                3'd1: byte_enable = 8'b110;
                3'd2: byte_enable = 8'b1100;
                3'd3: byte_enable = 8'b11000;
                3'd4: byte_enable = 8'b110000;
                3'd5: byte_enable = 8'b1100000;
                3'd6: byte_enable = 8'b11000000;
                default:; // TODO, misalign exception
            endcase
            L32: unique case(lsu_data_in_fun.dm_addr[3-1:0]) // LOG2REGOFFSET - 1
                3'd0: byte_enable = 8'b1111;
                3'd1: byte_enable = 8'b11110;
                3'd2: byte_enable = 8'b111100;
                3'd3: byte_enable = 8'b1111000;
                3'd4: byte_enable = 8'b11110000;
                default:; // TODO, misalign exception
            endcase
            L64: unique case(lsu_data_in_fun.dm_addr[3-1:0]) // LOG2REGOFFSET - 1
                3'd0: byte_enable = 8'b11111111;
                default:; // TODO, misalign exception
            endcase
            default:;
        endcase
    end




endmodule
