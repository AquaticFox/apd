
module ex_stage 
import riscv_pkg::*;
import apd_pkg::*; 
#(
)(
    input  logic                    clk_i,
    input  logic                    rst_ni,
    input  logic                    flush_i,
   

    // from ISSUE
    input  fu_data_t            is_entry_i,
    input  logic                is_entry_valid_i,
    output logic                ex_ready_o,
    
    // to COMMIT
    input  logic                ma_ready_i,
    // to CSR
    output fu_data_t            csr_data_o,
    input  riscv_pkg::xlen_t    csr_result_i,   // data from csr to write in gpr
    input  exception_t          csr_exception_i,

    // to ISSUE for write back
    output ex2wb_t              ex2wb_o,
    
    // to ISSUE for commit
    output logic                commit_valid_o,
    
    // to control for flush
    output bp_resolve_t         resolved_branch_o,
    output logic                resolved_branch_valid_o,
    
    // mmu interface
    output lsu_req_lsu2mmu_t    lsu2mmu_o,
    input  lsu_req_mmu2lsu_t    mmu2lsu_i,
    output logic                sfence_vma_en_o,
    input  logic                en_ld_st_translation_i,

    // to/from memory
    output dcache_req_lsu2dc_t     lsu2dc_o,
    output logic                   dc_fence_o,
    input  dcache_req_dc2lsu_t     dc2lsu_i, // its vaddr not used yet
    
    // for CSR
    input  logic[riscv_pkg::VLEN-1:0] boot_addr_i, // Address from which to start booting, mtvec is set to the same address
    input  logic[riscv_pkg::XLEN-1:0] hart_id_i   // Hart id in a multicore environment (reflected in a CSR)
);

    // ---------------
    // Function units
    // ---------------
    logic alu_branch_res; // branch comparison result
    riscv_pkg::xlen_t alu_result, mult_result, lsu_result, csr_result; 
    logic mult_valid;
    logic mult_ready;
    logic lsu_valid; // load valid
    logic lsu_ready;
    logic [riscv_pkg::VLEN-1:0] branch_result_rd; 
    logic need_wb;
    exception_t lsu_exception;  
    
    // pipeline register
    fu_data_t  ex_entry_d, ex_entry_q, ex_entry_in_fun;
    logic ex_entry_valid_d, ex_entry_valid_q, ex_entry_valid_in_fun;
    assign ex_entry_d       = is_entry_i;
    assign ex_entry_valid_d = is_entry_valid_i;
    assign ex_entry_in_fun          = ex_ready_o ? ex_entry_d : ex_entry_q;
    assign ex_entry_valid_in_fun    = ex_ready_o ? ex_entry_valid_d : ex_entry_valid_q;
    always_ff @(posedge clk_i or negedge rst_ni) begin : ex_entry_ff
        if(~rst_ni) begin
            ex_entry_q          <= '0;
            ex_entry_valid_q    <= '0;
        end else if(flush_i) begin
            ex_entry_q          <= '0;
            ex_entry_valid_q    <= '0;
        end else if(is_entry_valid_i && ex_ready_o) begin
            ex_entry_q          <= ex_entry_d;
            ex_entry_valid_q    <= ex_entry_valid_d;
        end else if(ex_ready_o && !is_entry_valid_i) begin
            ex_entry_q          <= '0;
            ex_entry_valid_q    <= '0;
        end
    end
    
    assign ex_ready_o = mult_ready && lsu_ready;
    
    assign commit_valid_o = !flush_i && (
                                            ex_entry_in_fun.fu inside { MULT } ? mult_valid :
                                            ex_entry_in_fun.fu inside { LOAD } ? (lsu_valid || lsu_exception.valid) : is_entry_valid_i
                                        )
                                     && !(ex_entry_in_fun.fu inside { NONE } && (|ex_entry_in_fun.fu_valid == 0) && ex_entry_in_fun.ex.valid == 0);
                                    //  && !(ex_entry_in_fun.fu inside { NONE } && ex_entry_in_fun.fu_valid.csr_valid && ex_entry_in_fun.ex.valid && (ex_entry_in_fun.rd == 5'b0) && ex_entry_in_fun.rd_en && is_entry_valid_i && (ex_entry_in_fun.pc == 'h5002));

    assign ex2wb_o.wb_valid = need_wb && commit_valid_o && ex_entry_in_fun.rd_en;
    assign ex2wb_o.wb_rd    = ex_entry_in_fun.rd;
    assign ex2wb_o.wb_rd_en = ex_entry_in_fun.rd_en;
    assign ex2wb_o.pc       = ex_entry_in_fun.pc;
    assign ex2wb_o.ex_rd_q      = ex_entry_q.rd;
    assign ex2wb_o.ex_rd_en_q   = ex_entry_q.rd_en;
    
    always_comb begin : wb_mux_comb
        need_wb = 1'b0;
        case(ex_entry_in_fun.fu)
            LOAD:begin
                    ex2wb_o.wb_data  = lsu_result;
                    need_wb = lsu_valid;
                 end
            ALU: begin
                    ex2wb_o.wb_data  = alu_result;
                    need_wb = 1'b1;
                 end
            MULT: begin
                    ex2wb_o.wb_data  = mult_result;
                    need_wb = mult_valid;
                end
            CTRL_FLOW: begin
                    if(ex_entry_in_fun.rd_en) begin // for jal, jalr // write pc+4/2 into rd
                        ex2wb_o.wb_data  = branch_result_rd;
                        need_wb = 1'b1;
                    end
                end
            CSR: begin
               ex2wb_o.wb_data  = csr_result;
               need_wb = !csr_exception_i.valid;
            end
            default:;
        endcase
    end
   
    
    // --------------------------------------
    // ALU latency 1 cycle, all combinational
    // --------------------------------------
    fu_data_t alu_data;
    assign alu_data = (ex_entry_in_fun.fu_valid.alu_valid || ex_entry_in_fun.fu_valid.branch_valid) && is_entry_valid_i ? ex_entry_in_fun  : '0;
    alu alu_u (
        .clk_i,
        .rst_ni,
        .fu_data_i          ( alu_data          ),
        .result_o           ( alu_result        ),
        .alu_branch_res_o   ( alu_branch_res    )
    );
    
    // ------------------------------------
    // MUL DIV dynamic latency, handshake 
    // ------------------------------------
    fu_data_t mult_data;
    assign mult_data  = is_entry_i.fu_valid.mult_valid && is_entry_valid_i ? is_entry_i  : '0;
    mult mult_u (
        .clk_i      ( clk_i             ),
        .rst_ni     ( rst_ni            ),
        .flush_i    ( flush_i           ),
        .fu_data_i  ( mult_data         ),
        .mult_valid_i( is_entry_i.fu_valid.mult_valid ),
        .result_o   ( mult_result       ),
        .mult_valid_o( mult_valid       ),
        .mult_ready_o( mult_ready       )
    );
    
    // -----------
    // Branch Unit, branch, jump
    // -----------
    assign resolved_branch_valid_o = is_entry_valid_i && ex_ready_o;;
    branch_unit branch_unit_u (
        .clk_i          ( clk_i             ),
        .rst_ni         ( rst_ni            ),
        .fu_data_i      ( is_entry_i        ),
        .pc_i           ( is_entry_i.pc     ),
        .is_compressed_instr_i( is_entry_i.is_rvc ),
        .fu_valid_i     ( '1                ), // TODO
        .branch_valid_i ( is_entry_i.fu_valid.branch_valid),
        .branch_comp_res_i( alu_branch_res  ),
        
        .branch_result_o( branch_result_rd  ),
        .branch_predict_i(is_entry_i.bp     ),
        .resolved_branch_o(resolved_branch_o)
    );
    
    // ----------------
    // Load Store Unit
    // ----------------
    fu_data_t lsu_data;
    assign lsu_data  = is_entry_i.fu_valid.lsu_valid && is_entry_valid_i ? is_entry_i  : '0;
    
    // 1. address generation unit
    logic [riscv_pkg::VLEN-1:0]   lsu_vaddr;
    logic                         lsu_overflow;
    agu agu_u (
        .operand_a      ( lsu_data.operand_a    ),
        .imm            ( lsu_data.imm          ),
        .vaddr_o        ( lsu_vaddr             ),
        .overflow_o     ( lsu_overflow          )
    );

    // 2. load store unit
    logic       lsu_rw_en;
    logic       lsu_valid_en;
    ls_length_t lsu_req_length;
    logic       lsu_load_signed_en;
    logic       fence_en, sfence_vma_en;
    
    assign lsu_rw_en    = (lsu_data.fu == STORE);
    assign lsu_valid_en = lsu_data.fu_valid.lsu_valid;
    always_comb begin: lsu_input_comb
       unique case(lsu_data.operator)
            LD, SD:         
                lsu_req_length = L64;
            LW, LWU, SW:
                lsu_req_length = L32;
            LH, LHU, SH:
                lsu_req_length = L16;
            default : //LB, SB, LBU:
                lsu_req_length = L8;
       endcase 
    end
    assign lsu_load_signed_en = ~(lsu_data.operator inside {LWU, LHU, LBU});
    

    load_store_unit #(
        .DATAWIDTH  ( riscv_pkg::XLEN                       ),
        .LINEWIDTH  ( apd_pkg::ApdDefaultConfig.DCacheWidth ),
        .VADDRWIDTH ( riscv_pkg::VLEN                       ),
        .PADDRWIDTH ( riscv_pkg::PLEN                       )
    ) load_store_unit_u (
        .clk_i              ( clk_i                 ),
        .rst_ni             ( rst_ni                ),
        // to / from ex
        .rw_en_i            ( lsu_rw_en             ),
        .req_valid_i        ( lsu_valid_en          ),
        .req_length_i       ( lsu_req_length        ), // L8, L16, L32, L64
        .load_signed_i      ( lsu_load_signed_en    ),
        .dm_addr_i          ( lsu_vaddr             ),
        .data_i             ( lsu_data.operand_b    ),
        .fence_en_i         ( fence_en              ),
        .result_o           ( lsu_result            ),
        .lsu_exception_o    ( lsu_exception         ),
        .load_finish_o      ( lsu_valid             ),
        .lsu_ready_o        ( lsu_ready             ),
        // mmu interface
        .lsu2mmu_o          ( lsu2mmu_o             ),
        .mmu2lsu_i          ( mmu2lsu_i             ),
        .en_ld_st_translation_i ( en_ld_st_translation_i ),
        // to / from mem
        .lsu2dc_o           ( lsu2dc_o              ),
        .dc2lsu_i           ( dc2lsu_i              )
    );
    assign dc_fence_o   = lsu2dc_o.fence;
    // --------
    // CSR 
    // --------
    fu_data_t csr_data;
    assign csr_data     = lsu_exception.valid ? '{fu_valid: '{csr_valid: 1'b1, default: '0}, ex: lsu_exception, pc: lsu_data.pc, default: '0} 
                        : (is_entry_i.fu_valid.csr_valid && is_entry_valid_i && ex_ready_o ? is_entry_i  : '0);
    assign csr_data_o   = csr_data;
    assign csr_result   = csr_result_i;

    assign fence_en      = csr_data.operator inside {FENCE};
    assign sfence_vma_en = csr_data.operator inside {SFENCE_VMA};
    assign sfence_vma_en_o = sfence_vma_en;


    // always @(*) begin
    //     if(csr_data.ex.valid && csr_data.ex.cause == INSTR_PAGE_FAULT)begin
    //         $display("INSTR_PAGE_FAULT, pc: 0x%x", csr_data.pc);
    //     end
    //     if(csr_data.ex.valid && csr_data.ex.cause == LOAD_PAGE_FAULT)begin
    //         $display("LOAD_PAGE_FAULT, pc: 0x%x", csr_data.pc);
    //     end
    //     if(csr_data.ex.valid && csr_data.ex.cause == STORE_PAGE_FAULT)begin
    //         $display("STORE_PAGE_FAULT, pc: 0x%x", csr_data.pc);
    //     end
    // end
//    logic [63:0] mcycle;
//    always_ff @(posedge clk_i or negedge rst_ni) begin
//       if(!rst_ni) begin
//            mcycle <= '0;
//       end else begin
//            mcycle <= mcycle + 1;
//       end
//   end
//   
//   always_comb begin: read_mcycle_comb
//       csr_result = '0;
//       if( csr_data.fu == CSR 
//           && csr_data.operator == CSR_READ
//           && csr_data.operand_b[11:0] == CSR_MCYCLE
//       ) begin
//           csr_result = mcycle;
//       end
//   end
   
   




//    csr_buffer csr_buffer_u (
//        .clk_i              ( clk_i                 ),
//        .rst_ni             ( rst_ni                ),
//        .flush_i            ( flush_i               ),
//        .fu_data_i(fu_data_i),
//        .csr_ready_o(csr_ready_o),
//        .csr_valid_i(csr_valid_i),
//        .csr_result_o(csr_result_o),
//        .csr_commit_i(csr_commit_i),
//        .csr_addr_o(csr_addr_o)
//    );
endmodule