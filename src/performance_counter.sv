module performance_counter 
import apd_pkg::*; 
import riscv_pkg::*; 
#() 
(
  input  logic                         clk_i,
  input  logic                         flush_i,
  input  logic                         rst_ni,

// performance counter
  input  cache_perf_counter_t l1i2perf_i,
  input  cache_perf_counter_t l1d2perf_i,
  input  cache_perf_counter_t l22perf_i

);

    logic [XLEN-1:0]  l1i_read_req_d, l1i_read_req_q,
                      l1i_write_req_d, l1i_write_req_q,
                      l1d_read_req_d, l1d_read_req_q,
                      l1d_write_req_d, l1d_write_req_q,
                      l2_read_req_d, l2_read_req_q,
                      l2_write_req_d, l2_write_req_q;

    logic [XLEN-1:0]  l1i_read_miss_d, l1i_read_miss_q,
                      l1i_write_miss_d, l1i_write_miss_q,
                      l1d_read_miss_d, l1d_read_miss_q,
                      l1d_write_miss_d, l1d_write_miss_q,
                      l2_read_miss_d, l2_read_miss_q,
                      l2_write_miss_d, l2_write_miss_q;
    
    logic [XLEN-1:0]  l1i_hit_cycles_d, l1i_hit_cycles_q,
                      l1i_miss_cycles_d, l1i_miss_cycles_q,
                      l1d_hit_cycles_d, l1d_hit_cycles_q,
                      l1d_miss_cycles_d, l1d_miss_cycles_q,
                      l2_hit_cycles_d, l2_hit_cycles_q,
                      l2_miss_cycles_d, l2_miss_cycles_q,
                      l1i_this_cycles_q,
                      l1d_this_cycles_q,
                      l2_this_cycles_q;

    cache_perf_counter_t  l1i2perf_q, l1d2perf_q, l22perf_q;

    logic l1i_req_en, l1d_req_en, l2_req_en;
    assign l1i_req_en = l1i2perf_i.read_req || l1i2perf_i.write_req;
    assign l1d_req_en = l1d2perf_i.read_req || l1d2perf_i.write_req;
    assign l2_req_en  = l22perf_i.read_req  || l22perf_i.write_req;

    always_comb begin: nextcounter_d_1
        l1i_read_req_d    = l1i_read_req_q;
        l1i_write_req_d   = l1i_write_req_q;
        l1d_read_req_d    = l1d_read_req_q;
        l1d_write_req_d   = l1d_write_req_q;
        l2_read_req_d     = l2_read_req_q;
        l2_write_req_d    = l2_write_req_q;

        l1i_read_miss_d   = l1i_read_miss_q;
        l1i_write_miss_d  = l1i_write_miss_q;
        l1d_read_miss_d   = l1d_read_miss_q;
        l1d_write_miss_d  = l1d_write_miss_q;
        l2_read_miss_d    = l2_read_miss_q;
        l2_write_miss_d   = l2_write_miss_q;

        l1i_hit_cycles_d  = l1i_hit_cycles_q;
        l1i_miss_cycles_d =  l1i_miss_cycles_q;
        l1d_hit_cycles_d  = l1d_hit_cycles_q;
        l1d_miss_cycles_d =  l1d_miss_cycles_q;
        l2_hit_cycles_d   = l2_hit_cycles_q;
        l2_miss_cycles_d  = l2_miss_cycles_q;

        // l1i update
        cache_rw_req_counter_update(
          l1i2perf_i, l1i2perf_q,
          l1i_read_req_q, l1i_write_req_q,
          l1i_read_miss_q, l1i_write_miss_q,
          l1i_hit_cycles_q, l1i_miss_cycles_q, l1i_this_cycles_q,

          l1i_read_req_d, l1i_write_req_d,
          l1i_read_miss_d, l1i_write_miss_d,
          l1i_hit_cycles_d, l1i_miss_cycles_d
        );

        // l1d update
        cache_rw_req_counter_update(
          l1d2perf_i, l1d2perf_q,
          l1d_read_req_q, l1d_write_req_q,
          l1d_read_miss_q, l1d_write_miss_q,
          l1d_hit_cycles_q, l1d_miss_cycles_q, l1d_this_cycles_q,

          l1d_read_req_d, l1d_write_req_d,
          l1d_read_miss_d, l1d_write_miss_d,
          l1d_hit_cycles_d, l1d_miss_cycles_d
        );

        // l2 update
        cache_rw_req_counter_update(
          l22perf_i, l22perf_q,
          l2_read_req_q, l2_write_req_q,
          l2_read_miss_q, l2_write_miss_q,
          l2_hit_cycles_q, l2_miss_cycles_q, l2_this_cycles_q,

          l2_read_req_d, l2_write_req_d,
          l2_read_miss_d, l2_write_miss_d,
          l2_hit_cycles_d, l2_miss_cycles_d
        );

    end


    always_ff @( posedge clk_i or negedge rst_ni ) begin : nextcounter_q
        if(!rst_ni) begin
          l1i_read_req_q   <= '0;
          l1i_write_req_q  <= '0;
          l1d_read_req_q   <= '0;
          l1d_write_req_q  <= '0;
          l2_read_req_q    <= '0;
          l2_write_req_q   <= '0;

          l1i_read_miss_q  <= '0;
          l1i_write_miss_q <= '0;
          l1d_read_miss_q  <= '0;
          l1d_write_miss_q <= '0;
          l2_read_miss_q   <= '0;
          l2_write_miss_q  <= '0;

          l1i_hit_cycles_q  <= '0;
          l1i_miss_cycles_q <= '0;
          l1d_hit_cycles_q  <= '0; 
          l1d_miss_cycles_q <= '0;
          l2_hit_cycles_q   <= '0; 
          l2_miss_cycles_q  <= '0;

        end else begin
          l1i_read_req_q      <= l1i_read_req_d;
          l1i_write_req_q     <= l1i_write_req_d;
          l1d_read_req_q      <= l1d_read_req_d;
          l1d_write_req_q     <= l1d_write_req_d;
          l2_read_req_q       <= l2_read_req_d;
          l2_write_req_q      <= l2_write_req_d;

          l1i_read_miss_q     <= l1i_read_miss_d;
          l1i_write_miss_q    <= l1i_write_miss_d;
          l1d_read_miss_q     <= l1d_read_miss_d;
          l1d_write_miss_q    <= l1d_write_miss_d;
          l2_read_miss_q      <= l2_read_miss_d;
          l2_write_miss_q     <= l2_write_miss_d;

          l1i_hit_cycles_q  <= l1i_hit_cycles_d;
          l1i_miss_cycles_q <= l1i_miss_cycles_d;
          l1d_hit_cycles_q  <= l1d_hit_cycles_d; 
          l1d_miss_cycles_q <= l1d_miss_cycles_d;
          l2_hit_cycles_q   <= l2_hit_cycles_d; 
          l2_miss_cycles_q  <= l2_miss_cycles_d;
        end
    end

    always_ff @(posedge clk_i or negedge rst_ni) begin
      if(!rst_ni) begin
        l1i_this_cycles_q <= 1;
        l1d_this_cycles_q <= 1;
        l2_this_cycles_q  <= 1;
      end else begin
          if(l1i2perf_i.inst_finish && l1i_req_en) begin
              l1i_this_cycles_q <= 1;
          end else if(!l22perf_i.in_fence_en && (l1i2perf_i.read_req || l1i2perf_i.write_req)) begin
              l1i_this_cycles_q <= l1i_this_cycles_q + 1;
          end
          
          if(l1d2perf_i.inst_finish && l1d_req_en) begin
              l1d_this_cycles_q <= 1;
          end else if(!l22perf_i.in_fence_en && (l1d2perf_i.read_req || l1d2perf_i.write_req)) begin
              l1d_this_cycles_q <= l1d_this_cycles_q + 1;
          end

          if(l22perf_i.inst_finish && l2_req_en) begin
              l2_this_cycles_q <= 1;
          end else if(!l22perf_i.in_fence_en && (l22perf_i.read_req || l22perf_i.write_req)) begin
              l2_this_cycles_q <= l2_this_cycles_q + 1;
          end
      end 
    end

    always_ff @(posedge clk_i or negedge rst_ni) begin
      if(!rst_ni) begin
        l1i2perf_q <= '0;
        l1d2perf_q <= '0;
        l22perf_q  <= '0;
      end else begin
        l1i2perf_q <= l1i2perf_i;
        l1d2perf_q <= l1d2perf_i;
        l22perf_q  <= l22perf_i;
      end
    end

endmodule