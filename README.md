# APD
* It is a 64-bit dual-issue out-of-order RISC-V processor, develop with systemverilog. It has 2 level PAg branch predictor, 2-level cache system, mmu, csr, precise exception support, reorder buffer...
* Some of the units, like decoder, mmu, csr, divider are referred to the open-source project `cva6`
* On the `Master` banch is a single-issue, in-order processor, on the `single-issue-out-of-order` branch is a single issue out-of-order backend processor, on the `order-of-order`  branch is a dual-issue out-of-order backend processor.
# Build
You can build it by `make verilator` or `make vcs` in `tb` folder, 
# Run
You can check the Makefile in `tb` folder to check out the isa test, random instruction test, and other benchmarks
# Performance
## Dhrystone
    Microseconds for one run through Dhrystone: 403
    Dhrystones per Second:                      2480
    Dhrystones           :                      500
    mcycle = 201581
    minstret = 196029
## Coremark
    2K performance run parameters for coremark.
    CoreMark Size    : 666
    Total ticks      : 7019307
    Total time (secs): 7
    Iterations/Sec   : 2
    Iterations       : 20
    Compiler version : GCC9.2.0
    Compiler flags   : -O2 -DPREALLOCATE=1 -mcmodel=medany -static -std=gnu99 -ffast-math -fno-common -fno-builtin-printf -DPERFORMANCE_RUN=1  -static -nostdlib -nostartfiles -lm -lgcc -Tsimple/test.ld
    Memory location  : STACK
    seedcrc          : 0xe9f5
    [0]crclist       : 0xe714
    [0]crcmatrix     : 0x1Wd7
    [0]crcstate      : 0x8W3W
    [0]crcfinal      : 0x4983
## Embench-iot
| Case   | instr count | cycle count | IPC
| ----   | ----        | ----        | ----
aha-mont64 | 1921325   | 1800957     | 1.066836
crc32      | 4208550   | 4530455     | 0.928946
edn        | 3607138   | 4123324     | 0.874813
huffbench  | 2770587   | 2738136     | 1.011851
matmult-int| 4218803   | 3288457     | 0.779476
nettle-aes | 5368337   | 3178037     | 1.689199
nettle-sha256 | 4095101| 2375880     | 1.723614
nsichneu   | 2552992   | 2869209     | 0.889789
picojpeg   | 4368532   | 3114160     | 1.402796
sglib-combined| 2648206| 3053468     | 0.867278
