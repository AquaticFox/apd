package apd_pkg;
    import riscv_pkg::*;
    // -------------------
    // Global Configure
    // -------------------
`ifdef LAB_TEST
    parameter BOOT_ADDR     = 'h80001000;
    parameter TOHOST_ADDR   = 'h80000000;
`endif

`ifdef LAB_TEST2
    parameter BOOT_ADDR     = 'h100c2;
    parameter TOHOST_ADDR   = 'h80001000;
`else
    parameter BOOT_ADDR     = 'h80000000;
    parameter TOHOST_ADDR   = 'h80001000;
`endif


    typedef struct packed {
      // BPs
      int               RASDepth;
      int               BTBEntries;
      int               BHTEntries;
      // I$
      int               ICacheWidth;
      int               ICacheLineNum;
      int               ICacheWayNum;
      // D$
      int               DCacheWidth;
    } apd_cfg_t;

    localparam apd_cfg_t ApdDefaultConfig = '{
      // BPs
      RASDepth: 2,
      BTBEntries: 64,
      BHTEntries: 128,
      // I$
      ICacheWidth: 512,
      ICacheLineNum: 1024,
      ICacheWayNum: 1,
      // D$
      DCacheWidth: 512
    };

    localparam ISSUE_NUM = 2;
    localparam BACK_END_ISSUE_NUM = 1;
    
    localparam riscv_pkg::xlen_t APD_MARCHID = {{riscv_pkg::XLEN-32{1'b0}}, 32'd3};
    localparam bit FP_PRESENT = 0;
    localparam riscv_pkg::xlen_t ISA_CODE = (0  <<  0)  // A - Atomic Instructions extension
                                     | (1   <<  2)  // C - Compressed extension
                                     | (0   <<  3)  // D - Double precsision floating-point extension
                                     | (FP_PRESENT  <<  5)  // F - Single precsision floating-point extension
                                     | (1   <<  8)  // I - RV32I/64I/128I base ISA
                                     | (1   << 12)  // M - Integer Multiply/Divide extension
                                     | (0   << 13)  // N - User level interrupts supported
                                     | (1   << 18)  // S - Supervisor mode implemented
                                     | (1   << 20)  // U - User mode implemented
                                     | (0   << 23)  // X - Non-standard extensions present
                                     | ((riscv_pkg::XLEN == 64 ? 2 : 1) << riscv_pkg::XLEN-2);  // MXL
                                     
    localparam bit ENABLE_CYCLE_COUNT = 1'b1;
    localparam bit ENABLE_WFI = 1'b0;
    localparam bit ZERO_TVAL = 1'b0;
    localparam ASID_WIDTH    = (riscv_pkg::XLEN == 64) ? 16 : 1;
    
    // read mask for SSTATUS over MMSTATUS
    localparam logic [63:0] SMODE_STATUS_READ_MASK = riscv_pkg::SSTATUS_UIE
                                                   | riscv_pkg::SSTATUS_SIE
                                                   | riscv_pkg::SSTATUS_SPIE
                                                   | riscv_pkg::SSTATUS_SPP
                                                   | riscv_pkg::SSTATUS_FS
                                                   | riscv_pkg::SSTATUS_XS
                                                   | riscv_pkg::SSTATUS_SUM
                                                   | riscv_pkg::SSTATUS_MXR
                                                   | riscv_pkg::SSTATUS_UPIE
                                                   | riscv_pkg::SSTATUS_SPIE
                                                   | riscv_pkg::SSTATUS_UXL
                                                   | riscv_pkg::SSTATUS_SD;

    localparam logic [63:0] SMODE_STATUS_WRITE_MASK = riscv_pkg::SSTATUS_SIE
                                                    | riscv_pkg::SSTATUS_SPIE
                                                    | riscv_pkg::SSTATUS_SPP
                                                    | riscv_pkg::SSTATUS_FS
                                                    | riscv_pkg::SSTATUS_SUM
                                                    | riscv_pkg::SSTATUS_MXR;
    
    // -------------------
    // I$ parameter
    // -------------------
    localparam ICACHE_LINE_WIDTH    = ApdDefaultConfig.ICacheWidth;
    localparam BYTE_PER_LINE        = ICACHE_LINE_WIDTH / 8;
    localparam LOG_BYTE_PER_LINE    = $clog2(BYTE_PER_LINE);

    // -------------------
    // D$ parameter
    // -------------------
    localparam DCACHE_LINE_WIDTH    = ApdDefaultConfig.DCacheWidth;

    // -------------------
    // Fetch stage
    // -------------------
    typedef struct packed {
         riscv_pkg::xlen_t       cause; // cause of exception
         riscv_pkg::xlen_t       tval;  // additional information of causing exception (e.g.: instruction causing it),
                             // address of LD/ST fault
         logic        valid;
    } exception_t;
    // ib
    localparam IB_FIFO_DEPTH    = 64;
    localparam FETCH_WIDTH      = ICACHE_LINE_WIDTH;
    localparam ENTRIESPERLINE = FETCH_WIDTH/16;
    
    localparam DEPTHMSB         = $clog2(IB_FIFO_DEPTH)-1;
    // max insts can fetch on 1 request (compressed instructions 16 bits)
    localparam INSTR_PER_FETCH  = FETCH_WIDTH / 16;
    typedef logic [DEPTHMSB:0] rd_ptr_t; 
    typedef logic [$clog2(ENTRIESPERLINE)-1:0] idx_per_ic_line_t;
    typedef struct packed {
        logic [15:0]                entry;
        logic [riscv_pkg::VLEN-1:0] addr;             // update at PC
        exception_t                 exception;
    } ib_entry_t;

    function automatic logic is_inst_rvc(logic [1:0] inst_msb);
        return (inst_msb != 2'b11);
    endfunction

    // btb
    localparam BTB_OFFSET_LEN    = $clog2(INSTR_PER_FETCH);
    localparam BTB_INDEX_LEN     = $clog2(ApdDefaultConfig.BTBEntries / INSTR_PER_FETCH);
    localparam BTB_TAG_LEN       = riscv_pkg::VLEN - BTB_OFFSET_LEN - BTB_INDEX_LEN;

    typedef struct packed {
        logic                       valid;
        logic [riscv_pkg::VLEN-1:0] pc;             // update at PC
        logic [riscv_pkg::VLEN-1:0] target_address;
    } btb_update_t;

    typedef struct packed {
        logic                       valid;
        logic [BTB_TAG_LEN - 1:0]   tag;
        logic [riscv_pkg::VLEN-1:0] target_address;
    } btb_entry_t;

    typedef struct packed {
        logic                       valid;
        logic [riscv_pkg::VLEN-1:0] target_address;
    } btb_output_t;


    // -------------------
    // Icache to frontend
    // -------------------
    // I$ data requests
    typedef struct packed {
        logic                       req;                    // request a new line
        logic [riscv_pkg::PLEN-1:0] paddr;                  
    } icache_req_f2ic_t;

    typedef struct packed {
        logic                       ready;                  // icache is ready
        logic                       valid;                  // the data is valid
        logic [FETCH_WIDTH-1:0]     data;                   
        logic [riscv_pkg::PLEN-1:0] paddr;                  // physical address out
    } icache_req_ic2f_t;
    
    // ------------------
    // Icache to mmu
    // ------------------
    typedef struct packed {
        logic                     fetch_valid;     // address translation valid
        logic [riscv_pkg::PLEN-1:0]   fetch_paddr;     // physical address in
        exception_t               fetch_exception; // exception occurred during fetch
    } icache_req_mmu2f_t;

    typedef struct packed {
        logic                     fetch_req;       // address translation request
        logic [riscv_pkg::VLEN-1:0]   fetch_vaddr;     // virtual address out
    } icache_req_f2mmu_t;
    
    // ------------------
    // LSU to mmu
    // ------------------
    typedef struct packed {
        // Cycle 0
        logic                       lsu_dtlb_hit;
        logic [riscv_pkg::PPNW-1:0] lsu_dtlb_ppn; // ppn (send same cycle as hit)
        // Cycle 1
        logic                       lsu_valid;     // address translation valid
        logic [riscv_pkg::PLEN-1:0] lsu_paddr;     // physical address in
        exception_t                 lsu_exception; // exception occurred during fetch
    } lsu_req_mmu2lsu_t;

    typedef struct packed {
        exception_t               misaligned_ex;
        logic                     lsu_req;       // address translation request
        logic [riscv_pkg::VLEN-1:0]   lsu_vaddr;     // virtual address out
        logic                     lsu_is_store;  // the translation is requested by a store, for pmp check
    } lsu_req_lsu2mmu_t;

    // -------------------
    // Dcache to lsu
    // -------------------
    typedef struct packed {
        logic                       rw_req;                 // 0 read, 1 write
        logic                       valid;
        logic [riscv_pkg::PLEN-1:0] paddr;
        logic [riscv_pkg::XLEN-1:0] data;                   // data to store    
        logic [riscv_pkg::XLEN/8-1:0] byte_mask;            // byte mask for data to store                
        logic                       fence;
    } dcache_req_lsu2dc_t;

    typedef struct packed {
        logic                       ready;                  // dcache is ready
        logic                       valid;                  // the loaded data is valid
        logic [DCACHE_LINE_WIDTH-1:0] data;                   
        logic [riscv_pkg::PLEN-1:0] paddr;                  // physical address out
    } dcache_req_dc2lsu_t;
    
    // ------------------
    // Cache to memory
    // ------------------
    typedef logic [DCACHE_LINE_WIDTH-1:0]   cc_data_t;
    typedef logic [BYTE_PER_LINE-1:0]       cc_byte_mask_t;
    typedef struct packed {
        logic                       rw_req;                 // 0 read, 1 write
        logic                       valid;
        logic [riscv_pkg::PLEN-1:0] paddr;
        cc_data_t                   data;                   // line to store    
        cc_byte_mask_t              byte_mask;            // byte mask for data to store                
        logic                       uncached_en;        // TODO
        logic                       l1_fence_en;
        logic                       l2_fence_en;
        logic                       dc_ic;   // 0: dc, 1: ic
    } dcache_req_cc2mem_t;

    typedef struct packed {
        logic                       ready;                  // dcache is ready
        logic                       valid;                  // the loaded data is valid
        cc_data_t                   data;                   
        logic [riscv_pkg::PLEN-1:0] paddr;                  // physical address out
        logic                       dc_ic;   // 0: dc, 1: ic
    } dcache_req_mem2cc_t;
    
    // -------------------
    // MMU to memory
    // -------------------
    typedef struct packed {
        logic [riscv_pkg::PLEN-1:0]    paddr; // TODO, change it to physically addressed
        riscv_pkg::xlen_t              data_wdata;
        logic                          data_req;
        logic                          data_we; // 0:read, 1:write
        logic [7:0]                    data_be; // byte enable
    } mmu_req_mmu2mem_t;

    typedef struct packed {
        logic                          data_gnt;
        logic                          data_rvalid;
        logic [DCACHE_LINE_WIDTH-1:0]  data_rdata;
    } mmu_req_mem2mmu_t;
    
    // -------------------
    // Frontend to ID
    // -------------------
    typedef struct packed {
        logic [riscv_pkg::VLEN-1:0] addr;        // the address of the instructions from below
        logic [31:0]                inst;    // instruction word
        logic                       valid;
        logic                       is_rvc;
        exception_t                 ex;
    } inst_per_issue_t;
    
    typedef struct packed {
        inst_per_issue_t [ISSUE_NUM-1:0] issue_inst;
    } inst_f2d_t;
    

    // ---------------------
    // ID to ISSUE
    // ---------------------
    
    typedef enum logic[3:0] {
        NONE,      // 0
        LOAD,      // 1
        STORE,     // 2
        ALU,       // 3
        CTRL_FLOW, // 4
        MULT,      // 5
        CSR        // 6
    } fu_t;
    localparam SupervisorIrq = 1;
    localparam MachineIrq = 0;
    typedef enum logic [6:0] { // basic ALU op
                               ADD, SUB, ADDW, SUBW,
                               // logic operations
                               XORL, ORL, ANDL,
                               // shifts
                               SRA, SRL, SLL, SRLW, SLLW, SRAW,
                               // comparisons
                               LTS, LTU, GES, GEU, EQ, NE,
                               // jumps
                               JALR, BRANCH,
                               // set lower than operations
                               SLTS, SLTU,
                               // CSR functions
                               MRET, SRET, DRET, ECALL, WFI, FENCE, FENCE_I, SFENCE_VMA, CSR_WRITE, CSR_READ, CSR_SET, CSR_CLEAR,
                               // LSU functions
                               LD, SD, LW, LWU, SW, LH, LHU, SH, LB, SB, LBU,
                               // Multiplications
                               MUL, MULH, MULHU, MULHSU, MULW,
                               // Divisions
                               DIV, DIVU, DIVW, DIVUW, REM, REMU, REMW, REMUW
                             } fu_op;
    typedef enum logic [2:0] {
      NoCF,   // No control flow prediction
      Branch, // Branch
      Jump,   // Jump to address from immediate
      JumpR,  // Jump to address from registers
      Return  // Return Address Prediction
    } cf_t;    
    typedef struct packed {
        cf_t                    cf;              // type of control flow prediction
        logic [riscv_pkg::VLEN-1:0] predict_address; // target address at which to jump, or not
    } branchpredict_sbe_t; // TODO
    
    
    
    localparam NR_SB_ENTRIES = 8; // number of scoreboard entries
    localparam TRANS_ID_BITS = $clog2(NR_SB_ENTRIES); // depending on the number of scoreboard entries we need that many bits                      
    localparam REG_ADDR_SIZE = 6;
    
    typedef struct packed {
        logic [riscv_pkg::VLEN-1:0]   pc;            // PC of instruction
        logic [TRANS_ID_BITS-1:0] trans_id;      // this can potentially be simplified, we could index the scoreboard entry
                                                 // with the transaction id in any case make the width more generic
        fu_t                      fu;            // functional unit to use
        fu_op                     op;            // operation to perform in each functional unit
        logic [REG_ADDR_SIZE-1:0] rs1;           // register source address 1
        logic [REG_ADDR_SIZE-1:0] rs2;           // register source address 2
        logic [REG_ADDR_SIZE-1:0] rd;            // register destination address
        riscv_pkg::xlen_t         result;        // for unfinished instructions this field also holds the immediate,
        logic                     valid;         // is the result valid (execution finished)
        logic                     use_imm;       // should we use the immediate as operand b?
        logic                     use_zimm;      // use zimm as operand a
        logic                     use_pc;        // set if we need to use the PC as operand a, PC from exception
        exception_t               ex;            // exception has occurred
        branchpredict_sbe_t       bp;            // branch predict scoreboard data structure
        logic                     is_compressed; // signals a compressed instructions, we need this information at the commit stage if
                                                 // we want jump accordingly e.g.: +4, +2
    } scoreboard_entry_t;
    
    typedef struct packed {
        logic                           valid;
        scoreboard_entry_t     sbe;
        logic                           is_ctrl_flow;
    } id_per_issue_t;
    typedef struct packed {
        id_per_issue_t [ISSUE_NUM-1:0]  issue_inst;
    } inst_id2is_t;
    // -----------------
    // TODO in decoder
    // -----------------

    
    typedef struct packed {
      riscv_pkg::xlen_t       mie;
      riscv_pkg::xlen_t       mip;
      riscv_pkg::xlen_t       mideleg;
      logic        sie;
      logic        global_enable;
    } irq_ctrl_t;

    // ------------
    // ISSUE Stage
    // ------------
    typedef struct packed {
        logic [riscv_pkg::VLEN-1:0]   pc;
        fu_t                      fu;            // functional unit to use
//        fu_op                     op;            // operation to perform in each functional unit
        logic [REG_ADDR_SIZE-1:0] rs1;           // register source address 1
        logic [REG_ADDR_SIZE-1:0] rs2;           // register source address 2
//        riscv_pkg::xlen_t         imm;
        logic [REG_ADDR_SIZE-1:0] rd;            // register destination address
        logic                     rs1_en;       // operation_a is a register
        logic                     rs2_en;
        logic                     rd_en;
    } issue_entry_t;
    // ------------
    // EX Stage
    // ------------
    function automatic riscv_pkg::xlen_t sext32 (logic [31:0] operand);
        return {{riscv_pkg::XLEN-32{operand[31]}}, operand[31:0]};
    endfunction
    // alu
    typedef struct packed {
        logic               alu_valid;
        logic               lsu_valid;
        logic               mult_valid;
        logic               csr_valid;
        logic               branch_valid;
    } fu_valid_t;
    
    typedef struct packed {
        // fu
        fu_t                      fu;
        fu_valid_t               fu_valid;
        exception_t               ex;            // exception has occurred
        logic [riscv_pkg::VLEN-1:0]   pc;
        fu_op                     operator;
        riscv_pkg::xlen_t             operand_a;
        riscv_pkg::xlen_t             operand_b;
        riscv_pkg::xlen_t             imm;
        logic                    is_rvc;
        branchpredict_sbe_t      bp;
        
        logic [REG_ADDR_SIZE-1:0] rd;            // register destination address
        logic                     rd_en;
    } fu_data_t;
    
    // branch unit
    typedef struct packed {
        logic                   valid;           // prediction with all its values is valid
        logic [riscv_pkg::VLEN-1:0] pc;              // PC of predict or mis-predict
        logic [riscv_pkg::VLEN-1:0] target_address;  // target address at which to jump, or not
        logic                   is_mispredict;   // set if this was a mis-predict
        logic                   is_taken;        // branch is taken
        cf_t                    cf_type;         // Type of control flow change
    } bp_resolve_t;
    
    function automatic logic op_is_branch (input fu_op op);
        unique case (op) inside
            EQ, NE, LTS, GES, LTU, GEU: return 1'b1;
            default                   : return 1'b0; // all other ops
        endcase
    endfunction
    
    // load store unit
    typedef enum logic[1:0] {
      L8  = 2'b00,
      L16 = 2'b01,
      L32 = 2'b10,
      L64 = 2'b11
    } ls_length_t;
    
    typedef struct packed {
        logic                   rw_en;   // 0 read, 1 write
        logic                   req_valid;    // mem req enable
        ls_length_t             req_length;   // L8, L16, L32, L64
        logic                   load_signed;   //0: unsigned ext,1: sext
        logic [riscv_pkg::VLEN-1:0]  dm_addr;   // data mem w/r address
        riscv_pkg::xlen_t       data;    // data to mem
        logic                   fence;
    } lsu_data_t;

    // ------------
    // EX2COMMIT
    // ------------
    typedef struct packed {
      riscv_pkg::xlen_t         wb_data;
      logic                     wb_valid;
      logic[REG_ADDR_SIZE-1:0]  wb_rd;
      logic                     wb_rd_en;
      logic[REG_ADDR_SIZE-1:0]  ex_rd_q;
      logic                     ex_rd_en_q;
      logic [riscv_pkg::VLEN-1:0] pc;
    } ex2wb_t;
    
    // ----------------
    // Cache subsystem
    // ----------------
    // D$ data requests
    typedef struct packed {
        logic [riscv_pkg::PLEN-1:0]    address; // TODO, change it to physically addressed
        riscv_pkg::xlen_t              data_wdata;
        logic                          req_valid;
        logic                          data_we; // 0:read, 1:write
        logic [7:0]                    data_be; // byte enable
    } dcache_req_i_t;

    typedef struct packed {
        logic                          data_wgrant;
        logic                          data_rvalid;
        logic [DCACHE_LINE_WIDTH-1:0]                   data_rdata;
        logic [riscv_pkg::VLEN-1:0]    vaddr; 
    } dcache_req_o_t;


    // ----------
    // MMU
    // ----------
    typedef struct packed {
        logic                  valid;      // valid flag
        logic                  is_2M;      //
        logic                  is_1G;      //
        logic [26:0]           vpn;
        logic [ASID_WIDTH-1:0] asid;
        riscv_pkg::pte_t           content;
    } tlb_update_t;


    // ------------------------
    // performance counter
    // ------------------------

    typedef struct packed {
        logic                         read_req;
        logic                         write_req;
        logic                         read_miss;
        logic                         write_miss;

        logic                         inst_finish;

        logic                         rm_en;
        logic                         wb_en;
        
        logic                         in_fence_en;
    } cache_perf_counter_t;    
    
    function automatic logic cache_rw_req_counter_update 
    (
        input  cache_perf_counter_t perf_i,
        input  cache_perf_counter_t perf_q_i,
        input  logic [XLEN-1:0]  read_req_q_i,
        input  logic [XLEN-1:0]  write_req_q_i,
        input  logic [XLEN-1:0]  read_miss_q_i,
        input  logic [XLEN-1:0]  write_miss_q_i,
        input  logic [XLEN-1:0]  hit_cycles_q_i,
        input  logic [XLEN-1:0]  miss_cycles_q_i,
        input  logic [XLEN-1:0]  this_cycles_q_i,

        output logic [XLEN-1:0]  read_req_d_o,
        output logic [XLEN-1:0]  write_req_d_o,
        output logic [XLEN-1:0]  read_miss_d_o,
        output logic [XLEN-1:0]  write_miss_d_o,
        output logic [XLEN-1:0]  hit_cycles_d_o,
        output logic [XLEN-1:0]  miss_cycles_d_o
    );
        read_req_d_o    = read_req_q_i;
        write_req_d_o   = write_req_q_i;
        read_miss_d_o   = read_miss_q_i;
        write_miss_d_o  = write_miss_q_i;
        hit_cycles_d_o  = hit_cycles_q_i;
        miss_cycles_d_o = miss_cycles_q_i;

        if(perf_i.inst_finish && !perf_i.in_fence_en) begin
            if(perf_i.read_req) begin
                read_req_d_o  = read_req_q_i  + 1;
                if(perf_q_i.rm_en && perf_q_i.read_miss) begin
                    read_miss_d_o = read_miss_q_i + 1;
                end
                
                if(perf_q_i.rm_en) begin
                    miss_cycles_d_o = miss_cycles_q_i + this_cycles_q_i;
                end else begin
                    hit_cycles_d_o  = hit_cycles_q_i + this_cycles_q_i;
                end

            end else if(perf_i.write_req && !perf_i.in_fence_en) begin
                write_req_d_o = write_req_q_i + 1;
                if(perf_q_i.rm_en && perf_q_i.write_miss) begin
                    write_miss_d_o = write_miss_q_i + 1;
                end

                if(perf_q_i.rm_en) begin
                    miss_cycles_d_o = miss_cycles_q_i + this_cycles_q_i;
                end else begin
                    hit_cycles_d_o  = hit_cycles_q_i + this_cycles_q_i;
                end
            end
        end
    endfunction


endpackage // apd_pkg