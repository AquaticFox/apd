// Copyright 2021 RISC-V International Open Source Laboratory (RIOS Lab). All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

`ifndef VERILATOR
    `timescale   1ns/1ps
`endif
module testbench
(
`ifndef VCS
    input  logic             clk,
    input  logic             rstn,
    input  string            image_f,
    output logic[39-1:0]     if_pc,
//
//    output logic             rd_en,
//    output logic             wr_en,
    output logic [39-1:0]    debug_o_wb_pc,
    output logic [31:0]      exit_verilator
`endif

`ifdef SPIKE_DEBUG
    ,
    output logic [32-1:0][64-1:0]   debug_o_wb_gpr,
    output logic [9-1:0][64-1:0]    debug_o_wb_csr,
    output logic [1:0]              priv_lvl_ver
`endif

`ifndef VCS
    `ifdef LAB_TEST2
    ,
    output logic [32-1:0][64-1:0]   debug_o_wb_gpr,
    output logic [9-1:0][64-1:0]    debug_o_wb_csr,
    output logic [1:0]              priv_lvl_ver
    `endif
`endif
);
    // logic [9-1:0][64-1:0]    debug_o_wb_csr;
    // assign debug_o_wb_csr[0]      = top_u.dut.csr_regfile_u.ISA_CODE;                           // misa
    // assign debug_o_wb_csr[1]      = top_u.dut.csr_regfile_u.hart_id_i;  // mhartid
    // assign debug_o_wb_csr[2]      = top_u.dut.csr_regfile_u.mstatus_q;  // mstatus
    // assign debug_o_wb_csr[3]      = top_u.dut.csr_regfile_u.mtvec_q;    // mtvec
    // assign debug_o_wb_csr[4]      = top_u.dut.csr_regfile_u.mscratch_q; // mscratch
    // assign debug_o_wb_csr[5]      = top_u.dut.csr_regfile_u.mepc_q;     // mepc
    // assign debug_o_wb_csr[6]      = top_u.dut.csr_regfile_u.mcause_q;   // mcause
    // assign debug_o_wb_csr[7]      = top_u.dut.csr_regfile_u.mtval_q;    // mtval
    // assign debug_o_wb_csr[8]      = top_u.dut.csr_regfile_u.cycle_q;    // mcycle
`ifdef SPIKE_DEBUG
    assign debug_o_wb_gpr[32-1:1] = top_u.dut.is_stage_u.issue_u.regfile_u.mem[32-1:1];
    assign debug_o_wb_gpr[0]      = '0;
    assign debug_o_wb_csr[0]      = ISA_CODE;                           // misa
    assign debug_o_wb_csr[1]      = top_u.dut.csr_regfile_u.hart_id_i;  // mhartid
    assign debug_o_wb_csr[2]      = top_u.dut.csr_regfile_u.sstatus_q;  // mstatus
    assign debug_o_wb_csr[3]      = top_u.dut.csr_regfile_u.stvec_q;    // mtvec
    assign debug_o_wb_csr[4]      = top_u.dut.csr_regfile_u.sscratch_q; // mscratch
    assign debug_o_wb_csr[5]      = top_u.dut.csr_regfile_u.sepc_q;     // mepc
    assign debug_o_wb_csr[6]      = top_u.dut.csr_regfile_u.scause_q;   // mcause
    assign debug_o_wb_csr[7]      = top_u.dut.csr_regfile_u.stval_q;    // mtval
    assign debug_o_wb_csr[8]      = top_u.dut.csr_regfile_u.cycle_q;    // mcycle

    assign priv_lvl_ver           = top_u.dut.csr_regfile_u.priv_lvl_o;
`endif

`ifndef VCS
    `ifdef LAB_TEST2
    assign debug_o_wb_gpr[32-1:1] = top_u.dut.is_stage_u.issue_u.regfile_u.mem[32-1:1];
    assign debug_o_wb_gpr[0]      = '0;
    assign debug_o_wb_csr[0]      = ISA_CODE;                           // misa
    assign debug_o_wb_csr[1]      = top_u.dut.csr_regfile_u.hart_id_i;  // mhartid
    assign debug_o_wb_csr[2]      = top_u.dut.csr_regfile_u.sstatus_q;  // mstatus
    assign debug_o_wb_csr[3]      = top_u.dut.csr_regfile_u.stvec_q;    // mtvec
    assign debug_o_wb_csr[4]      = top_u.dut.csr_regfile_u.sscratch_q; // mscratch
    assign debug_o_wb_csr[5]      = top_u.dut.csr_regfile_u.sepc_q;     // mepc
    assign debug_o_wb_csr[6]      = top_u.dut.csr_regfile_u.scause_q;   // mcause
    assign debug_o_wb_csr[7]      = top_u.dut.csr_regfile_u.stval_q;    // mtval
    assign debug_o_wb_csr[8]      = top_u.dut.csr_regfile_u.cycle_q;    // mcycle

    assign priv_lvl_ver           = top_u.dut.csr_regfile_u.priv_lvl_o;
    `endif
`endif


`ifdef LAB_TEST2
    always @(*) begin
    `ifndef VCS
        exit_verilator = 0;
    `endif
        if(top_u.dut.ex_stage_u.csr_data.ex.valid 
            && top_u.dut.ex_stage_u.csr_data.ex.cause == 8) begin
            $display("receive ecall\n");
            $display("\nRun cycle = %d cycles", top_u.dut.csr_regfile_u.cycle_q);
            $display("Total load store Latency = %d cycles", 
                                top_u.dut.performance_counter_u.l1i_hit_cycles_q 
                            +   top_u.dut.performance_counter_u.l1i_miss_cycles_q
                            +   top_u.dut.performance_counter_u.l1d_hit_cycles_q
                            +   top_u.dut.performance_counter_u.l1d_miss_cycles_q
                        );
            $display("Cache Hit rate = %f%s", 
                        (1.0 - ( 
                        //   top_u.dut.performance_counter_u.l1i_read_miss_q
                        // + top_u.dut.performance_counter_u.l1i_write_miss_q
                        // + top_u.dut.performance_counter_u.l1d_read_miss_q
                        // + top_u.dut.performance_counter_u.l1d_write_miss_q
                        top_u.dut.performance_counter_u.l2_read_miss_q * 1.0
                        + top_u.dut.performance_counter_u.l2_write_miss_q * 1.0
                        )
                        /
                        (
                        top_u.dut.performance_counter_u.l1i_read_req_q * 1.0
                        + top_u.dut.performance_counter_u.l1i_write_req_q * 1.0
                        + top_u.dut.performance_counter_u.l1d_read_req_q * 1.0
                        + top_u.dut.performance_counter_u.l1d_write_req_q * 1.0
                        //   top_u.dut.performance_counter_u.l2_read_req_q
                        // + top_u.dut.performance_counter_u.l2_write_req_q
                        )) * 100.0,
                        "%"
                    );
            $display("Cache Miss rate = %f%s", 
                (( 
                //   top_u.dut.performance_counter_u.l1i_read_miss_q
                // + top_u.dut.performance_counter_u.l1i_write_miss_q
                // + top_u.dut.performance_counter_u.l1d_read_miss_q
                // + top_u.dut.performance_counter_u.l1d_write_miss_q
                    top_u.dut.performance_counter_u.l2_read_miss_q * 1.0
                + top_u.dut.performance_counter_u.l2_write_miss_q *1.0
                )
                /
                (
                    top_u.dut.performance_counter_u.l1i_read_req_q * 1.0
                + top_u.dut.performance_counter_u.l1i_write_req_q * 1.0
                + top_u.dut.performance_counter_u.l1d_read_req_q * 1.0
                + top_u.dut.performance_counter_u.l1d_write_req_q * 1.0
                //   top_u.dut.performance_counter_u.l2_read_req_q
                // + top_u.dut.performance_counter_u.l2_write_req_q
                )) * 100.0,
                "%"
            );
            $display("Cache Hit Latency = %f cycles", 
                ( 
                    top_u.dut.performance_counter_u.l1i_hit_cycles_q * 1.0
                +   top_u.dut.performance_counter_u.l1d_hit_cycles_q * 1.0
                )
                /
                (
                    top_u.dut.performance_counter_u.l1i_read_req_q * 1.0
                + top_u.dut.performance_counter_u.l1i_write_req_q * 1.0
                + top_u.dut.performance_counter_u.l1d_read_req_q * 1.0
                + top_u.dut.performance_counter_u.l1d_write_req_q * 1.0
                - top_u.dut.performance_counter_u.l2_read_req_q * 1.0
                //   top_u.dut.performance_counter_u.l2_read_req_q
                // + top_u.dut.performance_counter_u.l2_write_req_q
                )
            );
            $display("Cache Miss Latency = %f cycles", 
                ( 
                    top_u.dut.performance_counter_u.l1i_miss_cycles_q * 1.0
                +   top_u.dut.performance_counter_u.l1d_miss_cycles_q * 1.0
                )
                /
                (
                //     top_u.dut.performance_counter_u.l1i_read_req_q * 1.0
                // + top_u.dut.performance_counter_u.l1i_write_req_q * 1.0
                // + top_u.dut.performance_counter_u.l1d_read_req_q * 1.0
                // + top_u.dut.performance_counter_u.l1d_write_req_q * 1.0
                top_u.dut.performance_counter_u.l2_read_req_q * 1.0
                // + top_u.dut.performance_counter_u.l2_write_req_q * 1.0
                )
            );
            $display("DCache Hit rate = %f%s", 
                        (1.0 - ( 
                        //   top_u.dut.performance_counter_u.l1i_read_miss_q
                        // + top_u.dut.performance_counter_u.l1i_write_miss_q
                          top_u.dut.performance_counter_u.l1d_read_miss_q * 1.0
                        + top_u.dut.performance_counter_u.l1d_write_miss_q * 1.0
                        // top_u.dut.performance_counter_u.l2_read_miss_q * 1.0
                        // + top_u.dut.performance_counter_u.l2_write_miss_q * 1.0
                        )
                        /
                        (
                        // top_u.dut.performance_counter_u.l1i_read_req_q * 1.0
                        // + top_u.dut.performance_counter_u.l1i_write_req_q * 1.0
                          top_u.dut.performance_counter_u.l1d_read_req_q * 1.0
                        + top_u.dut.performance_counter_u.l1d_write_req_q * 1.0
                        //   top_u.dut.performance_counter_u.l2_read_req_q
                        // + top_u.dut.performance_counter_u.l2_write_req_q
                        )) * 100.0,
                        "%"
                    );
            $display("DCache Miss rate = %f%s", 
                (( 
                //   top_u.dut.performance_counter_u.l1i_read_miss_q
                // + top_u.dut.performance_counter_u.l1i_write_miss_q
                  top_u.dut.performance_counter_u.l1d_read_miss_q * 1.0
                + top_u.dut.performance_counter_u.l1d_write_miss_q * 1.0
                //     top_u.dut.performance_counter_u.l2_read_miss_q * 1.0
                // + top_u.dut.performance_counter_u.l2_write_miss_q *1.0
                )
                /
                (
                //     top_u.dut.performance_counter_u.l1i_read_req_q * 1.0
                // + top_u.dut.performance_counter_u.l1i_write_req_q * 1.0
                  top_u.dut.performance_counter_u.l1d_read_req_q * 1.0
                + top_u.dut.performance_counter_u.l1d_write_req_q * 1.0
                //   top_u.dut.performance_counter_u.l2_read_req_q
                // + top_u.dut.performance_counter_u.l2_write_req_q
                )) * 100.0,
                "%"
            );
            $display("DCache Hit Latency = %f cycles", 
                ( 
                    // top_u.dut.performance_counter_u.l1i_hit_cycles_q * 1.0
                    top_u.dut.performance_counter_u.l1d_hit_cycles_q * 1.0
                )
                /
                (
                //     top_u.dut.performance_counter_u.l1i_read_req_q * 1.0
                // + top_u.dut.performance_counter_u.l1i_write_req_q * 1.0
                  top_u.dut.performance_counter_u.l1d_read_req_q * 1.0
                + top_u.dut.performance_counter_u.l1d_write_req_q * 1.0
                - top_u.dut.performance_counter_u.l1d_read_miss_q * 1.0
                - top_u.dut.performance_counter_u.l1d_write_miss_q * 1.0
                //   top_u.dut.performance_counter_u.l2_read_req_q
                // + top_u.dut.performance_counter_u.l2_write_req_q
                )
            );
            $display("DCache Miss Latency = %f cycles", 
                ( 
                    // top_u.dut.performance_counter_u.l1i_miss_cycles_q * 1.0
                    top_u.dut.performance_counter_u.l1d_miss_cycles_q * 1.0
                )
                /
                (
                    top_u.dut.performance_counter_u.l1d_read_miss_q * 1.0
                +   top_u.dut.performance_counter_u.l1d_write_miss_q * 1.0
                )
            );

            $display("\nl1i_read_req_q    = %d,", top_u.dut.performance_counter_u.l1i_read_req_q );
            $display("l1i_write_req_q   = %d,", top_u.dut.performance_counter_u.l1i_write_req_q );
            $display("l1d_read_req_q    = %d,", top_u.dut.performance_counter_u.l1d_read_req_q );
            $display("l1d_write_req_q   = %d,", top_u.dut.performance_counter_u.l1d_write_req_q );
            $display("l2_read_req_q     = %d,", top_u.dut.performance_counter_u.l2_read_req_q );
            $display("l2_write_req_q    = %d,\n", top_u.dut.performance_counter_u.l2_write_req_q );

            $display("l1i_read_miss_q   = %d,", top_u.dut.performance_counter_u.l1i_read_miss_q );
            $display("l1i_write_miss_q  = %d,", top_u.dut.performance_counter_u.l1i_write_miss_q );
            $display("l1d_read_miss_q   = %d,", top_u.dut.performance_counter_u.l1d_read_miss_q );
            $display("l1d_write_miss_q  = %d,", top_u.dut.performance_counter_u.l1d_write_miss_q );
            $display("l2_read_miss_q    = %d,", top_u.dut.performance_counter_u.l2_read_miss_q );
            $display("l2_write_miss_q   = %d,\n", top_u.dut.performance_counter_u.l2_write_miss_q );
    
            $display("l1i_hit_cycles_q  = %d,", top_u.dut.performance_counter_u.l1i_hit_cycles_q );
            $display("l1i_miss_cycles_q = %d,", top_u.dut.performance_counter_u.l1i_miss_cycles_q );
            $display("l1d_hit_cycles_q  = %d,", top_u.dut.performance_counter_u.l1d_hit_cycles_q );
            $display("l1d_miss_cycles_q = %d,", top_u.dut.performance_counter_u.l1d_miss_cycles_q );
            $display("l2_hit_cycles_q   = %d,", top_u.dut.performance_counter_u.l2_hit_cycles_q );
            $display("l2_miss_cycles_q  = %d\n", top_u.dut.performance_counter_u.l2_miss_cycles_q );
    `ifdef VCS
            $finish;        
    `else
            exit_verilator = 1;
    `endif
        end
    end
`endif


`ifdef VCS

logic clk;
logic rstn;

initial begin
    // $dumpfile("rrv64.vcd");
    // $dumpvars(0, top_u);
    $vcdpluson();
end

initial begin
    clk = 1'b0;
    // $dumpon;
    forever #5 clk = ~clk;
    // $dumpoff;
end

initial begin
    rstn = 1'b0;
    #20;
    rstn = 1'b1;
end

logic [5:0] cnt;

always_ff @(posedge clk or negedge rstn) begin
    if(!rstn)   cnt <= '0;
    else        cnt <= cnt + 1; 
end

//assign top_u.dut.flush = (&cnt == 1'b1) ? 1'b1 : 1'b0;
//assign top_u.dut.flush = 0;
`endif

`ifndef VCS
initial begin
    top_u.image_f = image_f;
  `ifdef LAB_TEST
        top_u.dut.rom_ideal_u.image_f = image_f;
  `endif
  `ifdef LAB_TEST2
        top_u.dut.rom_ideal_u.image_f = image_f;
  `endif
    $display("%t Get image_f %s", $time, image_f);
end

assign debug_o_wb_pc  = top_u.dut.is_stage_u.issue_u.wb_pc;
assign if_pc = top_u.dut.frontend_u.fetch_addr_q;
`ifndef LAB_TEST2
`ifndef VCS
assign exit_verilator = top_u.dut.rom_ideal_u.exit_verilator;
`endif
`endif
//assign rd_en = top_u.dut.rrv64_u.DC.l1_common_rd_en;
//assign wr_en = top_u.dut.rrv64_u.DC.l1_common_wt_en;
`endif


// logic a_80001624;
// assign a_80001624 = top_u.dut.is_stage_u.issue_u.wb_pc == 'h80001624;
// always_ff @(posedge clk) begin
//     if(a_80001624) $display("pc @ 80001624, time=%d", $time);
// end

`ifdef LAB_TEST2
// `ifdef LAB_CACHE
    always@(posedge clk) begin
        if(top_u.dut.csr_regfile_u.cycle_q[20-1:0] == 20'd1) begin // 1M cycles
            $display("\n\ncycle            = %d", top_u.dut.csr_regfile_u.cycle_q);
            $display("pc               = 0x%x", top_u.dut.is_stage_u.issue_u.wb_pc);
            $display("DCache Miss rate = %f%s", 
                (( 
                //   top_u.dut.performance_counter_u.l1i_read_miss_q
                // + top_u.dut.performance_counter_u.l1i_write_miss_q
                  top_u.dut.performance_counter_u.l1d_read_miss_q * 1.0
                + top_u.dut.performance_counter_u.l1d_write_miss_q * 1.0
                //     top_u.dut.performance_counter_u.l2_read_miss_q * 1.0
                // + top_u.dut.performance_counter_u.l2_write_miss_q *1.0
                )
                /
                (
                //     top_u.dut.performance_counter_u.l1i_read_req_q * 1.0
                // + top_u.dut.performance_counter_u.l1i_write_req_q * 1.0
                  top_u.dut.performance_counter_u.l1d_read_req_q * 1.0
                + top_u.dut.performance_counter_u.l1d_write_req_q * 1.0
                //   top_u.dut.performance_counter_u.l2_read_req_q
                // + top_u.dut.performance_counter_u.l2_write_req_q
                )) * 100.0,
                "%"
            );
            $display("DCache Hit Latency = %f cycles", 
                ( 
                    // top_u.dut.performance_counter_u.l1i_hit_cycles_q * 1.0
                    top_u.dut.performance_counter_u.l1d_hit_cycles_q * 1.0
                )
                /
                (
                //     top_u.dut.performance_counter_u.l1i_read_req_q * 1.0
                // + top_u.dut.performance_counter_u.l1i_write_req_q * 1.0
                  top_u.dut.performance_counter_u.l1d_read_req_q * 1.0
                + top_u.dut.performance_counter_u.l1d_write_req_q * 1.0
                - top_u.dut.performance_counter_u.l1d_read_miss_q * 1.0
                - top_u.dut.performance_counter_u.l1d_write_miss_q * 1.0
                //   top_u.dut.performance_counter_u.l2_read_req_q
                // + top_u.dut.performance_counter_u.l2_write_req_q
                )
            );
            $display("DCache Miss Latency = %f cycles", 
                ( 
                    // top_u.dut.performance_counter_u.l1i_miss_cycles_q * 1.0
                    top_u.dut.performance_counter_u.l1d_miss_cycles_q * 1.0
                )
                /
                (
                    top_u.dut.performance_counter_u.l1d_read_miss_q * 1.0
                +   top_u.dut.performance_counter_u.l1d_write_miss_q * 1.0
                )
            );
            $display("\nl1i_read_req_q    = %d,", top_u.dut.performance_counter_u.l1i_read_req_q );
            $display("l1i_write_req_q   = %d,", top_u.dut.performance_counter_u.l1i_write_req_q );
            $display("l1d_read_req_q    = %d,", top_u.dut.performance_counter_u.l1d_read_req_q );
            $display("l1d_write_req_q   = %d,", top_u.dut.performance_counter_u.l1d_write_req_q );
            $display("l2_read_req_q     = %d,", top_u.dut.performance_counter_u.l2_read_req_q );
            $display("l2_write_req_q    = %d,\n", top_u.dut.performance_counter_u.l2_write_req_q );

            $display("l1i_read_miss_q   = %d,", top_u.dut.performance_counter_u.l1i_read_miss_q );
            $display("l1i_write_miss_q  = %d,", top_u.dut.performance_counter_u.l1i_write_miss_q );
            $display("l1d_read_miss_q   = %d,", top_u.dut.performance_counter_u.l1d_read_miss_q );
            $display("l1d_write_miss_q  = %d,", top_u.dut.performance_counter_u.l1d_write_miss_q );
            $display("l2_read_miss_q    = %d,", top_u.dut.performance_counter_u.l2_read_miss_q );
            $display("l2_write_miss_q   = %d,\n", top_u.dut.performance_counter_u.l2_write_miss_q );
    
            $display("l1i_hit_cycles_q  = %d,", top_u.dut.performance_counter_u.l1i_hit_cycles_q );
            $display("l1i_miss_cycles_q = %d,", top_u.dut.performance_counter_u.l1i_miss_cycles_q );
            $display("l1d_hit_cycles_q  = %d,", top_u.dut.performance_counter_u.l1d_hit_cycles_q );
            $display("l1d_miss_cycles_q = %d,", top_u.dut.performance_counter_u.l1d_miss_cycles_q );
            $display("l2_hit_cycles_q   = %d,", top_u.dut.performance_counter_u.l2_hit_cycles_q );
            $display("l2_miss_cycles_q  = %d\n", top_u.dut.performance_counter_u.l2_miss_cycles_q );

            for(int i = 0; i < 32; i++) begin
                $display("reg[%d] = 0x%x", i, top_u.dut.is_stage_u.issue_u.regfile_u.mem[i]);
            end
            $display("----------------------------------------------------------------\n");
        end
    end
// `endif
`endif

top top_u
(
  .clk_i                   (clk),
  .rst_ni                  (rstn)
);

endmodule
