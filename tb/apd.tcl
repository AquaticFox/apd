# Begin_DVE_Session_Save_Info
# DVE full session
# Saved on Sat May 8 17:00:57 2021
# Designs open: 1
#   V1: vcdplus.vpd
# Toplevel windows open: 2
# 	TopLevel.1
# 	TopLevel.2
#   Source.1: testbench.top_u.dut
#   Wave.1: 245 signals
#   Group count = 17
#   Group global signal count = 3
#   Group id signal count = 15
#   Group id2is_fifo signal count = 11
#   Group is_pick signal count = 10
#   Group is_issue signal count = 8
#   Group ex_from_is signal count = 3
#   Group ex_fu_mul signal count = 7
#   Group ex_fu_div signal count = 9
#   Group ex_fu_alu signal count = 3
#   Group dcache signal count = 49
#   Group ex2wb signal count = 1
#   Group csr signal count = 7
#   Group control signal count = 4
#   Group regfile signal count = 1
#   Group printf signal count = 75
#   Group fence signal count = 35
#   Group mmu signal count = 4
# End_DVE_Session_Save_Info

# DVE version: Q-2020.03-SP2_Full64
# DVE build date: Sep  1 2020 20:47:52


#<Session mode="Full" path="/home/zfu/rios/apd_from_lab1/apd/tb/apd.tcl" type="Debug">

gui_set_loading_session_type Post
gui_continuetime_set

# Close design
if { [gui_sim_state -check active] } {
    gui_sim_terminate
}
gui_close_db -all
gui_expr_clear_all

# Close all windows
gui_close_window -type Console
gui_close_window -type Wave
gui_close_window -type Source
gui_close_window -type Schematic
gui_close_window -type Data
gui_close_window -type DriverLoad
gui_close_window -type List
gui_close_window -type Memory
gui_close_window -type HSPane
gui_close_window -type DLPane
gui_close_window -type Assertion
gui_close_window -type CovHier
gui_close_window -type CoverageTable
gui_close_window -type CoverageMap
gui_close_window -type CovDetail
gui_close_window -type Local
gui_close_window -type Stack
gui_close_window -type Watch
gui_close_window -type Group
gui_close_window -type Transaction



# Application preferences
gui_set_pref_value -key app_default_font -value {Helvetica,10,-1,5,50,0,0,0,0,0}
gui_src_preferences -tabstop 8 -maxbits 24 -windownumber 1
#<WindowLayout>

# DVE top-level session


# Create and position top-level window: TopLevel.1

if {![gui_exist_window -window TopLevel.1]} {
    set TopLevel.1 [ gui_create_window -type TopLevel \
       -icon $::env(DVE)/auxx/gui/images/toolbars/dvewin.xpm] 
} else { 
    set TopLevel.1 TopLevel.1
}
gui_show_window -window ${TopLevel.1} -show_state normal -rect {{89 67} {2376 1315}}

# ToolBar settings
gui_set_toolbar_attributes -toolbar {TimeOperations} -dock_state top
gui_set_toolbar_attributes -toolbar {TimeOperations} -offset 0
gui_show_toolbar -toolbar {TimeOperations}
gui_hide_toolbar -toolbar {&File}
gui_set_toolbar_attributes -toolbar {&Edit} -dock_state top
gui_set_toolbar_attributes -toolbar {&Edit} -offset 0
gui_show_toolbar -toolbar {&Edit}
gui_hide_toolbar -toolbar {CopyPaste}
gui_set_toolbar_attributes -toolbar {&Trace} -dock_state top
gui_set_toolbar_attributes -toolbar {&Trace} -offset 0
gui_show_toolbar -toolbar {&Trace}
gui_hide_toolbar -toolbar {TraceInstance}
gui_hide_toolbar -toolbar {BackTrace}
gui_set_toolbar_attributes -toolbar {&Scope} -dock_state top
gui_set_toolbar_attributes -toolbar {&Scope} -offset 0
gui_show_toolbar -toolbar {&Scope}
gui_set_toolbar_attributes -toolbar {&Window} -dock_state top
gui_set_toolbar_attributes -toolbar {&Window} -offset 0
gui_show_toolbar -toolbar {&Window}
gui_set_toolbar_attributes -toolbar {Signal} -dock_state top
gui_set_toolbar_attributes -toolbar {Signal} -offset 0
gui_show_toolbar -toolbar {Signal}
gui_set_toolbar_attributes -toolbar {Zoom} -dock_state top
gui_set_toolbar_attributes -toolbar {Zoom} -offset 0
gui_show_toolbar -toolbar {Zoom}
gui_set_toolbar_attributes -toolbar {Zoom And Pan History} -dock_state top
gui_set_toolbar_attributes -toolbar {Zoom And Pan History} -offset 0
gui_show_toolbar -toolbar {Zoom And Pan History}
gui_set_toolbar_attributes -toolbar {Grid} -dock_state top
gui_set_toolbar_attributes -toolbar {Grid} -offset 0
gui_show_toolbar -toolbar {Grid}
gui_hide_toolbar -toolbar {Simulator}
gui_hide_toolbar -toolbar {Interactive Rewind}
gui_hide_toolbar -toolbar {Testbench}

# End ToolBar settings

# Docked window settings
set HSPane.1 [gui_create_window -type HSPane -parent ${TopLevel.1} -dock_state left -dock_on_new_line true -dock_extent 166]
catch { set Hier.1 [gui_share_window -id ${HSPane.1} -type Hier] }
gui_set_window_pref_key -window ${HSPane.1} -key dock_width -value_type integer -value 166
gui_set_window_pref_key -window ${HSPane.1} -key dock_height -value_type integer -value -1
gui_set_window_pref_key -window ${HSPane.1} -key dock_offset -value_type integer -value 0
gui_update_layout -id ${HSPane.1} {{left 0} {top 0} {width 165} {height 994} {dock_state left} {dock_on_new_line true} {child_hier_colhier 168} {child_hier_coltype 10} {child_hier_colpd 0} {child_hier_col1 0} {child_hier_col2 1} {child_hier_col3 -1}}
set DLPane.1 [gui_create_window -type DLPane -parent ${TopLevel.1} -dock_state left -dock_on_new_line true -dock_extent 107]
catch { set Data.1 [gui_share_window -id ${DLPane.1} -type Data] }
gui_set_window_pref_key -window ${DLPane.1} -key dock_width -value_type integer -value 107
gui_set_window_pref_key -window ${DLPane.1} -key dock_height -value_type integer -value 784
gui_set_window_pref_key -window ${DLPane.1} -key dock_offset -value_type integer -value 0
gui_update_layout -id ${DLPane.1} {{left 0} {top 0} {width 106} {height 994} {dock_state left} {dock_on_new_line true} {child_data_colvariable 72} {child_data_colvalue 12} {child_data_coltype 17} {child_data_col1 0} {child_data_col2 1} {child_data_col3 2}}
set Console.1 [gui_create_window -type Console -parent ${TopLevel.1} -dock_state bottom -dock_on_new_line true -dock_extent 174]
gui_set_window_pref_key -window ${Console.1} -key dock_width -value_type integer -value -1
gui_set_window_pref_key -window ${Console.1} -key dock_height -value_type integer -value 174
gui_set_window_pref_key -window ${Console.1} -key dock_offset -value_type integer -value 0
gui_update_layout -id ${Console.1} {{left 0} {top 0} {width 295} {height 173} {dock_state bottom} {dock_on_new_line true}}
set DriverLoad.1 [gui_create_window -type DriverLoad -parent ${TopLevel.1} -dock_state bottom -dock_on_new_line false -dock_extent 174]
gui_set_window_pref_key -window ${DriverLoad.1} -key dock_width -value_type integer -value 150
gui_set_window_pref_key -window ${DriverLoad.1} -key dock_height -value_type integer -value 174
gui_set_window_pref_key -window ${DriverLoad.1} -key dock_offset -value_type integer -value 0
gui_update_layout -id ${DriverLoad.1} {{left 0} {top 0} {width 1991} {height 173} {dock_state bottom} {dock_on_new_line false}}
#### Start - Readjusting docked view's offset / size
set dockAreaList { top left right bottom }
foreach dockArea $dockAreaList {
  set viewList [gui_ekki_get_window_ids -active_parent -dock_area $dockArea]
  foreach view $viewList {
      if {[lsearch -exact [gui_get_window_pref_keys -window $view] dock_width] != -1} {
        set dockWidth [gui_get_window_pref_value -window $view -key dock_width]
        set dockHeight [gui_get_window_pref_value -window $view -key dock_height]
        set offset [gui_get_window_pref_value -window $view -key dock_offset]
        if { [string equal "top" $dockArea] || [string equal "bottom" $dockArea]} {
          gui_set_window_attributes -window $view -dock_offset $offset -width $dockWidth
        } else {
          gui_set_window_attributes -window $view -dock_offset $offset -height $dockHeight
        }
      }
  }
}
#### End - Readjusting docked view's offset / size
gui_sync_global -id ${TopLevel.1} -option true

# MDI window settings
set Source.1 [gui_create_window -type {Source}  -parent ${TopLevel.1}]
gui_show_window -window ${Source.1} -show_state maximized
gui_update_layout -id ${Source.1} {{show_state maximized} {dock_state undocked} {dock_on_new_line false}}

# End MDI window settings


# Create and position top-level window: TopLevel.2

if {![gui_exist_window -window TopLevel.2]} {
    set TopLevel.2 [ gui_create_window -type TopLevel \
       -icon $::env(DVE)/auxx/gui/images/toolbars/dvewin.xpm] 
} else { 
    set TopLevel.2 TopLevel.2
}
gui_show_window -window ${TopLevel.2} -show_state maximized -rect {{2632 101} {5119 1476}}

# ToolBar settings
gui_set_toolbar_attributes -toolbar {TimeOperations} -dock_state top
gui_set_toolbar_attributes -toolbar {TimeOperations} -offset 0
gui_show_toolbar -toolbar {TimeOperations}
gui_hide_toolbar -toolbar {&File}
gui_set_toolbar_attributes -toolbar {&Edit} -dock_state top
gui_set_toolbar_attributes -toolbar {&Edit} -offset 0
gui_show_toolbar -toolbar {&Edit}
gui_hide_toolbar -toolbar {CopyPaste}
gui_set_toolbar_attributes -toolbar {&Trace} -dock_state top
gui_set_toolbar_attributes -toolbar {&Trace} -offset 0
gui_show_toolbar -toolbar {&Trace}
gui_hide_toolbar -toolbar {TraceInstance}
gui_hide_toolbar -toolbar {BackTrace}
gui_set_toolbar_attributes -toolbar {&Scope} -dock_state top
gui_set_toolbar_attributes -toolbar {&Scope} -offset 0
gui_show_toolbar -toolbar {&Scope}
gui_set_toolbar_attributes -toolbar {&Window} -dock_state top
gui_set_toolbar_attributes -toolbar {&Window} -offset 0
gui_show_toolbar -toolbar {&Window}
gui_set_toolbar_attributes -toolbar {Signal} -dock_state top
gui_set_toolbar_attributes -toolbar {Signal} -offset 0
gui_show_toolbar -toolbar {Signal}
gui_set_toolbar_attributes -toolbar {Zoom} -dock_state top
gui_set_toolbar_attributes -toolbar {Zoom} -offset 0
gui_show_toolbar -toolbar {Zoom}
gui_set_toolbar_attributes -toolbar {Zoom And Pan History} -dock_state top
gui_set_toolbar_attributes -toolbar {Zoom And Pan History} -offset 0
gui_show_toolbar -toolbar {Zoom And Pan History}
gui_set_toolbar_attributes -toolbar {Grid} -dock_state top
gui_set_toolbar_attributes -toolbar {Grid} -offset 0
gui_show_toolbar -toolbar {Grid}
gui_hide_toolbar -toolbar {Simulator}
gui_hide_toolbar -toolbar {Interactive Rewind}
gui_set_toolbar_attributes -toolbar {Testbench} -dock_state top
gui_set_toolbar_attributes -toolbar {Testbench} -offset 0
gui_show_toolbar -toolbar {Testbench}

# End ToolBar settings

# Docked window settings
gui_sync_global -id ${TopLevel.2} -option true

# MDI window settings
set Wave.1 [gui_create_window -type {Wave}  -parent ${TopLevel.2}]
gui_show_window -window ${Wave.1} -show_state maximized
gui_update_layout -id ${Wave.1} {{show_state maximized} {dock_state undocked} {dock_on_new_line false} {child_wave_left 732} {child_wave_right 1750} {child_wave_colname 207} {child_wave_colvalue 521} {child_wave_col1 0} {child_wave_col2 1}}

# End MDI window settings

gui_set_env TOPLEVELS::TARGET_FRAME(Source) ${TopLevel.1}
gui_set_env TOPLEVELS::TARGET_FRAME(Schematic) ${TopLevel.1}
gui_set_env TOPLEVELS::TARGET_FRAME(PathSchematic) ${TopLevel.1}
gui_set_env TOPLEVELS::TARGET_FRAME(Wave) none
gui_set_env TOPLEVELS::TARGET_FRAME(List) none
gui_set_env TOPLEVELS::TARGET_FRAME(Memory) ${TopLevel.1}
gui_set_env TOPLEVELS::TARGET_FRAME(DriverLoad) none
gui_update_statusbar_target_frame ${TopLevel.1}
gui_update_statusbar_target_frame ${TopLevel.2}

#</WindowLayout>

#<Database>

# DVE Open design session: 

if { ![gui_is_db_opened -db {vcdplus.vpd}] } {
	gui_open_db -design V1 -file vcdplus.vpd -nosource
}
gui_set_precision 1ps
gui_set_time_units 1ps
#</Database>

# DVE Global setting session: 


# Global: Bus

# Global: Expressions

# Global: Signal Time Shift

# Global: Signal Compare

# Global: Signal Groups
gui_load_child_values {testbench.top_u.dut.control_u}
gui_load_child_values {testbench.top_u.dut.ex_stage_u}
gui_load_child_values {testbench}
gui_load_child_values {testbench.top_u.dut.is_stage_u.issue_u.regfile_u}
gui_load_child_values {testbench.top_u.dut.csr_regfile_u}
gui_load_child_values {testbench.top_u.dut.cache_subsystem_u}
gui_load_child_values {testbench.top_u.dut.is_stage_u.pick_u}
gui_load_child_values {testbench.top_u.dut.cache_subsystem_u.inst_cache_mesi_u}
gui_load_child_values {testbench.top_u.dut.ex_stage_u.mult_u.i_div}
gui_load_child_values {testbench.top_u.dut.genblk1[0].fifo_u}


set _session_group_1 global
gui_sg_create "$_session_group_1"
set global "$_session_group_1"

gui_sg_addsignal -group "$_session_group_1" { testbench.clk testbench.rstn testbench.top_u.dut.is_stage_u.pick_u.flush_i }

set _session_group_2 id
gui_sg_create "$_session_group_2"
set id "$_session_group_2"

gui_sg_addsignal -group "$_session_group_2" { {testbench.top_u.dut.id_stage_u.genblk2[0].decoder_u.ex_i} riscv_pkg.OpcodeSystem {testbench.top_u.dut.id_stage_u.genblk2[0].decoder_u.instr.rtype.opcode} {testbench.top_u.dut.id_stage_u.genblk2[0].decoder_u.instr} {testbench.top_u.dut.id_stage_u.genblk2[0].decoder_u.instr.itype.funct3} {testbench.top_u.dut.id_stage_u.genblk2[0].decoder_u.instr.itype.imm} {testbench.top_u.dut.id_stage_u.genblk2[0].decoder_u.instruction_o.fu} {testbench.top_u.dut.id_stage_u.genblk2[0].decoder_u.ebreak} {testbench.top_u.dut.id_stage_u.genblk2[0].decoder_u.instr} {testbench.top_u.dut.id_stage_u.genblk2[0].decoder_u.instruction_o.op} {testbench.top_u.dut.id_stage_u.genblk2[0].decoder_u.instruction_o.fu} {testbench.top_u.dut.id_stage_u.genblk2[0].decoder_u.illegal_instr} {testbench.top_u.dut.id_stage_u.genblk2[0].decoder_u.instr.itype.rs1} {testbench.top_u.dut.id_stage_u.genblk2[0].decoder_u.instr.itype.rd} {testbench.top_u.dut.id_stage_u.genblk2[0].decoder_u.priv_lvl_i} }
gui_set_radix -radix {decimal} -signals {V1:riscv_pkg.OpcodeSystem}
gui_set_radix -radix {unsigned} -signals {V1:riscv_pkg.OpcodeSystem}

set _session_group_3 id2is_fifo
gui_sg_create "$_session_group_3"
set id2is_fifo "$_session_group_3"

gui_sg_addsignal -group "$_session_group_3" { {testbench.top_u.dut.genblk1[0].fifo_u.pop_i} {testbench.top_u.dut.genblk1[0].fifo_u.push_i} {testbench.top_u.dut.genblk1[0].fifo_u.read_pointer_q} {testbench.top_u.dut.genblk1[0].fifo_u.write_pointer_q} {testbench.top_u.dut.genblk1[0].fifo_u.data_o.sbe.pc} {testbench.top_u.dut.genblk1[0].fifo_u.mem_q} {testbench.top_u.dut.genblk1[0].fifo_u.mem_q[0].sbe.pc} {testbench.top_u.dut.genblk1[0].fifo_u.mem_q[1].sbe.pc} {testbench.top_u.dut.genblk1[0].fifo_u.mem_q[2].sbe.pc} {testbench.top_u.dut.genblk1[0].fifo_u.mem_q[3].sbe.pc} {testbench.top_u.dut.genblk1[0].fifo_u.data_o} }

set _session_group_4 is_pick
gui_sg_create "$_session_group_4"
set is_pick "$_session_group_4"

gui_sg_addsignal -group "$_session_group_4" { testbench.top_u.dut.is_stage_u.pick_u.pick_entry_o.sbe.pc testbench.top_u.dut.is_stage_u.pick_u.pick_entry_o testbench.top_u.dut.is_stage_u.pick_u.pick_entry_valid_o testbench.top_u.dut.is_stage_u.pick_u.is_ready_i testbench.top_u.dut.is_stage_u.pick_u.fifo_empty_i testbench.top_u.dut.is_stage_u.pick_u.inst_entry_id2is_i {testbench.top_u.dut.is_stage_u.pick_u.inst_entry_id2is_i.issue_inst[0].sbe.pc} {testbench.top_u.dut.is_stage_u.pick_u.inst_entry_id2is_i.issue_inst[1].sbe.pc} testbench.top_u.dut.is_stage_u.pick_u.id2is_ptr_q testbench.top_u.dut.is_stage_u.pick_u.pick_ready_o }

set _session_group_5 is_issue
gui_sg_create "$_session_group_5"
set is_issue "$_session_group_5"

gui_sg_addsignal -group "$_session_group_5" { testbench.top_u.dut.is_stage_u.issue_u.is2ex_o testbench.top_u.dut.is_stage_u.issue_u.is2ex_valid_o testbench.top_u.dut.is_stage_u.issue_u.ex_ready_i testbench.top_u.dut.is_stage_u.issue_u.commit_valid_i testbench.top_u.dut.is_stage_u.issue_u.the_inst_at_ex.fu testbench.top_u.dut.is_stage_u.issue_u.issue_valid_d testbench.top_u.dut.is_stage_u.issue_u.rs1_pending_en testbench.top_u.dut.is_stage_u.issue_u.rs2_pending_en }

set _session_group_6 ex_from_is
gui_sg_create "$_session_group_6"
set ex_from_is "$_session_group_6"

gui_sg_addsignal -group "$_session_group_6" { testbench.top_u.dut.ex_stage_u.is_entry_i.pc testbench.top_u.dut.ex_stage_u.is_entry_i testbench.top_u.dut.ex_stage_u.is_entry_valid_i }

set _session_group_7 ex_fu_mul
gui_sg_create "$_session_group_7"
set ex_fu_mul "$_session_group_7"

gui_sg_addsignal -group "$_session_group_7" { testbench.top_u.dut.ex_stage_u.mult_valid testbench.top_u.dut.ex_stage_u.mult_ready testbench.top_u.dut.ex_stage_u.commit_valid_o testbench.top_u.dut.ex_stage_u.mult_result testbench.top_u.dut.ex_stage_u.mult_data testbench.top_u.dut.ex_stage_u.ex_entry_q testbench.top_u.dut.ex_stage_u.ex_entry_q.pc }

set _session_group_8 ex_fu_div
gui_sg_create "$_session_group_8"
set ex_fu_div "$_session_group_8"

gui_sg_addsignal -group "$_session_group_8" { testbench.top_u.dut.ex_stage_u.mult_u.i_div.op_a_i testbench.top_u.dut.ex_stage_u.mult_u.i_div.op_b_i testbench.top_u.dut.ex_stage_u.mult_u.i_div.in_vld_i testbench.top_u.dut.ex_stage_u.mult_u.i_div.in_rdy_o testbench.top_u.dut.ex_stage_u.mult_u.i_div.flush_i testbench.top_u.dut.ex_stage_u.mult_u.i_div.out_vld_o testbench.top_u.dut.ex_stage_u.mult_u.i_div.out_rdy_i testbench.top_u.dut.ex_stage_u.mult_u.i_div.res_o testbench.top_u.dut.ex_stage_u.mult_u.i_div.state_q }

set _session_group_9 ex_fu_alu
gui_sg_create "$_session_group_9"
set ex_fu_alu "$_session_group_9"

gui_sg_addsignal -group "$_session_group_9" { testbench.top_u.dut.ex_stage_u.alu_data testbench.top_u.dut.ex_stage_u.alu_result testbench.top_u.dut.ex_stage_u.alu_branch_res }

set _session_group_10 dcache
gui_sg_create "$_session_group_10"
set dcache "$_session_group_10"

gui_sg_addsignal -group "$_session_group_10" { testbench.top_u.dut.cache_subsystem_u.cc2mem_o testbench.top_u.dut.rom_ideal_u.mem2cc_o testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.ready_o testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.l2_wb_addr_o testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.tag_ram_en testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.tag_ram_rw testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.reqIndex testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.fence_rd_index testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.fence_count_q testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.cacheModified testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.valid_ram_qout testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.tag_ram_rd_valid testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.in_fence testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.fence_count_q testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.l2_wb_en_o testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.l2_ready_i testbench.top_u.dut.cache_subsystem_u.l2_wb_en testbench.top_u.dut.cache_subsystem_u.l2_rm_en testbench.top_u.dut.rom_ideal_u.struct_addr testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.MemR_en_i testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.MemW_en_i testbench.top_u.dut.cache_subsystem_u.dcache_fence_i testbench.top_u.dut.ex_stage_u.load_store_unit_u.lsu_data_in_fun testbench.top_u.dut.ex_stage_u.load_store_unit_u.req_valid_i testbench.top_u.dut.ex_stage_u.load_store_unit_u.fence_en_i testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.wb_rd_en_last testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.wb_rd_en testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.data_ram_rd_valid testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.wb_wyid testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.wb_rd_en_last testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.in_fence testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.cacheModified testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.tag_ram_rd_valid testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.fence_rd_way testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.valid_ram_en testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.valid_ram_rw testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.reqIndex testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.valid_ram_qout testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.valid_ram_qout_selected testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.valid_ram_rd_valid_selected testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.l2_wb_en_o_last testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.wb_rd_en_last testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.wb_rd_en testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.data_ram_rd_valid testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.wb_wyid testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.valid_ram_rd_valid testbench.top_u.dut.cache_subsystem_u.dcache_fence_i testbench.top_u.dut.cache_subsystem_u.l2_wb_en testbench.top_u.dut.cache_subsystem_u.l2_rm_en }

set _session_group_11 ex2wb
gui_sg_create "$_session_group_11"
set ex2wb "$_session_group_11"

gui_sg_addsignal -group "$_session_group_11" { testbench.top_u.dut.ex_stage_u.ex2wb_o }

set _session_group_12 csr
gui_sg_create "$_session_group_12"
set csr "$_session_group_12"

gui_sg_addsignal -group "$_session_group_12" { testbench.top_u.dut.ex_stage_u.is_entry_i.ex testbench.top_u.dut.ex_stage_u.csr_data testbench.top_u.dut.csr_regfile_u.mstatus_q testbench.top_u.dut.csr_regfile_u.csr_rdata testbench.top_u.dut.csr_regfile_u.epc_o testbench.top_u.dut.csr_regfile_u.eret_o testbench.top_u.dut.csr_regfile_u.cycle_q }

set _session_group_13 control
gui_sg_create "$_session_group_13"
set control "$_session_group_13"

gui_sg_addsignal -group "$_session_group_13" { testbench.top_u.dut.control_u.resolved_branch_valid_i testbench.top_u.dut.control_u.resolved_branch_i testbench.top_u.dut.control_u.flush_o testbench.top_u.dut.control_u.flush_pc_o }

set _session_group_14 regfile
gui_sg_create "$_session_group_14"
set regfile "$_session_group_14"

gui_sg_addsignal -group "$_session_group_14" { testbench.top_u.dut.is_stage_u.issue_u.regfile_u.mem }

set _session_group_15 printf
gui_sg_create "$_session_group_15"
set printf "$_session_group_15"

gui_sg_addsignal -group "$_session_group_15" { testbench.top_u.dut.rom_ideal_u.a1 testbench.top_u.dut.rom_ideal_u.a2 testbench.top_u.dut.rom_ideal_u.a12_q testbench.top_u.dut.rom_ideal_u.cc2mem_i testbench.top_u.dut.cache_subsystem_u.l1_ld_rd_en testbench.top_u.dut.cache_subsystem_u.dcache_req_data testbench.top_u.dut.cache_subsystem_u.l1_to_l2 testbench.top_u.dut.cache_subsystem_u.l2_to_l1 testbench.top_u.dut.cache_subsystem_u.l2_cache_mesi_u.readMiss_o testbench.top_u.dut.cache_subsystem_u.l2_cache_mesi_u.MemR_en_i testbench.top_u.dut.cache_subsystem_u.l2_cache_mesi_u.uncached_addr_range_en testbench.top_u.dut.cache_subsystem_u.l2_cache_mesi_u.readHit testbench.top_u.dut.cache_subsystem_u.l2_cache_mesi_u.l2_rm_in_valid testbench.top_u.dut.cache_subsystem_u.l2_cache_mesi_u.read_same_line_en testbench.top_u.dut.cache_subsystem_u.l2_rm_en testbench.top_u.dut.cache_subsystem_u.l2_to_l1.ready testbench.top_u.dut.cache_subsystem_u.l2_cache_mesi_u.reqIndex testbench.top_u.dut.cache_subsystem_u.l1_to_l2_in_handshake_q testbench.top_u.dut.cache_subsystem_u.l2_hand_shaked_q testbench.top_u.dut.cache_subsystem_u.l1_to_l2_in_handshake_q testbench.top_u.dut.cache_subsystem_u.l1_to_l2_in_func testbench.top_u.dut.cache_subsystem_u.l2_read_same_line_en testbench.top_u.dut.cache_subsystem_u.dc_req_l2_valid testbench.top_u.dut.cache_subsystem_u.ic_req_l2_valid testbench.top_u.dut.cache_subsystem_u.l2_cache_mesi_u.reqAddr testbench.top_u.dut.cache_subsystem_u.l2_cache_mesi_u.MemR_en_i testbench.top_u.dut.cache_subsystem_u.l2_cache_mesi_u.l2_inst_finish_en testbench.top_u.dut.cache_subsystem_u.l2_cache_mesi_u.cacheModified testbench.top_u.dut.cache_subsystem_u.l2_cache_mesi_u.cacheExclusive testbench.top_u.dut.cache_subsystem_u.l2_cache_mesi_u.cacheShared testbench.top_u.dut.cache_subsystem_u.l2_cache_mesi_u.cacheInvalid testbench.top_u.dut.cache_subsystem_u.l2_cache_mesi_u.readMiss_o testbench.top_u.dut.cache_subsystem_u.l2_cache_mesi_u.readHit testbench.top_u.dut.cache_subsystem_u.l2_cache_mesi_u.l2_rm_in_valid testbench.top_u.dut.cache_subsystem_u.l2_cache_mesi_u.read_same_line_en testbench.top_u.dut.cache_subsystem_u.l2_cache_mesi_u.tag_index_hit testbench.top_u.dut.cache_subsystem_u.l2_cache_mesi_u.reqTag testbench.top_u.dut.cache_subsystem_u.l2_cache_mesi_u.reqIndex testbench.top_u.dut.cache_subsystem_u.l2_hand_shaked_d testbench.top_u.dut.cache_subsystem_u.l2_hand_shaked_q testbench.top_u.dut.cache_subsystem_u.l2_to_l1.ready testbench.top_u.dut.cache_subsystem_u.l1_to_l2_in_handshake_q testbench.top_u.dut.cache_subsystem_u.l1_to_l2 testbench.top_u.dut.cache_subsystem_u.a_test testbench.top_u.dut.cache_subsystem_u.l1_to_l2_in_handshake_q testbench.top_u.dut.cache_subsystem_u.l1_to_l2_in_handshake_d testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.TagHit testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.cacheInvalid testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.valid_ram_en testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.valid_ram_rw testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.valid_ram_qout testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.valid_ram_rd_valid_selected testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.valid_ram_rd_valid testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.resp_valid testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.dc_new_inst_en testbench.top_u.dut.cache_subsystem_u.inst_cache_mesi_u.l2_rm_en_o testbench.top_u.dut.cache_subsystem_u.inst_cache_mesi_u.TagHit testbench.top_u.dut.cache_subsystem_u.inst_cache_mesi_u.cacheInvalid testbench.top_u.dut.cache_subsystem_u.inst_cache_mesi_u.l2_wb_en_o_last_finished testbench.top_u.dut.cache_subsystem_u.inst_cache_mesi_u.l2_rm_en_o_last testbench.top_u.dut.cache_subsystem_u.inst_cache_mesi_u.l2_ready_i_last testbench.top_u.dut.cache_subsystem_u.l1_to_l2_in_func testbench.top_u.dut.cache_subsystem_u.l2_to_l1.ready testbench.top_u.dut.cache_subsystem_u.rst_ni testbench.top_u.dut.cache_subsystem_u.l1_to_l2.valid testbench.top_u.dut.cache_subsystem_u.l2_to_l1.ready }
gui_sg_addsignal -group "$_session_group_15" { testbench.top_u.dut.cache_subsystem_u.dcache_req_data testbench.top_u.dut.cache_subsystem_u.l1_st_wt_en testbench.top_u.dut.rom_ideal_u.struct_addr testbench.top_u.dut.rom_ideal_u.addr testbench.top_u.dut.cache_subsystem_u.l2_cache_mesi_u.MemR_en_i testbench.top_u.dut.cache_subsystem_u.l2_cache_mesi_u.readMiss_o testbench.top_u.dut.cache_subsystem_u.l2_inst_finish_en testbench.top_u.dut.cache_subsystem_u.l2_cache_mesi_u.write_same_line_en testbench.top_u.dut.cache_subsystem_u.l2_cache_mesi_u.read_same_line_en }

set _session_group_16 fence
gui_sg_create "$_session_group_16"
set fence "$_session_group_16"

gui_sg_addsignal -group "$_session_group_16" { testbench.top_u.dut.cache_subsystem_u.in_fence_en testbench.top_u.dut.cache_subsystem_u.l2_in_fence_en testbench.top_u.dut.cache_subsystem_u.l2_wb_en testbench.top_u.dut.cache_subsystem_u.l2_to_l1.ready testbench.top_u.dut.cache_subsystem_u.finish_fence_en testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.wb_rd_en testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.data_ram_rd_valid testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.wb_wyid testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.in_fence testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.cacheModified testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.valid_ram_en testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.valid_ram_rw testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.reqIndex testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.valid_ram_qout testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.valid_ram_rd_valid_selected testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.cacheModified testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.valid_ram_qout_selected testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.TagHit testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.replace_wyid testbench.top_u.dut.cache_subsystem_u.l2_cache_mesi_u.l2_wb_en_o testbench.top_u.dut.cache_subsystem_u.cc2mem_o testbench.top_u.dut.cache_subsystem_u.l2_cache_mesi_u.valid_ram_en testbench.top_u.dut.cache_subsystem_u.l2_cache_mesi_u.valid_ram_rw testbench.top_u.dut.cache_subsystem_u.l2_cache_mesi_u.cacheLineValid_next testbench.top_u.dut.cache_subsystem_u.l2_cache_mesi_u.wb_wyid testbench.top_u.dut.cache_subsystem_u.l2_cache_mesi_u.write_same_line_en testbench.top_u.dut.cache_subsystem_u.l2_cache_mesi_u.valid_ram_rw testbench.top_u.dut.cache_subsystem_u.l2_cache_mesi_u.TagHit testbench.top_u.dut.cache_subsystem_u.l2_cache_mesi_u.replace_wyid testbench.top_u.dut.cache_subsystem_u.l2_cache_mesi_u.cacheModified testbench.top_u.dut.cache_subsystem_u.l2_cache_mesi_u.cacheExclusive testbench.top_u.dut.cache_subsystem_u.l2_cache_mesi_u.cacheShared testbench.top_u.dut.cache_subsystem_u.l2_cache_mesi_u.cacheInvalid testbench.top_u.dut.cache_subsystem_u.l2_cache_mesi_u.fence_rd_index testbench.top_u.dut.cache_subsystem_u.l2_cache_mesi_u.atMESstage }

set _session_group_17 mmu
gui_sg_create "$_session_group_17"
set mmu "$_session_group_17"

gui_sg_addsignal -group "$_session_group_17" { testbench.top_u.dut.en_translation_csr testbench.top_u.dut.en_ld_st_translation_csr testbench.top_u.dut.fr2mmu testbench.top_u.dut.mmu2fr }

# Global: Highlighting

# Global: Stack
gui_change_stack_mode -mode list

# Post database loading setting...

# Restore C1 time
gui_set_time -C1_only 19185000



# Save global setting...

# Wave/List view global setting
gui_cov_show_value -switch false

# Close all empty TopLevel windows
foreach __top [gui_ekki_get_window_ids -type TopLevel] {
    if { [llength [gui_ekki_get_window_ids -parent $__top]] == 0} {
        gui_close_window -window $__top
    }
}
gui_set_loading_session_type noSession
# DVE View/pane content session: 


# Hier 'Hier.1'
gui_show_window -window ${Hier.1}
gui_list_set_filter -id ${Hier.1} -list { {Package 1} {All 0} {Process 1} {VirtPowSwitch 0} {UnnamedProcess 1} {UDP 0} {Function 1} {Block 1} {SrsnAndSpaCell 0} {OVA Unit 1} {LeafScCell 1} {LeafVlgCell 1} {Interface 1} {LeafVhdCell 1} {$unit 1} {NamedBlock 1} {Task 1} {VlgPackage 1} {ClassDef 1} {VirtIsoCell 0} }
gui_list_set_filter -id ${Hier.1} -text {*}
gui_hier_list_init -id ${Hier.1}
gui_change_design -id ${Hier.1} -design V1
catch {gui_list_expand -id ${Hier.1} testbench}
catch {gui_list_expand -id ${Hier.1} testbench.top_u}
catch {gui_list_select -id ${Hier.1} {testbench.top_u.dut}}
gui_view_scroll -id ${Hier.1} -vertical -set 0
gui_view_scroll -id ${Hier.1} -horizontal -set 0

# Data 'Data.1'
gui_list_set_filter -id ${Data.1} -list { {Buffer 1} {Input 1} {Others 1} {Linkage 1} {Output 1} {LowPower 1} {Parameter 1} {All 1} {Aggregate 1} {LibBaseMember 1} {Event 1} {Assertion 1} {Constant 1} {Interface 1} {BaseMembers 1} {Signal 1} {$unit 1} {Inout 1} {Variable 1} }
gui_list_set_filter -id ${Data.1} -text {*}
gui_list_show_data -id ${Data.1} {testbench.top_u.dut}
gui_view_scroll -id ${Data.1} -vertical -set 0
gui_view_scroll -id ${Data.1} -horizontal -set 0
gui_view_scroll -id ${Hier.1} -vertical -set 0
gui_view_scroll -id ${Hier.1} -horizontal -set 0

# DriverLoad 'DriverLoad.1'
gui_get_drivers -session -id ${DriverLoad.1} -signal testbench.top_u.dut.id_stage_u.id2is_valid_o -time 865000 -starttime 895000
gui_get_drivers -session -id ${DriverLoad.1} -signal testbench.top_u.dut.id_stage_u.inst_entry_f2d_i -time 1135000 -starttime 1135000
gui_get_drivers -session -id ${DriverLoad.1} -signal testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.ready_o -time 215000 -starttime 215000
gui_get_drivers -session -id ${DriverLoad.1} -signal testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.l2_rm_en_o_wait_last -time 0 -starttime 215000
gui_get_drivers -session -id ${DriverLoad.1} -signal {testbench.top_u.dut.ex_stage_u.load_store_unit_u.result_o[63:0]} -time 435000 -starttime 435000
gui_get_drivers -session -id ${DriverLoad.1} -signal testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.l2_rm_in_valid -time 0 -starttime 215000
gui_get_drivers -session -id ${DriverLoad.1} -signal {testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.store_data[511:0]} -time 225000 -starttime 225000
gui_get_drivers -session -id ${DriverLoad.1} -signal {testbench.top_u.dut.rom_ideal_u.rom_read_index[35:0]} -time 16285000 -starttime 16535000
gui_get_drivers -session -id ${DriverLoad.1} -signal {testbench.top_u.dut.rom_ideal_u.cc2mem_i.data[511:0]} -time 4192675000 -starttime 4192675000
gui_get_drivers -session -id ${DriverLoad.1} -signal testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.cacheModified -time 4981555000 -starttime 4981565000
gui_get_drivers -session -id ${DriverLoad.1} -signal testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.cacheModified -time 4981555000 -starttime 4984815000
gui_get_drivers -session -id ${DriverLoad.1} -signal {testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.valid_ram_qout_selected[1:0]} -time 4981555000 -starttime 4984815000
gui_get_drivers -session -id ${DriverLoad.1} -signal testbench.top_u.dut.cache_subsystem_u.l2_cache_mesi_u.readMiss_o -time 5688475000 -starttime 5688485000
gui_get_drivers -session -id ${DriverLoad.1} -signal testbench.top_u.dut.cache_subsystem_u.l2_cache_mesi_u.ready_o -time 5653225000 -starttime 5653225000
gui_get_drivers -session -id ${DriverLoad.1} -signal testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.l2_rm_en_o -time 5653235000 -starttime 5653255000
gui_get_drivers -session -id ${DriverLoad.1} -signal testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.cacheInvalid -time 5653225000 -starttime 5653225000
gui_get_drivers -session -id ${DriverLoad.1} -signal testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.valid_ram_rd_valid_selected -time 5653225000 -starttime 5653225000
gui_get_drivers -session -id ${DriverLoad.1} -signal {testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.valid_ram_rd_valid_next[1:0]} -time 5653225000 -starttime 5653225000
gui_get_drivers -session -id ${DriverLoad.1} -signal testbench.top_u.dut.cache_subsystem_u.inst_cache_mesi_u.l2_rm_en_o -time 5653205000 -starttime 5653225000

# Source 'Source.1'
gui_src_value_annotate -id ${Source.1} -switch false
gui_set_env TOGGLE::VALUEANNOTATE 0
gui_open_source -id ${Source.1}  -replace -active testbench.top_u.dut ../src/apd.sv
gui_view_scroll -id ${Source.1} -vertical -set 6858
gui_src_set_reusable -id ${Source.1}

# View 'Wave.1'
gui_wv_sync -id ${Wave.1} -switch false
set groupExD [gui_get_pref_value -category Wave -key exclusiveSG]
gui_set_pref_value -category Wave -key exclusiveSG -value {false}
set origWaveHeight [gui_get_pref_value -category Wave -key waveRowHeight]
gui_list_set_height -id Wave -height 25
set origGroupCreationState [gui_list_create_group_when_add -wave]
gui_list_create_group_when_add -wave -disable
gui_marker_set_ref -id ${Wave.1}  C1
gui_wv_zoom_timerange -id ${Wave.1} 18647043 19720388
gui_list_add_group -id ${Wave.1} -after {New Group} {global}
gui_list_add_group -id ${Wave.1} -after {New Group} {id}
gui_list_add_group -id ${Wave.1} -after {New Group} {id2is_fifo}
gui_list_add_group -id ${Wave.1} -after {New Group} {is_pick}
gui_list_add_group -id ${Wave.1} -after {New Group} {is_issue}
gui_list_add_group -id ${Wave.1} -after {New Group} {ex_from_is}
gui_list_add_group -id ${Wave.1} -after {New Group} {ex_fu_mul}
gui_list_add_group -id ${Wave.1} -after {New Group} {ex_fu_div}
gui_list_add_group -id ${Wave.1} -after {New Group} {ex_fu_alu}
gui_list_add_group -id ${Wave.1} -after {New Group} {dcache}
gui_list_add_group -id ${Wave.1} -after {New Group} {ex2wb}
gui_list_add_group -id ${Wave.1} -after {New Group} {csr}
gui_list_add_group -id ${Wave.1} -after {New Group} {control}
gui_list_add_group -id ${Wave.1} -after {New Group} {regfile}
gui_list_add_group -id ${Wave.1} -after {New Group} {printf}
gui_list_add_group -id ${Wave.1} -after {New Group} {fence}
gui_list_add_group -id ${Wave.1} -after {New Group} {mmu}
gui_list_collapse -id ${Wave.1} ex_fu_mul
gui_list_collapse -id ${Wave.1} ex_fu_div
gui_list_collapse -id ${Wave.1} ex_fu_alu
gui_list_collapse -id ${Wave.1} regfile
gui_list_collapse -id ${Wave.1} printf
gui_list_expand -id ${Wave.1} {testbench.top_u.dut.id_stage_u.genblk2[0].decoder_u.ex_i}
gui_list_expand -id ${Wave.1} riscv_pkg.OpcodeSystem
gui_list_expand -id ${Wave.1} {testbench.top_u.dut.id_stage_u.genblk2[0].decoder_u.instr}
gui_list_expand -id ${Wave.1} {testbench.top_u.dut.id_stage_u.genblk2[0].decoder_u.instr.rtype}
gui_list_expand -id ${Wave.1} {testbench.top_u.dut.id_stage_u.genblk2[0].decoder_u.instr.itype}
gui_list_expand -id ${Wave.1} testbench.top_u.dut.is_stage_u.pick_u.pick_entry_o
gui_list_expand -id ${Wave.1} testbench.top_u.dut.is_stage_u.pick_u.pick_entry_o.sbe
gui_list_expand -id ${Wave.1} testbench.top_u.dut.is_stage_u.issue_u.is2ex_o
gui_list_expand -id ${Wave.1} testbench.top_u.dut.ex_stage_u.is_entry_i
gui_list_expand -id ${Wave.1} testbench.top_u.dut.ex_stage_u.is_entry_i.ex
gui_list_expand -id ${Wave.1} testbench.top_u.dut.cache_subsystem_u.cc2mem_o
gui_list_expand -id ${Wave.1} testbench.top_u.dut.rom_ideal_u.mem2cc_o
gui_list_expand -id ${Wave.1} testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.valid_ram_qout
gui_list_expand -id ${Wave.1} testbench.top_u.dut.cache_subsystem_u.data_cache_mesi_u.valid_ram_qout_selected
gui_list_expand -id ${Wave.1} testbench.top_u.dut.ex_stage_u.ex2wb_o
gui_list_expand -id ${Wave.1} testbench.top_u.dut.cache_subsystem_u.l2_cache_mesi_u.valid_ram_en
gui_list_expand -id ${Wave.1} testbench.top_u.dut.cache_subsystem_u.l2_cache_mesi_u.replace_wyid
gui_list_expand -id ${Wave.1} testbench.top_u.dut.fr2mmu
gui_set_radix -radix enum_toggle -signal testbench.top_u.dut.is_stage_u.pick_u.pick_entry_o.sbe.fu
gui_set_radix -radix enum_toggle -signal testbench.top_u.dut.is_stage_u.pick_u.pick_entry_o.sbe.op
gui_set_radix -radix enum_toggle -signal testbench.top_u.dut.is_stage_u.issue_u.is2ex_o.fu
gui_set_radix -radix enum_toggle -signal testbench.top_u.dut.is_stage_u.issue_u.is2ex_o.operator
gui_seek_criteria -id ${Wave.1} {Any Edge}



gui_set_env TOGGLE::DEFAULT_WAVE_WINDOW ${Wave.1}
gui_set_pref_value -category Wave -key exclusiveSG -value $groupExD
gui_list_set_height -id Wave -height $origWaveHeight
if {$origGroupCreationState} {
	gui_list_create_group_when_add -wave -enable
}
if { $groupExD } {
 gui_msg_report -code DVWW028
}
gui_list_set_filter -id ${Wave.1} -list { {Buffer 1} {Input 1} {Others 1} {Linkage 1} {Output 1} {Parameter 1} {All 1} {Aggregate 1} {LibBaseMember 1} {Event 1} {Assertion 1} {Constant 1} {Interface 1} {BaseMembers 1} {Signal 1} {$unit 1} {Inout 1} {Variable 1} }
gui_list_set_filter -id ${Wave.1} -text {*}
gui_list_set_insertion_bar  -id ${Wave.1} -group mmu  -item testbench.top_u.dut.mmu2fr -position below

gui_marker_move -id ${Wave.1} {C1} 19185000
gui_view_scroll -id ${Wave.1} -vertical -set 5671
gui_show_grid -id ${Wave.1} -enable false
# Restore toplevel window zorder
# The toplevel window could be closed if it has no view/pane
if {[gui_exist_window -window ${TopLevel.1}]} {
	gui_set_active_window -window ${TopLevel.1}
	gui_set_active_window -window ${Source.1}
}
if {[gui_exist_window -window ${TopLevel.2}]} {
	gui_set_active_window -window ${TopLevel.2}
	gui_set_active_window -window ${Wave.1}
}
#</Session>

