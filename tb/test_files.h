#ifndef _TEST_FILES_
#define _TEST_FILES_
#ifndef V
#define TEST_NUM 122-54+12 -5
#else
#define TEST_NUM 122-54+12 + 65 -5
#endif

char test_files[TEST_NUM][50]={ 
"rv64ui-p-add.bin",
"rv64ui-p-addi.bin",
"rv64ui-p-addiw.bin",
"rv64ui-p-addw.bin",
"rv64ui-p-and.bin",
"rv64ui-p-andi.bin",
"rv64ui-p-auipc.bin",
"rv64ui-p-beq.bin",
"rv64ui-p-bge.bin",
"rv64ui-p-bgeu.bin",
"rv64ui-p-blt.bin",
"rv64ui-p-bltu.bin",
"rv64ui-p-bne.bin",
// "rv64ui-p-fence_i.bin",      // newly added, not passed
"rv64ui-p-jal.bin",
"rv64ui-p-jalr.bin",
"rv64ui-p-lb.bin",
"rv64ui-p-lbu.bin",
"rv64ui-p-ld.bin",
"rv64ui-p-lh.bin",
"rv64ui-p-lhu.bin",
"rv64ui-p-lui.bin",
"rv64ui-p-lw.bin",
"rv64ui-p-lwu.bin",
"rv64ui-p-or.bin",
"rv64ui-p-ori.bin",
"rv64ui-p-sb.bin",
"rv64ui-p-sd.bin",
"rv64ui-p-sh.bin",
"rv64ui-p-simple.bin",       // newly added, passed
"rv64ui-p-sll.bin",
"rv64ui-p-slli.bin",
"rv64ui-p-slliw.bin",
"rv64ui-p-sllw.bin",
"rv64ui-p-slt.bin",
"rv64ui-p-slti.bin",
"rv64ui-p-sltiu.bin",
"rv64ui-p-sltu.bin",
"rv64ui-p-sra.bin",
"rv64ui-p-srai.bin",
"rv64ui-p-sraiw.bin",
"rv64ui-p-sraw.bin",
"rv64ui-p-srl.bin",
"rv64ui-p-srli.bin",
"rv64ui-p-srliw.bin",
"rv64ui-p-srlw.bin",
"rv64ui-p-sub.bin",
"rv64ui-p-subw.bin",
"rv64ui-p-sw.bin",
"rv64ui-p-xor.bin",
"rv64ui-p-xori.bin",

"rv64uc-p-rvc.bin",

// "rv64mi-p-access.bin",       // newly added, not passed
"rv64mi-p-breakpoint.bin",   // newly added, not passed
"rv64mi-p-csr.bin",          // newly added, passed
// "rv64mi-p-illegal.bin",      // newly added, not passed
// "rv64mi-p-ma_addr.bin",         // not passed
"rv64mi-p-ma_fetch.bin",
"rv64mi-p-mcsr.bin",
"rv64mi-p-sbreak.bin",       // newly added, passed
"rv64mi-p-scall.bin",        // newly added, passed

"rv64si-p-csr.bin",
// "rv64si-p-dirty.bin",        // newly added, not passed
"rv64si-p-ma_fetch.bin",
"rv64si-p-sbreak.bin",       // newly added, passed
"rv64si-p-scall.bin",        // newly added, passed
"rv64si-p-wfi.bin",          // newly added, passed

// "rv64ua-p-amoadd_d.bin",
// "rv64ua-p-amoadd_w.bin",
// "rv64ua-p-amoand_d.bin",
// "rv64ua-p-amoand_w.bin",
// "rv64ua-p-amomax_d.bin",
// "rv64ua-p-amomaxu_d.bin",
// "rv64ua-p-amomaxu_w.bin",
// "rv64ua-p-amomax_w.bin",
// "rv64ua-p-amomin_d.bin",
// "rv64ua-p-amominu_d.bin",
// "rv64ua-p-amominu_w.bin",
// "rv64ua-p-amomin_w.bin",
// "rv64ua-p-amoor_d.bin",
// "rv64ua-p-amoor_w.bin",
// "rv64ua-p-amoswap_d.bin",
// "rv64ua-p-amoswap_w.bin",
// "rv64ua-p-amoxor_d.bin",
// "rv64ua-p-amoxor_w.bin",
// "rv64ua-p-lrsc.bin",

// "rv64ud-p-fadd.bin",
// "rv64ud-p-fclass.bin",
// "rv64ud-p-fcmp.bin",
// "rv64ud-p-fcvt.bin",
// "rv64ud-p-fcvt_w.bin",
// "rv64ud-p-fdiv.bin",
// "rv64ud-p-fmadd.bin",
// "rv64ud-p-fmin.bin",
// "rv64ud-p-ldst.bin",
// "rv64ud-p-move.bin",
// "rv64ud-p-recoding.bin",
// "rv64ud-p-structural.bin",

// "rv64uf-p-fadd.bin",
// "rv64uf-p-fclass.bin",
// "rv64uf-p-fcmp.bin",
// "rv64uf-p-fcvt.bin",
// "rv64uf-p-fcvt_w.bin",
// "rv64uf-p-fdiv.bin",
// "rv64uf-p-fmadd.bin",
// "rv64uf-p-fmin.bin",
// "rv64uf-p-ldst.bin",
// "rv64uf-p-move.bin",
// "rv64uf-p-recoding.bin",

"rv64um-p-div.bin",
"rv64um-p-divu.bin",
"rv64um-p-divuw.bin",
"rv64um-p-divw.bin",
"rv64um-p-mul.bin",
"rv64um-p-mulh.bin",
"rv64um-p-mulhsu.bin",
"rv64um-p-mulhu.bin",
"rv64um-p-mulw.bin",
"rv64um-p-rem.bin",
"rv64um-p-remu.bin",
"rv64um-p-remuw.bin",
"rv64um-p-remw.bin",

// rv64-v-*:

// "rv64ua-v-amoadd_d.bin",
// "rv64ua-v-amoadd_w.bin",
// "rv64ua-v-amoand_d.bin",
// "rv64ua-v-amoand_w.bin",
// "rv64ua-v-amomax_d.bin",
// "rv64ua-v-amomaxu_d.bin",
// "rv64ua-v-amomaxu_w.bin",
// "rv64ua-v-amomax_w.bin",
// "rv64ua-v-amomin_d.bin",
// "rv64ua-v-amominu_d.bin",
// "rv64ua-v-amominu_w.bin",
// "rv64ua-v-amomin_w.bin",
// "rv64ua-v-amoor_d.bin",
// "rv64ua-v-amoor_w.bin",
// "rv64ua-v-amoswap_d.bin",
// "rv64ua-v-amoswap_w.bin",
// "rv64ua-v-amoxor_d.bin",
// "rv64ua-v-amoxor_w.bin",
// "rv64ua-v-lrsc.bin",

"rv64uc-v-rvc.bin",

// "rv64ud-v-fadd.bin",
// "rv64ud-v-fclass.bin",
// "rv64ud-v-fcmp.bin",
// "rv64ud-v-fcvt.bin",
// "rv64ud-v-fcvt_w.bin",
// "rv64ud-v-fdiv.bin",
// "rv64ud-v-fmadd.bin",
// "rv64ud-v-fmin.bin",
// "rv64ud-v-ldst.bin",
// "rv64ud-v-move.bin",
// "rv64ud-v-recoding.bin",
// "rv64ud-v-structural.bin",

// "rv64uf-v-fadd.bin",
// "rv64uf-v-fclass.bin",
// "rv64uf-v-fcmp.bin",
// "rv64uf-v-fcvt.bin",
// "rv64uf-v-fcvt_w.bin",
// "rv64uf-v-fdiv.bin",
// "rv64uf-v-fmadd.bin",
// "rv64uf-v-fmin.bin",
// "rv64uf-v-ldst.bin",
// "rv64uf-v-move.bin",
// "rv64uf-v-recoding.bin",

"rv64ui-v-add.bin",
"rv64ui-v-addi.bin",
"rv64ui-v-addiw.bin",
"rv64ui-v-addw.bin",
"rv64ui-v-and.bin",
"rv64ui-v-andi.bin",
"rv64ui-v-auipc.bin",
"rv64ui-v-beq.bin",
"rv64ui-v-bge.bin",
"rv64ui-v-bgeu.bin",
"rv64ui-v-blt.bin",
"rv64ui-v-bltu.bin",
"rv64ui-v-bne.bin",
"rv64ui-v-fence_i.bin",
"rv64ui-v-jal.bin",
"rv64ui-v-jalr.bin",
"rv64ui-v-lb.bin",
"rv64ui-v-lbu.bin",
"rv64ui-v-ld.bin",
"rv64ui-v-lh.bin",
"rv64ui-v-lhu.bin",
"rv64ui-v-lui.bin",
"rv64ui-v-lw.bin",
"rv64ui-v-lwu.bin",
"rv64ui-v-or.bin",
"rv64ui-v-ori.bin",
"rv64ui-v-sb.bin",
"rv64ui-v-sd.bin",
"rv64ui-v-sh.bin",
"rv64ui-v-simple.bin",
"rv64ui-v-sll.bin",
"rv64ui-v-slli.bin",
"rv64ui-v-slliw.bin",
"rv64ui-v-sllw.bin",
"rv64ui-v-slt.bin",
"rv64ui-v-slti.bin",
"rv64ui-v-sltiu.bin",
"rv64ui-v-sltu.bin",
"rv64ui-v-sra.bin",
"rv64ui-v-srai.bin",
"rv64ui-v-sraiw.bin",
"rv64ui-v-sraw.bin",
"rv64ui-v-srl.bin",
"rv64ui-v-srli.bin",
"rv64ui-v-srliw.bin",
"rv64ui-v-srlw.bin",
"rv64ui-v-sub.bin",
"rv64ui-v-subw.bin",
"rv64ui-v-sw.bin",
"rv64ui-v-xor.bin",
"rv64ui-v-xori.bin",

"rv64um-v-div.bin",
"rv64um-v-divu.bin",
"rv64um-v-divuw.bin",
"rv64um-v-divw.bin",
"rv64um-v-mul.bin",
"rv64um-v-mulh.bin",
"rv64um-v-mulhsu.bin",
"rv64um-v-mulhu.bin",
"rv64um-v-mulw.bin",
"rv64um-v-rem.bin",
"rv64um-v-remu.bin",
"rv64um-v-remuw.bin",
"rv64um-v-remw.bin"


};
#endif