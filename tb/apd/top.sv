

import apd_pkg::*;

module top 
(
  input logic                   clk_i,
  input logic                   rst_ni
);

parameter BIT_PER_LINE = $clog2(ICACHE_LINE_WIDTH / 8);



  apd dut
  (
    .clk_i              ( clk_i       ),
    .rst_ni             ( rst_ni      ),
    .boot_addr_i        ( BOOT_ADDR   ), // TODO
    .hart_id_i          ( '0          )  // TODO
  );



`ifdef VCS
  int        report_f;
`endif
`ifdef LAB_TEST2
  logic [63:0] 		lab2_data;
	logic [63:0]    lab2_addr;
`endif
  string     image_f;
  initial begin
    int        f;
    int        cnt = 0;
    bit [63:0] data;
    bit [$clog2(BYTE_PER_LINE/4)-1:0] oneline_count;
    logic[31:0]    bdata;
    int        num_byte;
    string     kernel_f;
    
    string     weights_f;
    string     report_file_f;
    string     case_name;
    bit [39:0] tohost_addr;
    bit [39:0] image_addr;
    bit [31:0] tohost_val;

// Loading Image
      $display("%t Start Loading Image from backdoor", $time);

`ifdef VCS
  if ($value$plusargs("backdoor_load_image=%s", image_f)) begin
      $display("%t Start Loading image from backdoor", $time);
      if ($value$plusargs("report_file=%s", report_file_f)) begin
        report_f = $fopen(report_file_f, "a");
        if ($value$plusargs("case_name=%s", case_name))
          $fdisplay(report_f, "\ncase: %s", case_name);
        else
          $fdisplay(report_f, "\ncase: unknown", case_name);
      end
      else
        report_f = 0;
`endif


`ifdef LAB_TEST2
      
      // dut.rom_ideal_u.rom_q['h802276b8 >> BIT_PER_LINE] = '0;
      // dut.rom_ideal_u.rom_q['h808276b8 >> BIT_PER_LINE] = '0;
      for(image_addr='h80000000; image_addr<'hffffffff; image_addr+='d256)begin
        dut.rom_ideal_u.rom_q[image_addr >> BIT_PER_LINE] = '0;
        dut.rom_ideal_u.rom_q[(image_addr+'d64) >> BIT_PER_LINE] = '0;
        dut.rom_ideal_u.rom_q[(image_addr+'d128) >> BIT_PER_LINE] = '0;
        dut.rom_ideal_u.rom_q[(image_addr+'d192) >> BIT_PER_LINE] = '0;
      end
      f = $fopen(image_f, "r");
      if (f == 0)
        $display("TOP", $sformatf("Unable to open %s for read", image_f));

      num_byte = $fscanf(f, "%x %x", lab2_addr, lab2_data);
      while (num_byte > 0) begin

				for (int i=0; i < 64; i++) begin
					if (lab2_addr[i] === 1'bx) begin
						lab2_addr[i] = 1'b0;
          end
        end
        oneline_count = lab2_addr[5:3];
				dut.rom_ideal_u.rom_q[lab2_addr >> BIT_PER_LINE][oneline_count*64 +: 64]  = lab2_data[63:0];
        // $display("Address: 0x%h, Data: 0x%h", lab2_addr, dut.rom_ideal_u.rom_q[lab2_addr >> BIT_PER_LINE]);
				num_byte = $fscanf(f, "%x %x", lab2_addr, lab2_data);
			end
`else
      f = $fopen(image_f, "rb");
      if (f == 0)
        $display("TOP", $sformatf("Unable to open %s for read", image_f));

      image_addr ='h80000000; 
      num_byte = $fread(bdata, f);
      oneline_count = 0;
      while (num_byte > 0) begin
        // align byte endianess
        if (num_byte == 4) begin
          data[31:24] = bdata[7:0];
          data[23:16] = bdata[15:8];
          data[15:8]  = bdata[23:16];
          data[7:0]   = bdata[31:24];
        end
        else if (num_byte == 3) begin
          data[31:24] = 0;
          data[23:16] = bdata[15:8];
          data[15:8]  = bdata[23:16];
          data[7:0]   = bdata[31:24];
        end
        else if (num_byte == 2) begin
          data[31:24] = 0;
          data[23:16] = 0;
          data[15:8]  = bdata[23:16];
          data[7:0]   = bdata[31:24];
        end
        else if (num_byte == 1) begin
          data[31:24] = 0;
          data[23:16] = 0;
          data[15:8]  = 0;
          data[7:0]   = bdata[31:24];
        end

        dut.rom_ideal_u.rom_q[image_addr >> BIT_PER_LINE][oneline_count*32 +: 32]  = data[31:0];
        // if(oneline_count == 15)begin
        //   $display("Address: 0x%h, Data: 0x%h", image_addr, dut.rom_ideal_u.rom_q[image_addr >> BIT_PER_LINE]);
        // end
        
        image_addr += 4;

        num_byte = $fread(bdata, f);
        oneline_count = oneline_count + 1;

      end


      // $display("Address: 0x%h, Data: 0x%h", image_addr, dut.rom_ideal_u.rom_q[image_addr >> BIT_PER_LINE]);
      // while (image_addr < 'h8002ffff)
`ifdef U_TEST
      while (image_addr < 'h8002ffff)
`else
      while (image_addr < 'h8102ffff)
`endif
      begin
        dut.rom_ideal_u.rom_q[image_addr >> BIT_PER_LINE][oneline_count*32 +: 32] = 32'd0;
        oneline_count = oneline_count + 1;
        image_addr += 4;
      end
`endif

      $display("%t Done Loading Image from backdoor. addr range: %h", $time, image_addr);
    end
`ifdef VCS
  end
`endif




endmodule
